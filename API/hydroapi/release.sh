set -ex

# SET THE FOLLOWING VARIABLES
USERNAME=xplolel
IMAGE=hydroapi

verbump

ver=`cat VERSION`

docker build -t $USERNAME/$IMAGE:latest .
docker tag $IMAGE:latest $IMAGE:$ver

git tag $ver