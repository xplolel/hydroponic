package com.uciekinierzy.hydroapi.svcsystems;

import com.uciekinierzy.hydroapi.svcsystems.system.TOs.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.uciekinierzy.hydroapi.svcclients.clients.ClientTestUtils.createUserAndLogIn;
import static com.uciekinierzy.hydroapi.svcsystems.SystemTestUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@Slf4j
public class BasicSystemsTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createEmptySystem() throws Exception {
        String jwt = createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();
        
        ModuleTO module = createModule(jwt, systemId, mockMvc);

        assertNotNull(module);
        assertNotNull(module.getSlots());
        assertEquals(8, module.getSlots().size());

        String moduleId = module.getId();
        checkIfSlotsAreEmpty(jwt, systemId, moduleId, mockMvc);

        final String systemix = "systemix";
        final SystemTO systemTO = patchSystem(jwt,
                systemId,
                SystemPatchingTO.builder()
                        .name(systemix)
                        .build(),
                mockMvc);
        assertEquals(systemix, systemTO.getName());
    }

    @Test
    public void createSystemWithTwoModules_removeOne() throws Exception {
        String jwt = createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        checkIfThereAreNoModules(jwt, systemId, mockMvc);

        String moduleId = createModule(jwt, systemId, mockMvc).getId();
        String module2Id = createModule(jwt, systemId, mockMvc).getId();

        checkIfSlotsAreEmpty(jwt, systemId, moduleId, mockMvc);
        checkIfSlotsAreEmpty(jwt, systemId, module2Id, mockMvc);

        deleteModule(jwt, systemId, module2Id, mockMvc);

        final List<ModuleTO> modules = getModules(jwt, systemId, mockMvc);
        assertEquals(1, modules.size());
        assertEquals(moduleId, modules.get(0).getId()); // 1st left

        final String newName = "Moduek";
        final ModuleTO module = changeModuleName(jwt, systemId, moduleId, newName, mockMvc);
        assertEquals(newName, module.getModuleName());
    }

    @Test
    public void createSystem_plantSeeds_checkSlots_deleteSeeds() throws Exception {
        String jwt = createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        checkIfThereAreNoModules(jwt, systemId, mockMvc);
        String moduleId = createModule(jwt, systemId, mockMvc).getId();

        checkIfSlotsAreEmpty(jwt, systemId, moduleId, mockMvc);
        List<SlotTO> slots = getSlots(jwt, systemId, moduleId, mockMvc);

        String plantId = "plant";
        SlotTO slotTO = plantSeed(plantId, systemId, moduleId, slots.get(0).getId(), jwt, mockMvc);

        assertEquals(slotTO, getSlot(systemId, moduleId, slotTO.getId(), jwt, mockMvc));

        deleteSeed(systemId, moduleId, slotTO.getId(), jwt, mockMvc);
        checkIfSlotsAreEmpty(jwt, systemId, moduleId, mockMvc);
    }

    @Test
    public void checkModulesOrdersTest() throws Exception {
        String jwt = createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        checkIfThereAreNoModules(jwt, systemId, mockMvc);

        Integer one = 1;
        Integer two = 2;
        Integer three = 3;
        Integer four = 4;

        assertEquals(one, createModule(jwt, systemId, mockMvc).getModuleNumber());
        assertEquals(two, createModule(jwt, systemId, mockMvc).getModuleNumber());
        assertEquals(three, createModule(jwt, systemId, mockMvc).getModuleNumber());
        assertEquals(four, createModule(jwt, systemId, mockMvc).getModuleNumber());
    }
}
