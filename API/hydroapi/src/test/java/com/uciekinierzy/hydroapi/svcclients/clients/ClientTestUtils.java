package com.uciekinierzy.hydroapi.svcclients.clients;

import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientLoginTO;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientPasswordResetTO;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientRegisterTO;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientTO;
import com.uciekinierzy.hydroapi.svcclients.controllers.TokenWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.test.web.servlet.MockMvc;

import java.security.SecureRandom;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.asJson;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.fromJson;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class ClientTestUtils {

    private static String randomText() {
        SecureRandom secureRandom = new SecureRandom();
        String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        StringBuilder generatedString = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            int randonSequence = secureRandom .nextInt(CHARACTERS.length());
            generatedString.append(CHARACTERS.charAt(randonSequence));
        }
        return generatedString.toString();
    }

    private static String randomMail(){
        return randomText() + "@gmail.com";
    }

    public static String createUserAndLogIn(MockMvc mockMvc) throws Exception {
        return createUserAndLogIn(mockMvc, randomMail(), randomText()).getToken();
    }

    public static String createUserAndLogIn(MockMvc mockMvc, String email) throws Exception {
        return createUserAndLogIn(mockMvc, email, randomText()).getToken();
    }

    public static TokenWrapper createUserAndLogIn(MockMvc mockMvc, String email, String pass) throws Exception {
        ClientTO clientTO = registerClient(mockMvc, email, pass);
        assertEquals(email, clientTO.getEmail());

        return login(mockMvc, email, pass);
    }

    public static TokenWrapper login(MockMvc mockMvc, String email, String pass) throws Exception {
        String loginTo = asJson(ClientLoginTO.builder().email(email).password(pass).build());
        String tokenWrapper = mockMvc.perform(
                post(LOGIN_ENDPOINT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(loginTo)
        )       .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        TokenWrapper tokenWrapper1 = fromJson(tokenWrapper, TokenWrapper.class);
        return tokenWrapper1;
    }

    public static void loginCheckStatus(MockMvc mockMvc, String email, String pass, int expectedStatus) throws Exception {
        String loginTo = asJson(ClientLoginTO.builder().email(email).password(pass).build());
        mockMvc.perform(
                post(LOGIN_ENDPOINT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(loginTo)
        )       .andExpect(status().is(expectedStatus));
    }

    public static ClientTO registerClient(MockMvc mockMvc, String email, String pass) throws Exception {
        String registerTo = asJson(new ClientRegisterTO(email, pass));
        String response = mockMvc.perform(post(REGISTER_ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(registerTo)
        )
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        return fromJson(response, ClientTO.class);
    }

    public static void forgottenPassword(MockMvc mockMvc, String email) throws Exception {
        mockMvc.perform(post(FORGOT_PASSWORD_ENDPOINT_URL)
                .param("email", email)
        )
                .andExpect(status().isOk());
    }

    public static void changePassword(MockMvc mockMvc, String email, String token, String newPassword) throws Exception {
        mockMvc.perform(post(RESET_PASSWORD_ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(asJson(new ClientPasswordResetTO(email, token, newPassword)))
        )
                .andExpect(status().isOk());
    }
}
