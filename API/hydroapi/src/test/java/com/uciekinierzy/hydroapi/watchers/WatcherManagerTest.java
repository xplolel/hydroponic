package com.uciekinierzy.hydroapi.watchers;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class WatcherManagerTest {
    @Test
    public void watcherManagerFlowTest() throws InterruptedException {
        SystemActivityWatcher systemActivityWatcher = Mockito.mock(SystemActivityWatcher.class);
        OldMeasuresWatcher oldMeasuresWatcher = Mockito.mock(OldMeasuresWatcher.class);
        WatcherManager watcherManager = new WatcherManager(oldMeasuresWatcher, systemActivityWatcher);
        WatcherManager.SLEEPTIME = 500;

        AtomicInteger calledOMW = new AtomicInteger(0);
        AtomicInteger calledSAW = new AtomicInteger(0);

        Mockito.when(oldMeasuresWatcher.call()).thenReturn(() -> {
            log.info("called oldMeasuresWatcher"); calledOMW.incrementAndGet();});
        Mockito.when(systemActivityWatcher.call()).thenReturn(() -> {
            log.info("called systemActivityWatcher"); calledSAW.incrementAndGet();});

        Mockito.when(oldMeasuresWatcher.getIntervalInMinutes()).thenReturn(0);
        Mockito.when(systemActivityWatcher.getIntervalInMinutes()).thenReturn(0);

        watcherManager.init();

        Thread.sleep(200);
        Assert.assertEquals(1, calledOMW.get());
        Assert.assertEquals(1, calledSAW.get());

        Mockito.when(oldMeasuresWatcher.getIntervalInMinutes()).thenReturn(5);
        Mockito.when(systemActivityWatcher.getIntervalInMinutes()).thenReturn(5);

        Thread.sleep(1600);

        Assert.assertEquals(2, calledOMW.get());
        Assert.assertEquals(2, calledSAW.get());
    }
}
