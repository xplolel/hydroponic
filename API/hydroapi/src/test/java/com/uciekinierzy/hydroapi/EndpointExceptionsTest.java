package com.uciekinierzy.hydroapi;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Slf4j
public class EndpointExceptionsTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test400() throws Exception {
        mockMvc.perform(
                get(TestController.TEST_CONTROLLER_400)
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void test403() throws Exception {
        mockMvc.perform(
                get(TestController.TEST_CONTROLLER_403)
        ).andExpect(status().isForbidden());

    }

    @Test
    public void test404() throws Exception {
        mockMvc.perform(
                get(TestController.TEST_CONTROLLER_404)
        ).andExpect(status().isNotFound());

    }


    @Test
    public void test409() throws Exception {
        mockMvc.perform(
                get(TestController.TEST_CONTROLLER_409)
        ).andExpect(status().isConflict());

    }

}
