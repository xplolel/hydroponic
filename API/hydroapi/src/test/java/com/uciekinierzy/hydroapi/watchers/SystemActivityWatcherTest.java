package com.uciekinierzy.hydroapi.watchers;

import com.google.common.collect.Maps;
import com.uciekinierzy.hydroapi.svcnotifications.NotificationAddingTO;
import com.uciekinierzy.hydroapi.svcnotifications.NotificationService;
import com.uciekinierzy.hydroapi.svcsystems.system.SystemService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

public class SystemActivityWatcherTest {
    private Map<String, NotificationAddingTO> notifications = Maps.newHashMap();

    @Before
    public void setUp() throws Exception {
        SystemActivityWatcher.appStartTime = LocalDateTime.now().minusMinutes(1).minusSeconds(1);
    }

    @Test
    public void testCheckingSystemsActivity() {
        NotificationService notificationService = Mockito.mock(NotificationService.class);
        SystemService systemService = Mockito.mock(SystemService.class);
        WatcherService watcherService = Mockito.mock(WatcherService.class);

        Map<String, LocalDateTime> map = Maps.newHashMap();
        map.put("2", LocalDateTime.now().minusSeconds(29));
        map.put("3", LocalDateTime.now().minusMinutes(1));
        map.put("4", LocalDateTime.now().minusSeconds(61));
        map.put("6", LocalDateTime.now().minusMinutes(5));
        map.put("7", LocalDateTime.now().minusMonths(3));

        Mockito.when(watcherService.getSystemsLastLooper()).thenReturn(map);

        Mockito.when(notificationService.postWithoutAuth(any())).thenAnswer(a -> {
            NotificationAddingTO notification = (NotificationAddingTO) a.getArguments()[0];
            notifications.put(notification.getSystemId(), notification);
                    return null;
            }
        );

        Mockito.when(systemService.isSystemRegisteredAndShouldItReportMeasures(any())).thenAnswer(
                a -> {
                    String sid = (String) a.getArguments()[0];
                    if(sid.equals("6") || sid.equals("7"))
                        return false;
                    else
                        return true;
                }
        );

        SystemActivityWatcher wa = new SystemActivityWatcher(notificationService, systemService, watcherService);

        wa.call().run();

        assertTrue(notifications.containsKey("3"));
        assertTrue(notifications.containsKey("4"));
        assertEquals(2, notifications.keySet().size());
    }
}
