package com.uciekinierzy.hydroapi.svcmeasures;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class MeasureServiceTest {
    @Test
    public void testGettingLastMeasure() {
        SystemMeasureRepo systemMeasureRepo = Mockito.mock(SystemMeasureRepo.class);
        ModuleMeasureRepo moduleMeasureRepo = Mockito.mock(ModuleMeasureRepo.class);
        MeasureService measureService = new MeasureService(systemMeasureRepo, moduleMeasureRepo);
        LocalDateTime now = LocalDateTime.now();

        List<SystemMeasure> l1 = Lists.newArrayList(
                SystemMeasure.builder().systemId("1").reportedTime(now.minusMinutes(30)).humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("1").reportedTime(now.minusHours(2)).humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("1").reportedTime(now.minusSeconds(2)).humidity(35.1f).temperature(20.1f).build()
        );

        List<SystemMeasure> l2 = Lists.newArrayList(
                SystemMeasure.builder().systemId("2").reportedTime(now.minusMinutes(30)).humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("2").reportedTime(now.minusHours(2)).humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("2").reportedTime(now.minusMinutes(60)).humidity(35.1f).temperature(20.1f).build()
        );

        List<SystemMeasure> l3 = Lists.newArrayList();
        l3.addAll(l1);
        l3.addAll(l2);

        when(systemMeasureRepo.findAll()).thenReturn(l3);

        when(systemMeasureRepo.findDistinctSystemIds()).thenReturn(
                Lists.newArrayList("1", "2")
        );

        when(systemMeasureRepo.findAllBySystemId(any())).thenAnswer(
                a -> {
                    String s = (String) a.getArguments()[0];
                    if("1".equals(s))
                        return l1;
                    else if("2".equals(s))
                        return l2;
                    else
                        return null;
                }
        );

        Map<String, Optional<LocalDateTime>> lastMeasuresTimes = measureService.getSystemsLastMeasureTimes();

        assertTrue(lastMeasuresTimes.containsKey("1"));
        assertTrue(lastMeasuresTimes.containsKey("2"));

        assertTrue(lastMeasuresTimes.get("1").isPresent());
        assertTrue(lastMeasuresTimes.get("2").isPresent());

        assertEquals(now.minusSeconds(2), lastMeasuresTimes.get("1").get());
        assertEquals(now.minusMinutes(30), lastMeasuresTimes.get("2").get());
    }

    @Test
    public void testDeletingOld() {
        SystemMeasureRepo systemMeasureRepo = Mockito.mock(SystemMeasureRepo.class);
        ModuleMeasureRepo moduleMeasureRepo = Mockito.mock(ModuleMeasureRepo.class);
        MeasureService measureService = new MeasureService(systemMeasureRepo, moduleMeasureRepo);

        when(systemMeasureRepo.findAll()).thenReturn(
                Lists.newArrayList(
                        SystemMeasure.builder().reportedTime(LocalDateTime.now().minusMinutes(30)).build(),
                        SystemMeasure.builder().reportedTime(LocalDateTime.now().minusMonths(2)).build(),
                        SystemMeasure.builder().reportedTime(LocalDateTime.now().minusMonths(2).minusDays(20)).build(),
                        SystemMeasure.builder().reportedTime(LocalDateTime.now().plusYears(2)).build()
                )
        );

        Mockito.doAnswer(a -> {
            List<SystemMeasure> deleted = (List<SystemMeasure>) a.getArguments()[0];
            assertTrue(deleted.isEmpty());
            return null;
        }).when(systemMeasureRepo).deleteAll();

        measureService.deleteOld();


        when(systemMeasureRepo.findAll()).thenReturn(
                Lists.newArrayList(
                        SystemMeasure.builder().reportedTime(LocalDateTime.now().minusMonths(6).minusDays(1)).build(),
                        SystemMeasure.builder().reportedTime(LocalDateTime.now().minusMonths(12)).build(),
                        SystemMeasure.builder().reportedTime(LocalDateTime.now().minusYears(5)).build()
                )
        );

        Mockito.doAnswer(a -> {
            List<SystemMeasure> deleted = (List<SystemMeasure>) a.getArguments()[0];
            assertFalse(deleted.isEmpty());
            assertEquals(3, deleted.size());
            return null;
        }).when(systemMeasureRepo).deleteAll();

        measureService.deleteOld();
    }
}
