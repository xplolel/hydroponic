package com.uciekinierzy.hydroapi.svcclients.clients;

import org.junit.Test;
import org.mockito.Mockito;

import static com.uciekinierzy.hydroapi.utils.JwtUtils.getEmailFromTokenWithoutAuth;
import static com.uciekinierzy.hydroapi.utils.JwtUtils.getIdFromTokenWithoutAuth;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

public class ClientServiceTest {
    @Test
    public void buildToken() {
        ClientService clientService = Mockito.mock(ClientService.class);
        Mockito.when(clientService.buildToken(any())).thenCallRealMethod();

        String salt = "12345678".repeat(8); // 64 byte salt
        String email = "xplolel@gmail.com";

        String token = clientService
                .buildToken(Client.builder()
                        .id("AJDI")
                .email(email)
                .salt(salt)
                .build());


        assertEquals(email, getEmailFromTokenWithoutAuth(token));
        assertEquals("AJDI", getIdFromTokenWithoutAuth(token));
    }

    @Test
    public void passwordResetTokenTest() {
        System.out.println(ClientService.generateToken());
    }
}
