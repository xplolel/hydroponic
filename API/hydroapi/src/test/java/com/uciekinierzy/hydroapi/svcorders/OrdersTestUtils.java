package com.uciekinierzy.hydroapi.svcorders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uciekinierzy.hydroapi.constants.Endpoints;
import lombok.extern.slf4j.Slf4j;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.uciekinierzy.hydroapi.utils.JsonUtils.asJson;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.fromJson;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class OrdersTestUtils {
    public static OrderTO createOrder(OrderAddingTO orderAddingTO, MockMvc mockMvc, String jwt) throws Exception {
        OrderTO order = fromJson(mockMvc.perform(
                post(Endpoints.ORDERS_PATH)
                        .header("Authorization", "Bearer " + jwt)
                        .contentType(APPLICATION_JSON)
                        .content(asJson(orderAddingTO))
                ).andExpect(status().isCreated())
                        .andReturn().getResponse().getContentAsString(),
                OrderTO.class);

        return order;
    }

    public static List<OrderTO> getOrders(MockMvc mockMvc, String jwt) throws Exception {
        return getOrders(mockMvc, jwt, null);
    }
    public static List<OrderTO> getOrders(MockMvc mockMvc, String jwt, Boolean notExecutedOnly) throws Exception {
        return fromJson(mockMvc.perform(
                get(Endpoints.ORDERS_PATH + (notExecutedOnly != null && notExecutedOnly ? "?notExecutedOnly=true" : ""))
                        .header("Authorization", "Bearer " + jwt)
                ).andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString(),
                new TypeReference<List<OrderTO>>() {
                });
    }

    public static void executeOrder(MockMvc mockMvc, String jwt, String id) throws Exception {
        mockMvc.perform(
                patch(Endpoints.ORDERS_ID_PATH.replace("{id}", id))
                        .header("Authorization", "Bearer " + jwt)
        ).andExpect(status().isOk());
    }
}
