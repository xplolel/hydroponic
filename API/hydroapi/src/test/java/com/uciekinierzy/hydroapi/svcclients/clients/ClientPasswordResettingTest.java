package com.uciekinierzy.hydroapi.svcclients.clients;

import com.uciekinierzy.hydroapi.configuration.PropsProvider;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientPasswordResetTO;
import com.uciekinierzy.hydroapi.svcclients.controllers.TokenWrapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static com.uciekinierzy.hydroapi.constants.Endpoints.APPLICATION_JSON;
import static com.uciekinierzy.hydroapi.constants.Endpoints.RESET_PASSWORD_ENDPOINT_URL;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.asJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@AutoConfigureMockMvc

public class ClientPasswordResettingTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClientService clientService;

    @Autowired
    private TokenRepo tokenRepo;

    @Test
    public void testResettingPassword_ok_service() throws Exception {
        String email = "lala11@lala.com";
        String password = "lilulala";

        TokenWrapper tokenWrapper = ClientTestUtils.createUserAndLogIn(mockMvc, email, password);

        String link = clientService.generateAndReturnResetPasswordLink(email);
        Assert.assertNotNull(link);
        Assert.assertTrue(link.contains(PropsProvider.getWebResetPasswordEndpoint()));
        Assert.assertTrue(link.contains("?token="));

        String token = link.substring(link.indexOf("?token=") + "?token=".length());
        Assert.assertTrue(clientService.checkResetToken(token));

        String newPassword = "lilulalaNEW";
        clientService.changePassword(token, email, newPassword);

        ClientTestUtils.login(mockMvc, email, newPassword);
    }

    @Test
    public void testResettingPassword_ok_endpoint() throws Exception {
        String email = "lala12@lala.com";
        String password = "lilulala";

        TokenWrapper tokenWrapper = ClientTestUtils.createUserAndLogIn(mockMvc, email, password);

        ClientTestUtils.forgottenPassword(mockMvc, email);

        PasswordResetToken next = tokenRepo.findAll().iterator().next();
        String token = next.getToken();
        String newPassword = "lilulalaNEW";

        ClientTestUtils.changePassword(mockMvc, email, token, newPassword);

        ClientTestUtils.login(mockMvc, email, newPassword);
    }

    @Test
    public void testResettingPassword_expiredAndNoSuchUser_endpoint() throws Exception {
        tokenRepo.deleteAll();

        String email = "lala13@lala.com";
        String password = "lilulala";

        ClientTestUtils.createUserAndLogIn(mockMvc, email, password);

        ClientTestUtils.forgottenPassword(mockMvc, email);

        PasswordResetToken next = tokenRepo.findAll().iterator().next();
        String token = next.getToken();

        next.setExpires(LocalDateTime.now().minusHours(PropsProvider.getResetPasswordTokenHourlyTimeout()).minusMinutes(1));
        tokenRepo.save(next);

        String newPassword = "lilulalaNEW";

        mockMvc.perform(post(RESET_PASSWORD_ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(asJson(new ClientPasswordResetTO(email, token, newPassword)))
        )
                .andExpect(status().isBadRequest());

        ClientTestUtils.loginCheckStatus(mockMvc, email, password, 200);
        ClientTestUtils.loginCheckStatus(mockMvc, email, newPassword, 400);
    }
}
