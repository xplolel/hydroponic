package com.uciekinierzy.hydroapi.utils;

import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.uciekinierzy.hydroapi.utils.PaginationUtils.paginateCollection;
import static com.uciekinierzy.hydroapi.utils.PaginationUtils.paginateList;
import static org.junit.Assert.*;

public class PaginationUtilsTest {
    @Test
    public void paginateList_test() {
        ArrayList<String> strings = Lists.newArrayList("A", "B", "C");
        List<String> strings1 = paginateList(strings, null, null);
        //tada
    }

    @Test
    public void paginateCollection_testNullParams() {
        ArrayList<String> strings = Lists.newArrayList("A", "B", "C");

        Collection<String> strings1 = paginateCollection(strings, null, null);
        assertNotSame(strings, strings1);
        assertEquals(3, strings1.size());
        assertTrue(strings1.contains("A"));
        assertTrue(strings1.contains("B"));
        assertTrue(strings1.contains("C"));
    }

    @Test
    public void paginateCollection_testContents() {
        ArrayList<String> strings = Lists.newArrayList("A", "B", "C");

        Collection<String> strings1 = paginateCollection(strings, 10, 0);
        assertNotSame(strings, strings1);
        assertEquals(3, strings1.size());
        assertTrue(strings1.contains("A"));
        assertTrue(strings1.contains("B"));
        assertTrue(strings1.contains("C"));
    }

    @Test
    public void paginateCollection_testLogic() {
        List<Integer> ints = IntStream.range(0, 100).boxed().collect(Collectors.toList());
        {
            Collection<Integer> ints1 = paginateCollection(ints, 10, 0);
            assertEquals(10, ints1.size());
            assertTrue(ints1.contains(9));
            assertTrue(ints1.contains(0));
        }
        {
            Collection<Integer> ints1 = paginateCollection(ints, 10, 10);
            assertEquals(10, ints1.size());
            assertTrue(ints1.contains(10));
            assertTrue(ints1.contains(19));
        }
        {
            Collection<Integer> ints1 = paginateCollection(ints, 5, 10);
            assertEquals(5, ints1.size());
            assertTrue(ints1.contains(10));
            assertTrue(ints1.contains(14));
        }
        {
            Collection<Integer> ints1 = paginateCollection(ints, 10, 99);
            assertEquals(1, ints1.size());
            assertTrue(ints1.contains(99));
        }
        {
            Collection<Integer> ints1 = paginateCollection(ints, 10, 97);
            assertEquals(3, ints1.size());
            assertTrue(ints1.contains(99));
            assertTrue(ints1.contains(98));
            assertTrue(ints1.contains(97));
        }
        {
            Collection<Integer> ints1 = paginateCollection(ints, 1000, 0);
            assertEquals(100, ints1.size());
            assertTrue(ints1.contains(99));
            assertTrue(ints1.contains(0));
        }
    }


}
