package com.uciekinierzy.hydroapi.svcsystems;

import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SettingTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemRegistrationResponseTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.uciekinierzy.hydroapi.constants.Endpoints.AUTHORIZATION;
import static com.uciekinierzy.hydroapi.constants.Endpoints.SETTINGS_SYSTEM_ID_PATH;
import static com.uciekinierzy.hydroapi.svcclients.clients.ClientTestUtils.createUserAndLogIn;
import static com.uciekinierzy.hydroapi.svcsystems.SystemTestUtils.*;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.asJson;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@Slf4j
public class SettingsTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void basicTest() throws Exception {
        String jwt = createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        String moduleId = createModule(jwt, systemId, mockMvc).getId();
        String moduleId2 = createModule(jwt, systemId, mockMvc).getId();

        List<SettingTO> settings = getSettings(systemId, jwt, mockMvc);
        assertFalse(settings.isEmpty()); // there are default one

        SettingTO settingModule1 = postSetting(systemId, jwt, mockMvc, SettingTO.builder()
                .halfLight(true)
                .fromHourUTC(18)
                .toHourUTC(8)
                .fromMinuteUTC(0)
                .toMinuteUTC(0)
                .moduleId(moduleId)
                .build());

        assertTrue(getSettings(systemId, jwt, mockMvc).contains(settingModule1));

        SettingTO settingModule2 = postSetting(systemId, jwt, mockMvc, SettingTO.builder()
                .halfLight(false)
                .fromHourUTC(18)
                .toHourUTC(8)
                .fromMinuteUTC(5)
                .toMinuteUTC(5)
                .moduleId(moduleId2)
                .build());

        assertTrue(getSettings(systemId, jwt, mockMvc).contains(settingModule2));

        SettingTO setting = getSetting(moduleId2, jwt, mockMvc);
        assertEquals(settingModule2, setting);
    }

    @Test
    public void testConstraint() throws Exception {
        String jwt = createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        String moduleId = createModule(jwt, systemId, mockMvc).getId();

        SettingTO settingTO = SettingTO.builder()
                .halfLight(false)
                .fromHourUTC(-1)
                .toHourUTC(0)
                .fromMinuteUTC(0)
                .toMinuteUTC(0)
                .moduleId(moduleId)
                .build();

        List<SettingTO> settings = getSettings(systemId, jwt, mockMvc);
        assertFalse(settings.isEmpty()); // default ones

        mockMvc.perform(
                post(SETTINGS_SYSTEM_ID_PATH.replace("{systemId}", systemId))
                .header(AUTHORIZATION, "Bearer " + jwt)
                .content(asJson(settingTO))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isBadRequest());

        settingTO.setFromHourUTC(0);
        settingTO.setHalfLight(null);

        mockMvc.perform(
                post(SETTINGS_SYSTEM_ID_PATH.replace("{systemId}", systemId))
                .header(AUTHORIZATION, "Bearer " + jwt)
                .content(asJson(settingTO))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isBadRequest());

        settingTO.setHalfLight(true);
        settingTO.setToMinuteUTC(60);

        mockMvc.perform(
                post(SETTINGS_SYSTEM_ID_PATH.replace("{systemId}", systemId))
                .header(AUTHORIZATION, "Bearer " + jwt)
                .content(asJson(settingTO))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isBadRequest());

        settingTO.setToMinuteUTC(0);
        settingTO.setToHourUTC(25);
        mockMvc.perform(
                post(SETTINGS_SYSTEM_ID_PATH.replace("{systemId}", systemId))
                .header(AUTHORIZATION, "Bearer " + jwt)
                .content(asJson(settingTO))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)

        )
                .andExpect(status().isBadRequest());

        settingTO.setToHourUTC(0);
        mockMvc.perform(
                post(SETTINGS_SYSTEM_ID_PATH.replace("{systemId}", systemId))
                .header(AUTHORIZATION, "Bearer " + jwt)
                .content(asJson(settingTO))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isOk());
    }
}
