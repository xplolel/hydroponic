package com.uciekinierzy.hydroapi.svcplants;

import com.uciekinierzy.hydroapi.constants.Endpoints;
import com.uciekinierzy.hydroapi.svcclients.clients.ClientTestUtils;
import com.uciekinierzy.hydroapi.svcplants.plants.GrowingDifficultyEnum;
import com.uciekinierzy.hydroapi.svcplants.plants.PlantTypeEnum;
import com.uciekinierzy.hydroapi.svcplants.plants.TOs.AddingPlantTO;
import com.uciekinierzy.hydroapi.svcplants.plants.TOs.PlantTO;
import com.uciekinierzy.hydroapi.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.FileInputStream;

import static com.uciekinierzy.hydroapi.utils.JsonUtils.asJson;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.fromJson;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@AutoConfigureMockMvc
public class PlantsTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void addPlant() throws Exception {
        String jwt = ClientTestUtils.createUserAndLogIn(mockMvc);

        AddingPlantTO bazylius = AddingPlantTO.builder()
                .growDifficulty(GrowingDifficultyEnum.EASY)
                .growTime(30)
                .name("Bazylius")
                .plantType(PlantTypeEnum.HOME_PLANT)
                .build();

        String json = mockMvc.perform(
                post(Endpoints.ALL_PLANTS_PATH)
                        .header("Authorization", "Bearer " + jwt)
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(asJson(bazylius))
        )
                .andExpect(status().isCreated())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        PlantTO plantTO = fromJson(json, PlantTO.class);

        assertEquals(bazylius.getGrowDifficulty(), plantTO.getGrowDifficulty());
        assertEquals(bazylius.getGrowTime(), plantTO.getGrowTime());
        assertEquals(bazylius.getName(), plantTO.getName());
        assertEquals(bazylius.getPlantType(), plantTO.getPlantType());
        assertEquals(JwtUtils.getIdFromTokenWithoutAuth(jwt), plantTO.getCreatedBy());
        assertNull(plantTO.getFullImageUrl());
        assertNull(plantTO.getMediumImageUrl());
        assertNull(plantTO.getSmallImageUrl());

        log.info(System.getProperty("user.dir"));
        MockMultipartFile multipartFile = new MockMultipartFile("file", new FileInputStream("src/test/resources/bazylia.jpg"));

        mockMvc.perform(
                fileUpload(Endpoints.PLANTS_ID_UPLOAD_IMAGE_PATH.replace("{id}", plantTO.getId()))
                        .file(multipartFile)
                        .header("Authorization", "Bearer " + jwt)
                        .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
        )
                .andExpect(status().isOk());

        plantTO = fromJson(mockMvc.perform(
                get(Endpoints.PLANTS_ID_PATH.replace("{id}", plantTO.getId()))
                        .header("Authorization", "Bearer " + jwt)
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), PlantTO.class);

        assertNotNull(plantTO.getFullImageUrl());
        assertNotNull(plantTO.getMediumImageUrl());
        assertNotNull(plantTO.getSmallImageUrl());

        mockMvc.perform(get(Endpoints.PLANTS_ID_IMAGE_FULL_PATH.replace("{id}", plantTO.getId()))
        )
                .andExpect(status().isOk());

        mockMvc.perform(get(Endpoints.PLANTS_ID_IMAGE_MEDIUM_PATH.replace("{id}", plantTO.getId()))
        )
                .andExpect(status().isOk());


        mockMvc.perform(get(Endpoints.PLANTS_ID_IMAGE_SMALL_PATH.replace("{id}", plantTO.getId()))
        )
                .andExpect(status().isOk());

//        some day for deleting
//        mockMvc.perform(
//                post(Endpoints.PLANTS.replace("{id}", plantTO.getId()))
//                        .header("Authorization", "Bearer " + jwt)
//                        .content(multipartFile.getBytes())
//        )
//                .andExpect(status().isOk());
//
//
//        plantTO = fromJson(mockMvc.perform(
//                get(Endpoints.PLANTS_ID_PATH.replace("{id}", plantTO.getId()))
//                        .header("Authorization", "Bearer " + jwt)
//        )
//                .andExpect(status().isOk())
//                .andReturn().getResponse().getContentAsString(), PlantTO.class);
//
//        assertNull(plantTO.getFullImageUrl());
//        assertNull(plantTO.getMediumImageUrl());
//        assertNull(plantTO.getSmallImageUrl());
//
//        mockMvc.perform(get(Endpoints.PLANTS_ID_IMAGE_FULL_PATH.replace("{id}", plantTO.getId()))
//                .header("Authorization", "Bearer " + jwt))
//                .andExpect(status().isOk());
//
//        mockMvc.perform(get(Endpoints.PLANTS_ID_IMAGE_MEDIUM_PATH.replace("{id}", plantTO.getId()))
//                .header("Authorization", "Bearer " + jwt))
//                .andExpect(status().isOk());
//
//
//        mockMvc.perform(get(Endpoints.PLANTS_ID_IMAGE_SMALL_PATH.replace("{id}", plantTO.getId()))
//                .header("Authorization", "Bearer " + jwt))
//                .andExpect(status().isOk());
    }
}
