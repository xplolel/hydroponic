package com.uciekinierzy.hydroapi.svcmailing;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailingServiceTest {
    @Autowired
    MailingService mailingService;
    @Autowired
    MailingHelper mailingHelper;

    @Test
    public void testMailing() throws IncorrectMailFormat, MailingException {
        System.out.println(System.getenv("sgapikey"));

        mailingService.sendMail(MailTO.builder()
                .to("xplolel+spam@gmail.com")
                .content("Junit")
                .subject("Junit")
                .build());
    }

    @Test
    public void testGettingWelcomeEmail() {
        Assert.assertFalse(mailingHelper.getWelcomeContent("en").isEmpty());
        Assert.assertFalse(mailingHelper.getWelcomeContent("pl").isEmpty());

        Assert.assertNotEquals("Welcome", mailingHelper.getWelcomeContent("en"));
        Assert.assertNotEquals("Welcome", mailingHelper.getWelcomeContent("pl"));
    }

    @Test
    public void testGettingPasswordResetEmail() throws MailingTemplateException {
        Assert.assertFalse(mailingHelper.getPasswordResetContent("en", "lala").isEmpty());
        Assert.assertFalse(mailingHelper.getPasswordResetContent("pl", "lala").isEmpty());
    }
}
