package com.uciekinierzy.hydroapi.watchers;

import com.google.common.collect.Maps;
import com.uciekinierzy.hydroapi.svcmeasures.MeasureService;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

public class OldMeasuresWatcherTest {
    @Test
    public void testDeletingOnlyOldMeasures() {
        MeasureService measureService = Mockito.mock(MeasureService.class);
        OldMeasuresWatcher watcher = new OldMeasuresWatcher(measureService);

        Map<String, Optional<LocalDateTime>> map = Maps.newHashMap();

        Mockito.when(measureService.getSystemsLastMeasureTimes()).thenReturn(map);

        watcher.call().run();
    }
}
