package com.uciekinierzy.hydroapi;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Slf4j
public class SwaggerDocsTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void checkUiHtml() throws Exception {
        mockMvc.perform(
                get("/swagger-ui.html")
        ).andExpect(status().isOk());
    }
}
