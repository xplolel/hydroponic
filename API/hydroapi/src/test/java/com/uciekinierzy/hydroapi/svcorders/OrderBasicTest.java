package com.uciekinierzy.hydroapi.svcorders;

import com.uciekinierzy.hydroapi.svcclients.clients.ClientTestUtils;
import com.uciekinierzy.hydroapi.svcsystems.SystemTestUtils;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemRegistrationResponseTO;
import com.uciekinierzy.hydroapi.watchers.WatcherService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;

import static com.uciekinierzy.hydroapi.svcorders.OrderType.FULL_LIGHT_OFF;
import static com.uciekinierzy.hydroapi.svcorders.OrdersTestUtils.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@AutoConfigureMockMvc
public class OrderBasicTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WatcherService watcherService;

    @Test
    public void checkOrders_checkWatcherService() throws Exception{
        String jwt = ClientTestUtils.createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);

        assertNull(watcherService.getLastSystemCall(system.getSystem().getId()));

        getOrders(mockMvc, system.getToken());

        assertNotNull(watcherService.getLastSystemCall(system.getSystem().getId()));
    }

    @Test
    public void createOrder_check() throws Exception {
        String jwt = ClientTestUtils.createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        String moduleId = SystemTestUtils.createModule(jwt, systemId, mockMvc).getId();

        LocalDateTime then = LocalDateTime.now();
        String shortNote = "PupaMa";

        OrderTO order = createOrder(
                OrderAddingTO.builder()
                        .shortNote(shortNote)
                        .created(then)
                        .moduleId(moduleId)
                        .orderType(FULL_LIGHT_OFF)
                        .build(),
                mockMvc,
                jwt);

        assertEquals(moduleId, order.getModuleId());
        assertEquals(shortNote, order.getShortNote());
        assertEquals(then, order.getCreated());
        assertEquals(false, order.getExecuted());
        assertEquals(FULL_LIGHT_OFF, order.getOrderType());
    }

    @Test
    public void createOrder_get_execute_getNotExecutedOnly() throws Exception {
        String jwt = ClientTestUtils.createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        String moduleId = SystemTestUtils.createModule(jwt, systemId, mockMvc).getId();

        OrderAddingTO order = OrderAddingTO.builder()
                        .shortNote("Mamoooo")
                        .moduleId(moduleId)
                        .orderType(FULL_LIGHT_OFF)
                        .build();

        OrderTO order1 = createOrder(order, mockMvc, jwt);
        OrderTO order2 = createOrder(order, mockMvc, jwt);
        OrderTO order3 = createOrder(order, mockMvc, jwt);

        List<OrderTO> orders = getOrders(mockMvc, jwt);

        assertEquals(3L, orders.size());
        assertEquals(order1, orders.get(0));
        assertEquals(order2, orders.get(1));
        assertEquals(order3, orders.get(2));

        executeOrder(mockMvc, jwt, order2.getId());

        orders = getOrders(mockMvc, jwt);

        assertEquals(3L, orders.size());
        assertEquals(order1, orders.get(0));
        assertNotEquals(order2, orders.get(1));
        assertEquals(order3, orders.get(2));

        assertFalse(orders.get(0).getExecuted());
        assertTrue(orders.get(1).getExecuted());
        assertFalse(orders.get(2).getExecuted());

        orders = getOrders(mockMvc, jwt, true);
        assertEquals(2, orders.size());
    }
}
