package com.uciekinierzy.hydroapi.svcmeasures;

import com.google.common.collect.Lists;
import com.uciekinierzy.hydroapi.svcclients.clients.ClientTestUtils;
import com.uciekinierzy.hydroapi.svcsystems.SystemTestUtils;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemRegistrationResponseTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import static com.uciekinierzy.hydroapi.constants.Endpoints.SYSTEM_MEASURES_PATH;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.asJson;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.fromJson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@AutoConfigureMockMvc
public class MeasuresTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MeasureService measureService;

    @Test
    public void postMeasure() throws Exception {
        String jwt = ClientTestUtils.createUserAndLogIn(mockMvc);
        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemToken = system.getToken();
        String systemId = system.getSystem().getId();

        SystemMeasure originalSystemMeasure = SystemMeasure.builder()
                .humidity(85.6F)
                .systemId(systemId)
                .temperature(27F)
                .reportedTime(LocalDateTime.now())
                .build();

        SystemMeasure systemMeasure = postMeasure(systemToken, originalSystemMeasure, mockMvc);

        assertEquals(originalSystemMeasure.getHumidity(), systemMeasure.getHumidity());
        assertEquals(originalSystemMeasure.getSystemId(), systemMeasure.getSystemId());
        assertEquals(originalSystemMeasure.getTemperature(), systemMeasure.getTemperature());
        assertEquals(originalSystemMeasure.getReportedTime(), systemMeasure.getReportedTime());
    }

    @Test
    public void getMeasuresBySystemId() throws Exception {
        String jwt = ClientTestUtils.createUserAndLogIn(mockMvc);
        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemToken = system.getToken();
        String systemId = system.getSystem().getId();

        SystemMeasure originalSystemMeasure1 = SystemMeasure.builder()
                .humidity(85.6F)
                .systemId(systemId)
                .temperature(27F)
                .reportedTime(LocalDateTime.now())
                .build();

        SystemMeasure originalSystemMeasure2 = SystemMeasure.builder()
                .humidity(83.6F)
                .systemId(systemId)
                .temperature(28F)
                .reportedTime(LocalDateTime.now().plus(3, ChronoUnit.MINUTES))
                .build();

        SystemMeasure originalSystemMeasure3 = SystemMeasure.builder()
                .humidity(80.1F)
                .systemId(systemId)
                .temperature(29F)
                .reportedTime(LocalDateTime.now().plus(6, ChronoUnit.MINUTES))
                .build();

        ArrayList<SystemMeasure> systemMeasures = Lists.newArrayList(
                postMeasure(systemToken, originalSystemMeasure1, mockMvc),
                postMeasure(systemToken, originalSystemMeasure2, mockMvc),
                postMeasure(systemToken, originalSystemMeasure3, mockMvc)
        );


        for (SystemMeasure systemMeasure : systemMeasures)
            assertEquals(systemId, systemMeasure.getSystemId());

        assertTrue(checkIfSomeMeasureFulfills(systemMeasures, m -> m.getHumidity().equals(originalSystemMeasure1.getHumidity())));
        assertTrue(checkIfSomeMeasureFulfills(systemMeasures, m -> m.getHumidity().equals(originalSystemMeasure2.getHumidity())));
        assertTrue(checkIfSomeMeasureFulfills(systemMeasures, m -> m.getHumidity().equals(originalSystemMeasure3.getHumidity())));

        assertTrue(checkIfSomeMeasureFulfills(systemMeasures, m -> m.getTemperature().equals(originalSystemMeasure3.getTemperature())));
        assertTrue(checkIfSomeMeasureFulfills(systemMeasures, m -> m.getTemperature().equals(originalSystemMeasure2.getTemperature())));
        assertTrue(checkIfSomeMeasureFulfills(systemMeasures, m -> m.getTemperature().equals(originalSystemMeasure1.getTemperature())));

        assertTrue(checkIfSomeMeasureFulfills(systemMeasures, m -> m.getReportedTime().equals(originalSystemMeasure1.getReportedTime())));
        assertTrue(checkIfSomeMeasureFulfills(systemMeasures, m -> m.getReportedTime().equals(originalSystemMeasure2.getReportedTime())));
        assertTrue(checkIfSomeMeasureFulfills(systemMeasures, m -> m.getReportedTime().equals(originalSystemMeasure3.getReportedTime())));
    }

    @Test
    public void name() {
        String systemId = "LALALLA";

        measureService.add(SystemMeasure.builder()
                .systemId(systemId)
                .humidity(5.5f)
                .temperature(25.5f)
                .reportedTime(LocalDateTime.now().minusMonths(7))
                .build());
        measureService.deleteOld();
        assertEquals(0, measureService.getSystemMeasures(systemId).size());

        measureService.add(SystemMeasure.builder()
                .systemId(systemId)
                .humidity(5.5f)
                .temperature(25.5f)
                .reportedTime(LocalDateTime.now().minusMonths(6).minusMinutes(1))
                .build());
        measureService.deleteOld();
        assertEquals(0, measureService.getSystemMeasures(systemId).size());

        measureService.add(SystemMeasure.builder()
                .systemId(systemId)
                .humidity(5.5f)
                .temperature(25.5f)
                .reportedTime(LocalDateTime.now().minusMonths(6).plusMinutes(1))
                .build());
        measureService.deleteOld();
        assertEquals(1, measureService.getSystemMeasures(systemId).size());

        measureService.add(SystemMeasure.builder()
                .systemId(systemId)
                .humidity(5.5f)
                .temperature(25.5f)
                .reportedTime(LocalDateTime.now().minusMonths(5))
                .build());
        measureService.deleteOld();
        assertEquals(2, measureService.getSystemMeasures(systemId).size());
    }

    @Test
    public void testModuleMeasure() {
        measureService.addModuleMeasure(ModuleMeasure.builder()
                .id(1L)
                .moduleId("123")
                .waterLevel(WaterLevel.FULL)
                .reportedTime(LocalDateTime.now())
                .build());

        assertEquals( 1, measureService.getModuleMeasures("123").size());
    }

    @Test
    public void testLastMeasureTime() {
        LocalDateTime now = LocalDateTime.now();

        org.assertj.core.util.Lists.newArrayList(
                SystemMeasure.builder().systemId("1").reportedTime(now.minusMinutes(120))     .humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("1").reportedTime(now.minusMinutes(60))      .humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("1").reportedTime(now.minusMinutes(90))      .humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("1").reportedTime(now.minusMinutes(150))     .humidity(35.1f).temperature(20.1f).build(),

                SystemMeasure.builder().systemId("3").reportedTime(now.minusMinutes(90))      .humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("3").reportedTime(now.minusMinutes(60))      .humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("3").reportedTime(now.minusMinutes(1))       .humidity(35.1f).temperature(20.1f).build(),
                SystemMeasure.builder().systemId("3").reportedTime(now.minusMinutes(30))      .humidity(35.1f).temperature(20.1f).build()
        )
                .forEach(measureService::add);

        Map<String, Optional<LocalDateTime>> measureTimes = measureService.getSystemsLastMeasureTimes();

        assertTrue(measureTimes.containsKey("1"));
        assertTrue(measureTimes.containsKey("3"));

        assertTrue(measureTimes.get("1").isPresent());
        assertTrue(measureTimes.get("3").isPresent());

        assertEquals(now.minusMinutes(60), measureTimes.get("1").get());
        assertEquals(now.minusMinutes(1), measureTimes.get("3").get());
    }

    private static boolean checkIfSomeMeasureFulfills(List<SystemMeasure> systemMeasures, Predicate<SystemMeasure> predicate) throws Exception {
        for (SystemMeasure systemMeasure : systemMeasures)
            if(predicate.test(systemMeasure))
                return true;

        return false;
    }

    private static SystemMeasure postMeasure(String token, SystemMeasure systemMeasure, MockMvc mockMvc) throws Exception {
        SystemMeasure result = fromJson(mockMvc.perform(
                post(SYSTEM_MEASURES_PATH)
                        .header("Authorization", "Bearer " + token)
                        .contentType("application/json")
                        .content(asJson(systemMeasure))
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(),
                SystemMeasure.class);

        return result;
    }
}
