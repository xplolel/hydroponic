package com.uciekinierzy.hydroapi.svcnotifications;

import com.uciekinierzy.hydroapi.svcclients.clients.ClientTestUtils;
import com.uciekinierzy.hydroapi.svcsystems.SystemTestUtils;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemRegistrationResponseTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;

import static com.uciekinierzy.hydroapi.svcnotifications.NotificationTestUtils.*;
import static com.uciekinierzy.hydroapi.svcnotifications.NotificationType.FAILURE;
import static com.uciekinierzy.hydroapi.svcnotifications.NotificationType.INFO;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@AutoConfigureMockMvc
public class NotificationsBasicTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private NotificationService notificationService;

    @Test
    public void createNotification_check() throws Exception {
        String jwt = ClientTestUtils.createUserAndLogIn(mockMvc);
        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        LocalDateTime then = LocalDateTime.now();
        String longNote = "Długa dupka jest stara jusz.";
        String shortNote = "Pupa";

        NotificationTO notification = createNotification(system.getToken(),
                NotificationAddingTO.builder()
                        .shortNote(shortNote)
                        .longNote(longNote)
                        .created(then)
                        .notificationType(INFO)
                        .notificationType2(NotificationType2.HARVESTING)
                        .systemId(systemId)
                        .build(),
                mockMvc);

        assertEquals(systemId, notification.getSystemId());
        assertEquals(INFO, notification.getNotificationType());
        assertEquals(shortNote, notification.getShortNote());
        assertEquals(longNote, notification.getLongNote());
        assertEquals(then, notification.getCreated());
        assertEquals(false, notification.getRead());
    }

    @Test
    public void createNotification_get_read_get() throws Exception {
        String jwt = ClientTestUtils.createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();
        String systemToken = system.getToken();

        NotificationTO notification = createNotification(systemToken, getDummyNotificationAddingTO(systemId), mockMvc);
        NotificationTO notification2 = createNotification(systemToken, getDummyNotificationAddingTO(systemId), mockMvc);
        NotificationTO notification3 = createNotification(systemToken, getDummyNotificationAddingTO(systemId), mockMvc);

        List<NotificationTO> notifications = getNotifications(jwt, mockMvc);

        assertEquals(3L, notifications.size());
        assertEquals(3L, notifications.size());
        assertEquals(notification, notifications.get(0));
        assertEquals(notification2, notifications.get(1));
        assertEquals(notification3, notifications.get(2));

        readNotification(jwt, notification.getId(), mockMvc);

        notifications = getNotifications(jwt, mockMvc);
        assertEquals(3L, notifications.size());
        assertEquals(notification2, notifications.get(0));
        assertEquals(notification3, notifications.get(1));
        assertNotEquals(notification, notifications.get(2));
        assertTrue(notifications.get(2).getRead());

        List<NotificationTO> list = getNotifications(jwt, mockMvc, null, null, true, false);
        assertEquals(2, list.size());

        list = getNotifications(jwt, mockMvc, null, null, false, true);
        assertEquals(1, list.size());
    }

    @Test
    public void testRecentlyNotifiedAboutSystemIssue() {
        notificationService.postWithoutAuth(NotificationAddingTO.builder()
                .longNote("long")
                .shortNote("short")
                .notificationType(FAILURE)
                .systemId("1")
                .created(LocalDateTime.now().minusHours(20))
                .build()
        );

        notificationService.postWithoutAuth(NotificationAddingTO.builder()
                .longNote("long")
                .shortNote("short")
                .notificationType(FAILURE)
                .systemId("2")
                .created(LocalDateTime.now().minusHours(11))
                .build()
        );

        notificationService.postWithoutAuth(NotificationAddingTO.builder()
                .longNote("long")
                .shortNote("short")
                .notificationType(FAILURE)
                .systemId("3")
                .created(LocalDateTime.now().minusHours(13))
                .build()
        );

        notificationService.postWithoutAuth(NotificationAddingTO.builder()
                .longNote("long")
                .shortNote("short")
                .notificationType(FAILURE)
                .systemId("3")
                .created(LocalDateTime.now().minusHours(18))
                .build()
        );

        assertTrue(notificationService.recentlyNotified("1", "long", "short", 24));
        assertTrue(notificationService.recentlyNotified("2", "long", "short", 12));
        assertFalse(notificationService.recentlyNotified("3", "long", "short", 12));
        assertFalse(notificationService.recentlyNotified("3", "long", "short", 12));
    }
}
