package com.uciekinierzy.hydroapi;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.exceptions.ResourceOfSuchIdExistsException;
import com.uciekinierzy.hydroapi.svcsystems.helpers.CannotDeleteOwnerPermissionException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController(TestController.TEST_CONTROLLER)
@ApiIgnore
public class TestController {
    public static final String TEST_CONTROLLER = "/test-controller";
    public static final String TEST_CONTROLLER_400 = TEST_CONTROLLER + "/test400";
    public static final String TEST_CONTROLLER_4002 = TEST_CONTROLLER + "/test400-2";
    public static final String TEST_CONTROLLER_403 = TEST_CONTROLLER + "/test403";
    public static final String TEST_CONTROLLER_404 = TEST_CONTROLLER + "/test404";
    public static final String TEST_CONTROLLER_409 = TEST_CONTROLLER + "/test409";

    @GetMapping(TEST_CONTROLLER_400)
    public ResponseEntity get400() throws IncorrectTOException {
        throw new IncorrectTOException();
    }

    @GetMapping(TEST_CONTROLLER_4002)
    public ResponseEntity get4002() throws CannotDeleteOwnerPermissionException {
        throw new CannotDeleteOwnerPermissionException();
    }

    @GetMapping(TEST_CONTROLLER_403)
    public ResponseEntity get403() throws NoAccessException {
        throw new NoAccessException();
    }

    @GetMapping(TEST_CONTROLLER_404)
    public ResponseEntity get404() throws NoSuchResourceException {
        throw new NoSuchResourceException();
    }

    @GetMapping(TEST_CONTROLLER_409)
    public ResponseEntity get409() throws ResourceOfSuchIdExistsException {
        throw new ResourceOfSuchIdExistsException();
    }
}
