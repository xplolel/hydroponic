package com.uciekinierzy.hydroapi.svcsystems;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import com.uciekinierzy.hydroapi.svcsystems.permission.PermissionType;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.*;
import com.uciekinierzy.hydroapi.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;
import static com.uciekinierzy.hydroapi.svcsystems.system.SlotService.SLOTS_IN_ROW;
import static com.uciekinierzy.hydroapi.svcsystems.system.SlotService.SLOTS_ROWS;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.asJson;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.fromJson;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class SystemTestUtils {
    private static final String APPLICATION_JSON = "application/json";
    public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json;charset=UTF-8";

    public static SlotTO deleteSeed(String systemId, String moduleId, String slotId, String jwt, MockMvc mockMvc) throws Exception {
        String slotTOString = mockMvc.perform(
                delete(SLOT_ID_PATH.replace("{systemId}", systemId)
                        .replace("{moduleId}", moduleId)
                        .replace("{slotId}", slotId)
                )
                        .header(AUTHORIZATION, "Bearer " + jwt)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();

        SlotTO slotTO = fromJson(slotTOString, SlotTO.class);

        assertEquals(slotId, slotTO.getId());
        assertNull(slotTO.getPlantId());
        assertNull(slotTO.getPlantPlantedTime());

        return slotTO;
    }

    public static SlotTO plantSeed(String plantId, String systemId, String moduleId, String slotId, String jwt, MockMvc mockMvc) throws Exception {
        LocalDateTime beforeCall = LocalDateTime.now();
        String slotTo = mockMvc.perform(
                put(SLOT_ID_PATH.replace("{systemId}", systemId)
                        .replace("{moduleId}", moduleId)
                        .replace("{slotId}", slotId)
                )
                        .header(AUTHORIZATION, "Bearer " + jwt)
                        .param("plantId", plantId)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();

        SlotTO slotTO = fromJson(slotTo, SlotTO.class);

        assertEquals(slotId, slotTO.getId());
        assertEquals(plantId, slotTO.getPlantId());
        assertEquals(moduleId, slotTO.getModuleId());

        assertTrue(slotTO.getPlantPlantedTime().isAfter(beforeCall));
        assertTrue(slotTO.getPlantPlantedTime().isBefore(LocalDateTime.now()));

        return slotTO;
    }

    public static SlotTO getSlot(String systemId, String moduleId, String slotId, String jwt, MockMvc mockMvc) throws Exception {
        String slotTo = mockMvc.perform(
                get(SLOT_ID_PATH.replace("{systemId}", systemId)
                        .replace("{moduleId}", moduleId)
                        .replace("{slotId}", slotId)
                )
                        .header(AUTHORIZATION, "Bearer " + jwt)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();

        SlotTO slotTO = fromJson(slotTo, SlotTO.class);
        return slotTO;
    }

    public static SystemRegistrationResponseTO createSystem(String jwt, MockMvc mockMvc) throws Exception {
        String id = JwtUtils.getIdFromTokenWithoutAuth(jwt);

        mockMvc.perform(
                get(SYSTEM_ALL_PATH)
                        .header(AUTHORIZATION, "Bearer " + jwt)
        ).andExpect(status().isOk())
                .andExpect(content().string("[]"));

        String systemAdd = asJson(SystemAddingTO.builder()
                .name("TestSystem")
                .ownerId(id)
                .ownerEmail(JwtUtils.getEmailFromTokenWithoutAuth(jwt))
                .build());

        SystemRegistrationResponseTO systemRegistrationResponseTO = fromJson(
                mockMvc.perform(
                        post(SYSTEM_ALL_PATH)
                                .contentType(APPLICATION_JSON)
                                .header(AUTHORIZATION, "Bearer " + jwt)
                                .content(systemAdd)
                )
                        .andExpect(status().isCreated())
                        .andReturn().getResponse().getContentAsString()
                , SystemRegistrationResponseTO.class);

        assertEquals(id, systemRegistrationResponseTO.getSystem().getOwnerId());
        assertEquals("TestSystem", systemRegistrationResponseTO.getSystem().getName());

        return systemRegistrationResponseTO;
    }

    public static SystemTO patchSystem(String jwt, String systemId, SystemPatchingTO patchingTO, MockMvc mockMvc) throws Exception {
        SystemTO systemTO = fromJson(mockMvc.perform(
                patch(SYSTEM_ID_PATH.replace("{systemId}", systemId))
                        .contentType(APPLICATION_JSON_CHARSET_UTF_8)
                        .content(asJson(patchingTO))
                        .header(AUTHORIZATION, "Bearer " + jwt)
        ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), SystemTO.class);

        return systemTO;
    }

    /**
     * @return moduleId
     */
    public static ModuleTO createModule(String jwt, String systemId, MockMvc mockMvc) throws Exception {
        ModuleTO moduleTO = fromJson(mockMvc.perform(
                post(MODULE_PATH.replace("{systemId}", systemId))
                        .contentType(APPLICATION_JSON)
                        .content(asJson(ModuleAddingTO.builder().name("Upper").systemId(systemId).build()))
                        .header(AUTHORIZATION, "Bearer " + jwt)
        ).andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), ModuleTO.class);

        assertEquals("Upper", moduleTO.getModuleName());
        assertEquals(systemId, moduleTO.getSystemId());

        return moduleTO;
    }

    public static ModuleTO changeModuleName(String jwt, String systemId, String moduleId, String newModuleName, MockMvc mockMvc) throws Exception {
        return fromJson(mockMvc.perform(
                patch(MODULE_ID_PATH
                        .replace("{systemId}", systemId)
                        .replace("{moduleId}", moduleId)
                )
                        .contentType(APPLICATION_JSON)
                        .content(asJson(ModulePatchingTO.builder().name(newModuleName).build()))
                        .header(AUTHORIZATION, "Bearer " + jwt)
        ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<ModuleTO>(){});
    }

    public static void deleteModule(String jwt, String systemId, String moduleId, MockMvc mockMvc) throws Exception {
        mockMvc.perform(
                delete(MODULE_ID_PATH
                        .replace("{systemId}", systemId)
                        .replace("{moduleId}", moduleId)
                )
                        .contentType(APPLICATION_JSON)
                        .content(asJson(ModuleAddingTO.builder().name("Upper").systemId(systemId).build()))
                        .header(AUTHORIZATION, "Bearer " + jwt)
        ).andExpect(status().isOk());
    }

    public static List<ModuleTO> getModules(String jwt, String systemId, MockMvc mockMvc) throws Exception {
        return fromJson(mockMvc.perform(
                get(MODULE_PATH.replace("{systemId}", systemId))
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, "Bearer " + jwt)
        ).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<List<ModuleTO>>(){});
    }

    public static void expectUnauthorized(String jwt, String systemId, MockMvc mockMvc) throws Exception{
        mockMvc.perform(get(MODULE_PATH.replace("{systemId}", systemId))
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, "Bearer " + jwt)
        ).andExpect(status().isForbidden());
    }

    public static void checkIfThereAreNoModules(String jwt, String systemId, MockMvc mockMvc) throws Exception {
        mockMvc.perform(
                get(MODULE_PATH.replace("{systemId}", systemId))
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, "Bearer " + jwt)
        ).andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    public static void checkIfSlotsAreEmpty(String jwt, String systemId, String moduleId, MockMvc mockMvc) throws Exception {
        log.info("Checking slots");

        //verify empty slots
        String contentAsString = mockMvc.perform(
                get(SLOTS_PATH.replace("{systemId}", systemId)
                        .replace("{moduleId}", moduleId))
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, "Bearer " + jwt)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();

        JSONArray array = new JSONArray(contentAsString);
        assertEquals(SLOTS_IN_ROW * SLOTS_ROWS, array.length());

        for (int i = 0; i < array.length(); i++) // all empty
            assertNull(fromJson(array.get(i).toString(), SlotTO.class).getPlantId());
    }

    public static List<SlotTO> getSlots(String jwt, String systemId, String moduleId, MockMvc mockMvc) throws Exception {
        LinkedList<SlotTO> slots = Lists.newLinkedList();
        //verify empty slots
        String contentAsString = mockMvc.perform(
                get(SLOTS_PATH.replace("{systemId}", systemId)
                        .replace("{moduleId}", moduleId))
                        .contentType(APPLICATION_JSON)
                        .header(AUTHORIZATION, "Bearer " + jwt)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();

        JSONArray array = new JSONArray(contentAsString);
        assertEquals(SLOTS_IN_ROW * SLOTS_ROWS, array.length());

        for (int i = 0; i < array.length(); i++) // all empty
            slots.add(fromJson(array.get(i).toString(), SlotTO.class));

        return slots;
    }

    public static PermissionTO grantPermission(String systemId, String jwtOwner, String holderMail, MockMvc mockMvc, PermissionType permissionType) throws Exception{
        return fromJson(mockMvc.perform(
                post(SYSTEM_ID_PERMISSIONS_PATH.replace("{systemId}", systemId))
                        .header(AUTHORIZATION, "Bearer " + jwtOwner)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(asJson(PermissionTO.builder()
                        .systemId(systemId)
                        .holderEmail(holderMail)
                        .permissionType(permissionType)
                        .build())
                )
        )
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), PermissionTO.class);
    }

    public static List<PermissionTO> deletePermission(String systemId, String jwtOwner, String permissionId, MockMvc mockMvc) throws Exception{
        mockMvc.perform(
                delete(SYSTEM_ID_PERMISSIONS_ID_PATH.replace("{systemId}", systemId).replace("{permissionId}", permissionId))
                        .header(AUTHORIZATION, "Bearer " + jwtOwner)
        )
                .andExpect(status().isAccepted());

        String contentAsString = mockMvc.perform(
                get(SYSTEM_ID_PERMISSIONS_PATH.replace("{systemId}", systemId))
                        .header(AUTHORIZATION, "Bearer " + jwtOwner)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();

        return fromJson(contentAsString, new TypeReference<List<PermissionTO>>() {});
    }

    public static void tryToDeleteOwnerPermission(String systemId, String jwtOwner, String id, MockMvc mockMvc) throws Exception {
        mockMvc.perform(
                delete(SYSTEM_ID_PERMISSIONS_ID_PATH.replace("{systemId}", systemId).replace("{permissionId}", id))
                        .header(AUTHORIZATION, "Bearer " + jwtOwner)
        )
                .andExpect(status().isBadRequest());
    }


    public static List<PermissionTO> getPermissions(String systemId, String jwtOwner, MockMvc mockMvc) throws Exception{
        List<PermissionTO> permissions;

        String contentAsString = mockMvc.perform(
                get(SYSTEM_ID_PERMISSIONS_PATH.replace("{systemId}", systemId))
                        .header(AUTHORIZATION, "Bearer " + jwtOwner)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();
        permissions = fromJson(contentAsString, new TypeReference<List<PermissionTO>>() {});

        return permissions;
    }

    public static List<SettingTO> getSettings(String systemId, String jwtOwner, MockMvc mockMvc) throws Exception {
        String contentAsString = mockMvc.perform(
                get(SETTINGS_SYSTEM_ID_PATH.replace("{systemId}", systemId))
                        .header(AUTHORIZATION, "Bearer " + jwtOwner)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();

        List<SettingTO> settingTOS = fromJson(contentAsString, new TypeReference<List<SettingTO>>() {
        });
        return settingTOS;
    }

    public static SettingTO getSetting(String moduleId, String jwtOwner, MockMvc mockMvc) throws Exception {
        String contentAsString = mockMvc.perform(
                get(SETTINGS_MODULE_ID_PATH.replace("{moduleId}", moduleId))
                        .header(AUTHORIZATION, "Bearer " + jwtOwner)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();

        SettingTO settingTOS = fromJson(contentAsString, new TypeReference<SettingTO>() {
        });
        return settingTOS;
    }

    public static SettingTO postSetting(String systemId, String jwtOwner, MockMvc mockMvc, SettingTO settingTO) throws Exception {
        String contentAsString = mockMvc.perform(
                post(SETTINGS_SYSTEM_ID_PATH.replace("{systemId}", systemId))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .header(AUTHORIZATION, "Bearer " + jwtOwner)
                .content(asJson(settingTO))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_CHARSET_UTF_8))
                .andReturn().getResponse().getContentAsString();

        SettingTO setting = fromJson(contentAsString, new TypeReference<SettingTO>() {
        });
        return setting;
    }
}
