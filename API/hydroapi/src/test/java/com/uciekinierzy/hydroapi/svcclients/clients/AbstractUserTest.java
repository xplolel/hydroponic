package com.uciekinierzy.hydroapi.svcclients.clients;

import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientLoginTO;
import com.uciekinierzy.hydroapi.svcclients.helpers.exceptions.InvalidClientException;
import com.uciekinierzy.hydroapi.svcmailing.MailingHelper;
import com.uciekinierzy.hydroapi.svcmailing.MailingService;
import com.uciekinierzy.hydroapi.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@Slf4j
public class AbstractUserTest {

    @MockBean
    private TokenRepo tokenRepo;
    @MockBean
    private ClientsRepo clientsRepo;
    @MockBean
    private PasswordEncoder passwordEncoder;
    @MockBean
    private MailingService mailingService;
    @MockBean
    private MailingHelper mailingHelper;

    private ClientService clientService;

    @Test
    public void testTokenCreation() throws InvalidClientException {
        ClientLoginTO clientLoginTO = ClientLoginTO.builder()
                .email("jan@dzban.pl")
                .password("jan@dzban.pl")
                .build();

        Client jan = Client.builder()
                .id("lala")
                .email("jan@dzban.pl")
                .salt("salt".repeat(16))
                .build();

        when(clientsRepo.findByEmail("jan@dzban.pl"))
        .then((e) -> Optional.of(jan));

        clientService = new ClientService(clientsRepo, tokenRepo, passwordEncoder, mailingService, mailingHelper);

        String token = clientService.loginAndGetToken(clientLoginTO);

        Assert.assertEquals(jan.getEmail(), JwtUtils.getEmailFromTokenWithoutAuth(token));
        Assert.assertEquals(jan.getId(), JwtUtils.getIdFromTokenWithoutAuth(token));
    }
}
