package com.uciekinierzy.hydroapi.svcclients.clients;

import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientLoginTO;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientRegisterTO;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientTO;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientUpdateTO;
import com.uciekinierzy.hydroapi.svcclients.controllers.TokenWrapper;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.asJson;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.fromJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@AutoConfigureMockMvc
public class ClientBasicTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void checkRegisterErrorCodes() throws Exception {
        String registerTo = asJson(new ClientRegisterTO("lala3", "lilu"));
        mockMvc.perform(post(REGISTER_ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(registerTo)
        )
                .andExpect(status().isBadRequest());

        registerTo = asJson(new ClientRegisterTO("lala3@lala", "lilu"));
        mockMvc.perform(post(REGISTER_ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(registerTo)
        )
                .andExpect(status().isBadRequest());

        registerTo = asJson(new ClientRegisterTO("lala3@lala.com", "lilu"));
        mockMvc.perform(post(REGISTER_ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(registerTo)
        )
                .andExpect(status().isBadRequest());

        registerTo = asJson(new ClientRegisterTO("lala3@lala.com", "lilulala"));
        mockMvc.perform(post(REGISTER_ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(registerTo)
        )
                .andExpect(status().isCreated());
    }

    @Test
    public void checkLoginErrorCodes() throws Exception {
        String json = asJson(new ClientRegisterTO("lala2@lala.com", "lilulala"));

        mockMvc.perform(
                post(REGISTER_ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(json)
        )
                .andExpect(status().isCreated());

        String loginJson = asJson(new ClientLoginTO("lala2@lala.com", "lilulala1"));
        mockMvc.perform(
                post(LOGIN_ENDPOINT_URL)
                .contentType(APPLICATION_JSON)
                .content(loginJson)
        )
                .andExpect(status().is4xxClientError());

        loginJson = asJson(new ClientLoginTO("lala2@lala.com", "lilulala"));
        mockMvc.perform(
                post(LOGIN_ENDPOINT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(loginJson)
        )
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(new Matcher<>() {
                    @Override
                    public boolean matches(Object item) {
                        return ((String) item).contains("token");
                    }

                    @Override
                    public void describeMismatch(Object item, Description mismatchDescription) {

                    }

                    @Override
                    public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {

                    }

                    @Override
                    public void describeTo(Description description) {

                    }
                }));
    }

    @Test
    public void clientDetailsInitiallyEmpty_addingTimeshift() throws Exception{
        String json = asJson(new ClientRegisterTO("lala1@lala.com", "lilulala"));

        ClientTO client = fromJson(mockMvc.perform(
                post(REGISTER_ENDPOINT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(json)
        )
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), ClientTO.class);

        String loginJson = asJson(new ClientLoginTO("lala1@lala.com", "lilulala"));
        TokenWrapper tokenWrapper = fromJson(mockMvc.perform(
                post(LOGIN_ENDPOINT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(loginJson)
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), TokenWrapper.class);

        ClientTO clientTO = fromJson(mockMvc.perform(
                get(CLIENT_ID_PATH.replace("{id}", client.getId()))
                        .header("Authorization", "Bearer " + tokenWrapper.getToken())
        )
                .andExpect(status().is2xxSuccessful())
                .andReturn().getResponse().getContentAsString(), ClientTO.class);

        Assert.assertNull(clientTO.getClientDetailsTO().getFirstName());
        Assert.assertNull(clientTO.getClientDetailsTO().getLastName());
        Assert.assertNull(clientTO.getClientDetailsTO().getTimezoneShift());
        Assert.assertNotNull(clientTO.getClientDetailsTO().getRegistered());

        mockMvc.perform(
                put(CLIENT_ID_PATH.replace("{id}", client.getId()))
                .header("Authorization", "Bearer " + tokenWrapper.getToken())
                .contentType(APPLICATION_JSON)
                .content(asJson(ClientUpdateTO.builder().timezoneShift(2000).build()))
        )
                .andExpect(status().is2xxSuccessful());

        clientTO = fromJson(mockMvc.perform(
                get(CLIENT_ID_PATH.replace("{id}", client.getId()))
                        .header("Authorization", "Bearer " + tokenWrapper.getToken())
        )
                .andExpect(status().is2xxSuccessful())
                .andReturn().getResponse().getContentAsString(), ClientTO.class);

        Assert.assertEquals(Integer.valueOf(2000), clientTO.getClientDetailsTO().getTimezoneShift());
//        Assert.assertNull(client.getClientDetailsTO());
    }
}
