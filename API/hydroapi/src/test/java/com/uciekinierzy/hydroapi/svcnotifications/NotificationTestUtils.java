package com.uciekinierzy.hydroapi.svcnotifications;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.List;

import static com.uciekinierzy.hydroapi.constants.Endpoints.NOTIFCATIONS_ID_PATH;
import static com.uciekinierzy.hydroapi.constants.Endpoints.NOTIFCATIONS_PATH;
import static com.uciekinierzy.hydroapi.svcnotifications.NotificationType.INFO;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.asJson;
import static com.uciekinierzy.hydroapi.utils.JsonUtils.fromJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("WeakerAccess")
@Slf4j
public class NotificationTestUtils {
    public static NotificationAddingTO getDummyNotificationAddingTO(String systemId){
        return NotificationAddingTO.builder()
                .systemId(systemId)
                .shortNote("dupka")
                .longNote("dwieDupki oaiuqoheqpe")
                .notificationType(INFO)
                .notificationType2(NotificationType2.CLEANING)
                .build();
    }

    public static NotificationTO createNotification(String jwt, NotificationAddingTO notificationAddingTO, MockMvc mockMvc) throws Exception {
        return fromJson(mockMvc.perform(
                post(NOTIFCATIONS_PATH)
                        .header("Authorization", "Bearer " + jwt)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJson(notificationAddingTO))
        )
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(), NotificationTO.class);
    }

    public static List<NotificationTO> getNotifications(String jwt, MockMvc mockMvc) throws Exception {
        return getNotifications(jwt, mockMvc, null, null, false, false);
    }

    public static List<NotificationTO> getNotifications(String jwt, MockMvc mockMvc, Integer offset, Integer limit, boolean unreadOnly, boolean readOnly) throws Exception {

        final MockHttpServletRequestBuilder request = get(NOTIFCATIONS_PATH)
                .header("Authorization", "Bearer " + jwt);

        if(offset != null)
            request.param("offset", offset.toString());

        if(limit != null)
            request.param("limit", limit.toString());

        if(unreadOnly)
            request.param("unreadOnly", String.valueOf(unreadOnly));

        if(readOnly)
            request.param("readOnly", String.valueOf(readOnly));

        return fromJson(mockMvc.perform(request)
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), new TypeReference<List<NotificationTO>>(){});
    }

    public static NotificationTO getNotification(String jwt, String notId, MockMvc mockMvc) throws Exception {
        return fromJson(mockMvc.perform(
                get(NOTIFCATIONS_ID_PATH.replace("{id}", notId))
                        .header("Authorization", "Bearer " + jwt)
        )
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), NotificationTO.class);
    }

    public static void readNotification(String jwt, String notId, MockMvc mockMvc) throws Exception {
        mockMvc.perform(
                patch(NOTIFCATIONS_ID_PATH.replace("{id}", notId))
                        .header("Authorization", "Bearer " + jwt)
        )
                .andExpect(status().isOk());
    }
}
