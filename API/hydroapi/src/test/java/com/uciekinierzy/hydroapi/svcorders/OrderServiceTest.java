package com.uciekinierzy.hydroapi.svcorders;

import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SettingTO;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class OrderServiceTest {
    OrderService orderService = new OrderService(null, null, null, null);

    @Test
    public void lightLogic() {
        Assert.assertFalse(orderService.isLightOnBySetting(SettingTO.builder()
                .fromHourUTC(5)
                .toHourUTC(23)
                .fromMinuteUTC(0)
                .toMinuteUTC(0)
                .build(),
                LocalDateTime.of(2000, 1, 2, 23, 20)
                ));

        Assert.assertFalse(orderService.isLightOnBySetting(SettingTO.builder()
                        .fromHourUTC(5)
                        .toHourUTC(5)
                        .fromMinuteUTC(0)
                        .toMinuteUTC(0)
                        .build(),
                LocalDateTime.of(2000, 1, 2, 5, 0)
        ));

        Assert.assertTrue(orderService.isLightOnBySetting(SettingTO.builder()
                        .fromHourUTC(5)
                        .toHourUTC(23)
                        .fromMinuteUTC(0)
                        .toMinuteUTC(0)
                        .build(),
                LocalDateTime.of(2000, 1, 2, 15, 20)
        ));

        Assert.assertTrue(orderService.isLightOnBySetting(SettingTO.builder()
                        .fromHourUTC(22)
                        .toHourUTC(5)
                        .fromMinuteUTC(20)
                        .toMinuteUTC(10)
                        .build(),
                LocalDateTime.of(2000, 1, 2, 23, 20)
        ));

        Assert.assertTrue(orderService.isLightOnBySetting(SettingTO.builder()
                        .fromHourUTC(22)
                        .toHourUTC(5)
                        .fromMinuteUTC(20)
                        .toMinuteUTC(10)
                        .build(),
                LocalDateTime.of(2000, 1, 2, 0, 10)
        ));

        Assert.assertTrue(orderService.isLightOnBySetting(SettingTO.builder()
                        .fromHourUTC(22)
                        .toHourUTC(5)
                        .fromMinuteUTC(20)
                        .toMinuteUTC(10)
                        .build(),
                LocalDateTime.of(2000, 1, 2, 5, 9)
        ));
    }
}
