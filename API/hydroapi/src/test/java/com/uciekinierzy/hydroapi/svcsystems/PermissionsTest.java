package com.uciekinierzy.hydroapi.svcsystems;

import com.uciekinierzy.hydroapi.svcsystems.permission.PermissionType;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.ModuleTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.PermissionTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemRegistrationResponseTO;
import com.uciekinierzy.hydroapi.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.uciekinierzy.hydroapi.svcclients.clients.ClientTestUtils.createUserAndLogIn;
import static com.uciekinierzy.hydroapi.svcsystems.SystemTestUtils.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@Slf4j
public class PermissionsTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testOwnerPermission() throws Exception {
        String jwt = createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        String moduleId = createModule(jwt, systemId, mockMvc).getId();
        checkIfSlotsAreEmpty(jwt, systemId, moduleId, mockMvc);
    }

    @Test
    public void grantPermission_test_thenRevoke_test() throws Exception {
        String jwt = createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        String viewerMail = "viewer@on.com";
        String jwtViewer = createUserAndLogIn(mockMvc, viewerMail);

        PermissionTO permissionTO = grantPermission(systemId, jwt, viewerMail, mockMvc, PermissionType.USER);

        assertEquals(PermissionType.USER, permissionTO.getPermissionType());
        assertEquals(JwtUtils.getEmailFromTokenWithoutAuth(jwtViewer), permissionTO.getHolderEmail());
        assertEquals(systemId, permissionTO.getSystemId());

        //testing permission
        List<ModuleTO> modules = getModules(jwtViewer, systemId, mockMvc);

        List<PermissionTO> permissionTOS = deletePermission(systemId, jwt, permissionTO.getId(), mockMvc);
        assertEquals(1L, permissionTOS.size());

        expectUnauthorized(jwtViewer, systemId, mockMvc);
    }

    @Test
    public void cannotDeleteOwnerPermission() throws Exception {
        String jwt = createUserAndLogIn(mockMvc);

        SystemRegistrationResponseTO system = SystemTestUtils.createSystem(jwt, mockMvc);
        String systemId = system.getSystem().getId();

        List<PermissionTO> permissions = getPermissions(systemId, jwt, mockMvc);

        assertEquals(1L, permissions.size());
        assertEquals(PermissionType.OWNER, permissions.get(0).getPermissionType());

        tryToDeleteOwnerPermission(systemId, jwt, permissions.get(0).getId(), mockMvc);
    }

//    @Test public void restrictedRightsForCertainPermissionType() { } TODO once permissions rights specified
}
