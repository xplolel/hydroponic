package com.uciekinierzy.hydroapi.svcclients.clients.TOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ClientUpdateTO {
    private Integer timezoneShift;
    private String firstName;
    private String lastName;
}
