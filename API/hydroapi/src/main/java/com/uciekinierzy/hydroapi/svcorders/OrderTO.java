package com.uciekinierzy.hydroapi.svcorders;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrderTO {
    private String id;
    private Boolean executed;
    private String moduleId;
    private String shortNote;
    private LocalDateTime created;
    private LocalDateTime executedAt;
    private OrderType orderType;
}
