package com.uciekinierzy.hydroapi.svcorders;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum OrderType {
    HALF_LIGHT_ON("HALF_LIGHT_ON"),
    HALF_LIGHT_OFF("HALF_LIGHT_OFF"),
    FULL_LIGHT_ON("FULL_LIGHT_ON"),
    FULL_LIGHT_OFF("FULL_LIGHT_OFF"),
    ;

    private final String value;

    @Override
    public String toString()
    {
        return value;
    }

    @JsonCreator
    public static OrderType fromText(String text)
    {
        return OrderType.valueOf(text);
    }
}
