package com.uciekinierzy.hydroapi.svcorders;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LightStateTO {
    private String moduleId;
    private boolean lightOn;
    private boolean halfLight;
}
