package com.uciekinierzy.hydroapi.svcclients.helpers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class PasswordResetTokenExpiredException extends Exception{
    public PasswordResetTokenExpiredException() {
        super();
    }

    public PasswordResetTokenExpiredException(String message) {
        super(message);
    }

    public PasswordResetTokenExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public PasswordResetTokenExpiredException(Throwable cause) {
        super(cause);
    }

    protected PasswordResetTokenExpiredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
