package com.uciekinierzy.hydroapi.configuration;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.exceptions.ResourceOfSuchIdExistsException;
import com.uciekinierzy.hydroapi.svcsystems.helpers.CannotDeleteOwnerPermissionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.*;

@Primary
@ControllerAdvice
@Slf4j
public class ExceptionHandlerHelper extends ResponseEntityExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ResponseEntity handle(Exception ex, WebRequest request){
        log.error(ex.getClass() + request.toString(), ex);

        if(ex instanceof NoAccessException)
            return ResponseEntity.status(FORBIDDEN).body("{\"error\":\"You have no access to this resource.\"}");
        else if(ex instanceof CannotDeleteOwnerPermissionException)
            return ResponseEntity.status(BAD_REQUEST).body("{\"error\":\"You cannot delete owner permissions.\"}");
        else if(ex instanceof IncorrectTOException)
            return ResponseEntity.status(BAD_REQUEST).body("{\"error\":\"Incorrect TO, thrown from service. Check optional and required fields.\"}");
        else if(ex instanceof NoSuchResourceException)
            return ResponseEntity.status(NOT_FOUND).body("{\"error\":\"Resource not found.\"}");
        else if(ex instanceof ResourceOfSuchIdExistsException)
            return ResponseEntity.status(CONFLICT).body("{\"error\":\"Resource with such id already exists.\"}");
        else
            return ResponseEntity.status(BAD_REQUEST).body("{\"error\":\"Could not precise the issue.\"}");
    }
}
