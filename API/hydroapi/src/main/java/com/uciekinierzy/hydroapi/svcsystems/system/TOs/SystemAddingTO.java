package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class SystemAddingTO
{
    private String ownerId;
    private String ownerEmail;
    private String name;
}
