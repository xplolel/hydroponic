package com.uciekinierzy.hydroapi.svcsystems.settings;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RestResource(exported = false)
public interface SettingRepo extends CrudRepository<Setting, String> {
    Optional<Setting> findByModuleId(String moduleId);
}
