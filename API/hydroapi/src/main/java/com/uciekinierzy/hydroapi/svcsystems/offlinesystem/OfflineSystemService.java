package com.uciekinierzy.hydroapi.svcsystems.offlinesystem;

import com.uciekinierzy.hydroapi.exceptions.ResourceOfSuchIdExistsException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class OfflineSystemService
{
    private final OfflineSystemRepo offlineSystemRepo;

    public void add(String id) throws ResourceOfSuchIdExistsException {
        if(offlineSystemRepo.findById(id).isPresent())
            throw new ResourceOfSuchIdExistsException("Offline System, id=" + id);

        OfflineSystem offlineSystem = new OfflineSystem();
        offlineSystem.setId(id);
        offlineSystemRepo.save(offlineSystem);
    }

    public Optional<OfflineSystem> findById(String id) {
        return offlineSystemRepo.findById(id);
    }

    public void deleteById(String id) {
        offlineSystemRepo.deleteById(id);
    }
}
