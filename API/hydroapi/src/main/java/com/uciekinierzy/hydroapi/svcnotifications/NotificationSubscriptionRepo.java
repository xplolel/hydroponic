package com.uciekinierzy.hydroapi.svcnotifications;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(exported = false)
public interface NotificationSubscriptionRepo extends CrudRepository<NotificationSubscription, Long> {
    List<NotificationSubscription> findAllByClientId(String clientId);
    List<NotificationSubscription> findAllByClientEmail(String email);
}
