package com.uciekinierzy.hydroapi.svcclients.controllers;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.exceptions.ResourceOfSuchIdExistsException;
import com.uciekinierzy.hydroapi.svcclients.clients.ClientService;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.*;
import com.uciekinierzy.hydroapi.svcclients.helpers.exceptions.InvalidClientException;
import com.uciekinierzy.hydroapi.svcclients.helpers.exceptions.PasswordResetTokenExpiredException;
import com.uciekinierzy.hydroapi.svcclients.helpers.exceptions.SuchClientAlreadyExistsException;
import com.uciekinierzy.hydroapi.utils.AuthUtils;
import com.uciekinierzy.hydroapi.utils.ControllerUtils;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Optional;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;
import static com.uciekinierzy.hydroapi.utils.JwtUtils.getIdFromTokenWithoutAuth;
import static org.springframework.http.ResponseEntity.*;


@RestController(CLIENTS_CONTROLLER)
@RequiredArgsConstructor
@Slf4j
public class ClientsController {
    private final ClientService clientService;
    private final AuthenticationManager authenticationManager;

    @PostMapping(REGISTER_ENDPOINT_URL)
    public ResponseEntity registerNewClient(@RequestBody ClientRegisterTO clientRegisterTO) throws ResourceOfSuchIdExistsException, IncorrectTOException {
        try {
            ClientTO client = clientService.addClient(clientRegisterTO);
            return created(getLocation(client)).body(client);
        } catch (InvalidClientException e) {
            throw new IncorrectTOException(e);
        } catch (SuchClientAlreadyExistsException e) {
            throw new ResourceOfSuchIdExistsException(e);
        }
    }

    @PostMapping(LOGIN_ENDPOINT_URL)
    public ResponseEntity login(@RequestBody ClientLoginTO clientLoginTO) throws IncorrectTOException {
        try {
            String token = clientService.loginAndGetToken(clientLoginTO);
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(clientLoginTO.getEmail(), clientLoginTO.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            return ok(new TokenWrapper(token, getIdFromTokenWithoutAuth(token)));
        } catch (InvalidClientException e) {
            log.error("Invalid TO" + clientLoginTO.toString());
            throw new IncorrectTOException();
        }
    }

    @GetMapping(WHO_AM_I)
    public ResponseEntity whoAmI(HttpServletRequest request) {
        String id = getIdFromTokenWithoutAuth(ControllerUtils.getToken(request));
        return ok(new UserWrapper(id));
    }

    @GetMapping(CLIENT_ID_PATH)
    public ResponseEntity getClient(@PathVariable String id, HttpServletRequest request) throws NoAccessException {
        AuthUtils.checkClientAccessAndThrowException(id, request);

        Optional client = clientService.getClientByIdAsTO(id);
        return client.isPresent() ? ok(client.get())
                : notFound().build();
    }

    @PutMapping(CLIENT_ID_PATH)
    public ResponseEntity updateDetail(@PathVariable String id, @RequestBody ClientUpdateTO clientUpdateTO, HttpServletRequest request) throws NoAccessException {
        AuthUtils.checkClientAccessAndThrowException(id, request);

        return ResponseEntity.ok(clientService.updateClient(id, clientUpdateTO));
    }

    @GetMapping(ALL_CLIENTS_PATH)
    public ResponseEntity getAll() {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("you have no power here");
    }

    @PostMapping(RESET_PASSWORD_ENDPOINT_URL)
    @ApiResponses( value = {
            @ApiResponse(code = 400, message = "Incorrect json request"),
            @ApiResponse(code = 404, message = "User of such email not found"),
            @ApiResponse(code = 409, message = "Token expired"),
            @ApiResponse(code = 200, message = "OK"),
    })
    public ResponseEntity resetPassword(@RequestBody ClientPasswordResetTO clientPasswordResetTO) throws NoSuchResourceException, PasswordResetTokenExpiredException, IncorrectTOException {
        clientService.changePassword(clientPasswordResetTO.getToken(),
                clientPasswordResetTO.getEmail(),
                clientPasswordResetTO.getPassword());

        return ok().build();
    }

    @PostMapping(FORGOT_PASSWORD_ENDPOINT_URL)
    @ApiResponses( value = {@ApiResponse(code = 200, message = "OK")})
    public ResponseEntity forgottenPassword(@RequestParam String email) {
        clientService.forgottenPassword(email);
        return ok().build();
    }

    private URI getLocation(ClientTO client) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(CLIENTS_CONTROLLER + CLIENT_ID_PATH).buildAndExpand(client.getId()).toUri();
    }
}
