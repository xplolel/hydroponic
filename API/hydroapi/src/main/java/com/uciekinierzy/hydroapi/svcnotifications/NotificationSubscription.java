package com.uciekinierzy.hydroapi.svcnotifications;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class NotificationSubscription {
    @Id
    @GeneratedValue
    Long id;
    String clientId;
    String clientEmail;
    @Column(length = 8192)
    String subscriptionJson;
}
