package com.uciekinierzy.hydroapi.svcsystems.settings;

import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SettingTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class Setting {
    @Id
    String moduleId;
    @Column(nullable = false)
    Boolean halfLight;

    @Column(nullable = false)
    Integer fromHourUTC;
    @Column(nullable = false)
    Integer fromMinuteUTC;
    @Column(nullable = false)
    Integer toHourUTC;
    @Column(nullable = false)
    Integer toMinuteUTC;

    SettingTO toTO(){
        return new SettingTO(moduleId, halfLight, fromHourUTC, fromMinuteUTC, toHourUTC, toMinuteUTC);
    }
}
