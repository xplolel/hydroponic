package com.uciekinierzy.hydroapi.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;

public class JwtUtils {
    public static String getIdFromTokenWithoutAuth(String token) {
        return getClaimsWithoutSignature(token).getBody().get("id", String.class);
    }

    public static String getEmailFromTokenWithoutAuth(String token) {
        return getClaimsWithoutSignature(token).getBody().get("email", String.class);
    }

    public static Jwt<Header, Claims> getClaimsWithoutSignature(String token) {
        return Jwts.parser()
                .parseClaimsJwt(token.substring(0, token.lastIndexOf('.') + 1));
    }

}
