package com.uciekinierzy.hydroapi.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PropsProvider {
    private static PropsProvider instance;

    @Value("${app.testing}")
    private String testing;

    @Value("${app.api.url}")
    private String apiUrl;

    @Value("${app.web.url}")
    private String webUrl;

    @Value("${app.web.resetPasswordEndpoint}")
    private String webResetPasswordEndpoint;

    @Value("${app.resetPasswordToken.hourlyTimeout}")
    private String resetPasswordTokenHourlyTimeout;

    @PostConstruct
    private void init() {
        instance = this;
    }

    public static boolean isAppInTesting(){
        return Boolean.parseBoolean(instance.testing);
    }

    public static String getApiUrl(){
        return instance.apiUrl;
    }

    public static String getWebUrl(){
        return instance.webUrl;
    }

    public static String getWebResetPasswordEndpoint(){
        return instance.webResetPasswordEndpoint;
    }

    public static int getResetPasswordTokenHourlyTimeout(){
        try{
            return Integer.parseInt(instance.resetPasswordTokenHourlyTimeout);
        }
        catch (NumberFormatException e) {
            return 6;
        }
    }
}
