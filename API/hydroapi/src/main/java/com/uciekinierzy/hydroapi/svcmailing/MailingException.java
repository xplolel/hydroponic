package com.uciekinierzy.hydroapi.svcmailing;

public class MailingException extends Exception {
    public MailingException() {
        super();
    }

    public MailingException(String message) {
        super(message);
    }

    public MailingException(String message, Throwable cause) {
        super(message, cause);
    }

    public MailingException(Throwable cause) {
        super(cause);
    }

    protected MailingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
