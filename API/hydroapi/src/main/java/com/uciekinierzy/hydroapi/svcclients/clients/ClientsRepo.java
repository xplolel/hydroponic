package com.uciekinierzy.hydroapi.svcclients.clients;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RestResource(exported = false)
public interface ClientsRepo extends CrudRepository<Client, String>
{
    Optional<Client> findByEmail(String email);
}
