package com.uciekinierzy.hydroapi.svcclients.clients.TOs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientDetailsTO
{
    private String firstName;
    private String lastName;
    private LocalDateTime registered;
    private Integer timezoneShift;
}
