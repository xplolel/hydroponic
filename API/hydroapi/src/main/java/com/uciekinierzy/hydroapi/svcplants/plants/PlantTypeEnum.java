package com.uciekinierzy.hydroapi.svcplants.plants;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum PlantTypeEnum
{
    FRUIT("FRUIT"),
    VEGETABLE("VEGETABLE"),
    SPICES("SPICES"),
    HOME_PLANT("HOME_PLANT");

    private final String value;

    @Override
    public String toString()
    {
        return value;
    }

    @JsonCreator
    public static PlantTypeEnum fromText(String text)
    {
        return PlantTypeEnum.valueOf(text);
    }
}
