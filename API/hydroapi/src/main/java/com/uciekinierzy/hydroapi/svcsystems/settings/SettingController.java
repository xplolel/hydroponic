package com.uciekinierzy.hydroapi.svcsystems.settings;

import com.uciekinierzy.hydroapi.constants.Endpoints;
import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.system.ModuleService;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.ModuleTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SettingTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

import static com.uciekinierzy.hydroapi.constants.Endpoints.SETTINGS_PATH;
import static com.uciekinierzy.hydroapi.utils.AuthUtils.checkModuleAccessAndThrowException;
import static com.uciekinierzy.hydroapi.utils.AuthUtils.checkSystemAccessAndThrowException;
import static org.springframework.http.ResponseEntity.ok;

@RestController(SETTINGS_PATH)
@RequiredArgsConstructor
public class SettingController {
    private final SettingService settingService;
    private final ModuleService moduleService;
    private final HttpServletRequest request;

    @GetMapping(Endpoints.SETTINGS_SYSTEM_ID_PATH)
    public ResponseEntity getSettingsForSystem(@PathVariable String systemId) throws NoAccessException, NoSuchResourceException {
        checkSystemAccessAndThrowException(systemId, request);
        //TODO this should go to service - circ dep
        List<String> moduleIds = moduleService.getModulesBySystem(systemId)
                .stream()
                .map(ModuleTO::getId)
                .collect(Collectors.toList());

        return ok(settingService.getSettingForModuleIds(moduleIds));
    }

    @GetMapping(Endpoints.SETTINGS_MODULE_ID_PATH)
    public ResponseEntity getSettingsForModule(@PathVariable String moduleId) throws NoAccessException, NoSuchResourceException {
        checkModuleAccessAndThrowException(moduleId, request);
        return ok(settingService.getSettingForModule(moduleId));
    }

    @PostMapping(Endpoints.SETTINGS_SYSTEM_ID_PATH)
    public ResponseEntity addOrAlterSettings(@PathVariable String systemId, @RequestBody SettingTO settingTO) throws NoAccessException, NoSuchResourceException, IncorrectTOException {
        checkSystemAccessAndThrowException(systemId, request);
        return ok(settingService.alterSetting(settingTO));
    }
}
