package com.uciekinierzy.hydroapi.svcsystems.permission;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum PermissionType {
    OWNER("OWNER"),
    USER("USER"),
    VIEWER("VIEWER"),
    ;

    private final String value;

    @Override
    public String toString()
    {
        return value;
    }

    @JsonCreator
    public static PermissionType fromText(String text)
    {
        return PermissionType.valueOf(text);
    }
}
