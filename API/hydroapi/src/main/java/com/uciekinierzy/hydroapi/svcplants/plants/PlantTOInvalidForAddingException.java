package com.uciekinierzy.hydroapi.svcplants.plants;

class PlantTOInvalidForAddingException extends Throwable
{
    PlantTOInvalidForAddingException(String msg)
    {
        super(msg);
    }
}
