package com.uciekinierzy.hydroapi.svcmailing;

public class MailingTemplateException extends MailingException {
    public MailingTemplateException() {
        super();
    }

    public MailingTemplateException(String message) {
        super(message);
    }

    public MailingTemplateException(String message, Throwable cause) {
        super(message, cause);
    }

    public MailingTemplateException(Throwable cause) {
        super(cause);
    }

    protected MailingTemplateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
