package com.uciekinierzy.hydroapi.svcnotifications;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.system.SystemService;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemTO;
import com.uciekinierzy.hydroapi.utils.AuthUtils;
import com.uciekinierzy.hydroapi.utils.PaginationUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.isEmpty;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationService {
    private final NotificationRepo notificationRepo;
    private final NotificationSubscriptionRepo notificationSubscriptionRepo;
    private final SystemService systemService;
    private final ScriptCaller scriptCaller;

    List<NotificationTO> getNotificationsForClient(String jwt, Integer limit, Integer offset, boolean readOnly, boolean unreadOnly) throws IncorrectTOException {
        if (readOnly && unreadOnly)
            throw new IncorrectTOException("Can't be both true at the same time");

        List<String> systemsIdsForThisClient = getSystemsIdsForThisClient(jwt);

        List<NotificationTO> collect = systemsIdsForThisClient.stream()
                .map(notificationRepo::getAllBySystemId)
                .flatMap(Collection::stream)
                .map(Notification::toTO)
                .filter(n -> shouldBeReturned(n, readOnly, unreadOnly))
                .sorted(Comparator.comparing(NotificationTO::getRead))
                .collect(Collectors.toList());


        return PaginationUtils.paginateList(collect, limit, offset);
    }

    @SuppressWarnings("RedundantIfStatement")
    private boolean shouldBeReturned(NotificationTO n, boolean readOnly, boolean unreadOnly) {
        if (!readOnly && !unreadOnly)
            return true;
        else if(readOnly && n.getRead())
            return true;
        else if(unreadOnly && !n.getRead())
            return true;
        else
            return false;
    }


    void read(String id, String token) throws NoSuchResourceException, NoAccessException {
        Notification notification = notificationRepo.findById(id).orElseThrow(NoSuchResourceException::new);

        AuthUtils.checkSystemAccessAndThrowException(notification.getSystemId(), token);

        notification.setRead(true);
        notificationRepo.save(notification);
    }

    public NotificationTO postNotification(NotificationAddingTO notificationAddingTO, String token) throws NoSuchResourceException, NoAccessException, IncorrectTOException {
        if(isEmpty(notificationAddingTO.getShortNote())
            || isEmpty(notificationAddingTO.getLongNote())
            || notificationAddingTO.getNotificationType() == null)
            throw new IncorrectTOException();
        //throws noaccess if not found or not access
        AuthUtils.checkSystemAccessAndThrowException(notificationAddingTO.getSystemId(), token);

        return postWithoutAuth(notificationAddingTO);
    }

    public NotificationTO postWithoutAuth(NotificationAddingTO notificationAddingTO) {
        brokeFirebaseMessage(notificationAddingTO);

        Notification notification = Notification.builder()
                .id(UUID.randomUUID().toString())
                .read(false)
                .created(notificationAddingTO.getCreated() != null ? notificationAddingTO.getCreated() : LocalDateTime.now())
                .longNote(notificationAddingTO.getLongNote())
                .shortNote(notificationAddingTO.getShortNote())
                .systemId(notificationAddingTO.getSystemId())
                .notificationType(notificationAddingTO.getNotificationType())
                .notificationType2(notificationAddingTO.getNotificationType2())
                .build();

        notificationRepo.save(notification);
        return notification.toTO();
    }

    void brokeFirebaseMessage(NotificationAddingTO notificationAddingTO) {
        Set<String> emails = systemService.getEmailsWithAccessToSystem(notificationAddingTO.getSystemId());

        Set<NotificationSubscription> subscriptions = emails.stream()
                .map(notificationSubscriptionRepo::findAllByClientEmail)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());


        if(!subscriptions.isEmpty()) {
            log.info("Pushing notification:" + notificationAddingTO);

            subscriptions.forEach(sub -> {
                if(scriptCaller.call(sub.getSubscriptionJson(), notificationAddingTO))
                    log.info("Successfully pushed notification to " + sub.clientEmail);
                else
                    log.warn("Failed to push notification to " + sub.clientEmail);
            });
        }
    }

    private List<String> getSystemsIdsForThisClient(String jwt) {
        return systemService.getSystemsClientHasAccessTo(jwt)
                .stream()
                .map(SystemTO::getId)
                .collect(Collectors.toList());
    }

    public NotificationTO get(String id, String token) throws NoSuchResourceException, NoAccessException {
        NotificationTO notificationTO = notificationRepo.findById(id)
                .orElseThrow(NoSuchResourceException::new)
                .toTO();

        AuthUtils.checkSystemAccessAndThrowException(notificationTO.getSystemId(), token);

        return notificationTO;
    }

    public boolean recentlyNotified(String system, String longNote, String shortNote, int hoursAgo) {
        Collection<Notification> nots = notificationRepo.getAllBySystemIdAndShortNoteAndLongNoteAndRead(system, shortNote, longNote, false);

        Optional<Notification> recent = nots.stream()
                .filter(notification -> notification.getCreated().isAfter(LocalDateTime.now().minusHours(hoursAgo)))
                .findAny();

        if(nots.isEmpty())
            return false;
        else if(recent.isPresent())
            return true;
        else
            return false;
    }

    public void subscribe(String email, String clientId, String json) {
        NotificationSubscription subscription = NotificationSubscription.builder()
                .clientId(clientId)
                .clientEmail(email)
                .subscriptionJson(json)
                .build();

        notificationSubscriptionRepo.save(subscription);
    }
}
