package com.uciekinierzy.hydroapi.svcsystems.system;

import com.uciekinierzy.hydroapi.svcsystems.permission.Permission;
import com.uciekinierzy.hydroapi.svcsystems.permission.PermissionRepo;
import com.uciekinierzy.hydroapi.utils.JwtUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
/*
  Solely for permission check & authorization.
 */
public class AuthorizationService {
    private final PermissionRepo permissionRepo;
    private final SystemRepo systemRepo;
    private final ModuleRepo moduleRepo;
    //TODO OauthRepo and eligibility

    public boolean ifTokenAuthorizedForModule(String token, String moduleId){
        Optional<Module> module = moduleRepo.findById(moduleId);

        if(module.isEmpty())
            return false;
        else
            return ifTokenAuthorizedForSystem(token, module.get().getSystem().getId());
    }

    public boolean ifTokenAuthorizedForSystem(String token, String systemId){
        if(isJwt(token)){
            String email = JwtUtils.getEmailFromTokenWithoutAuth(token);
            Optional<Permission> permission = permissionRepo.findByHolderEmailAndSystemId(email, systemId);
            return permission.isPresent();
        }
        else {
            Optional<System> system = systemRepo.findByIdAndToken(systemId, token);
            return system.isPresent();
        }
    }

    public boolean isSystemTokenValid(String token){
        return systemRepo.findByToken(token).isPresent();
    }

    public static boolean isJwt(String token){
        return token.contains(".");
    }

    public static boolean isTokenAServer(String token){
        return !isJwt(token);
    }
}
