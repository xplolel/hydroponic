package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SystemPatchingTO
{
    private String name;
}
