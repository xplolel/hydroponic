package com.uciekinierzy.hydroapi.svcclients.clients.TOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.regex.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Data
/**
 * Used for registering purposes only.
 * Any other case ClientTO should be used.
 */
public class ClientRegisterTO
{
    public static final String EMAIL_PATTERN = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
    public static final Pattern PATTERN = Pattern.compile(EMAIL_PATTERN);

    @Email
    @NotBlank
    @Size(max = 64)
    private String email;

    @NotBlank
    @Size(min = 6, max = 64)
    private String password;
}
