package com.uciekinierzy.hydroapi.svcnotifications;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum NotificationType2 {
    CLEANING("CLEANING"),
    HARVESTING("HARVESTING"),

    ;

    private final String value;

    @Override
    public String toString()
    {
        return value;
    }

    @JsonCreator
    public static NotificationType2 fromText(String text)
    {
        return NotificationType2.valueOf(text);
    }
}
