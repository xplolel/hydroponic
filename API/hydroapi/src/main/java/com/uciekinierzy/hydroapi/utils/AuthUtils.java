package com.uciekinierzy.hydroapi.utils;

import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.svcsystems.system.AuthorizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static com.uciekinierzy.hydroapi.utils.ControllerUtils.getToken;
import static com.uciekinierzy.hydroapi.utils.JwtUtils.getIdFromTokenWithoutAuth;

@Component
@RequiredArgsConstructor
public class AuthUtils implements InitializingBean {
    private static AuthUtils self;
    private final AuthorizationService authorizationService;

    public static void checkSystemAccessAndThrowException(String systemId, String token) throws NoAccessException {
        if(!self.authorizationService.ifTokenAuthorizedForSystem(token, systemId))
            throw new NoAccessException();
    }
    public static void checkSystemAccessAndThrowException(String systemId, HttpServletRequest request) throws NoAccessException {
        checkSystemAccessAndThrowException(systemId, getToken(request));
    }

    public static void checkModuleAccessAndThrowException(String moduleId, String token) throws NoAccessException {
        if(!self.authorizationService.ifTokenAuthorizedForModule(token, moduleId))
            throw new NoAccessException();
    }

    public static void checkModuleAccessAndThrowException(String moduleId, HttpServletRequest request) throws NoAccessException {
        checkModuleAccessAndThrowException(moduleId, getToken(request));
    }

    public static void checkClientAccessAndThrowException(String clientId, HttpServletRequest request) throws NoAccessException {
        if(!getIdFromTokenWithoutAuth(getToken(request)).equals(clientId))
            throw new NoAccessException();
    }

    @Override
    public void afterPropertiesSet() {
        self = this;
    }
}
