package com.uciekinierzy.hydroapi.watchers;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Watcher
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class WatcherManager{
    static int SLEEPTIME = 1000 * 60; // useful in testing

    private final OldMeasuresWatcher oldMeasuresWatcher;
    private final SystemActivityWatcher systemActivityWatcher;

    private ExecutorService executorService = Executors.newFixedThreadPool(1);
    List<Pair> watchers = Lists.newArrayList();

    @PostConstruct
    public void init(){
        watchers.add(new Pair(oldMeasuresWatcher, LocalDateTime.now()));
        watchers.add(new Pair(systemActivityWatcher, LocalDateTime.now()));

        executorService.submit(() -> {
            log.info("Started watcher manager thread");
            int i;

            //noinspection InfiniteLoopStatement
            while(true){
                i = 0;

                for (Pair pair : watchers) {
                    if(LocalDateTime.now().isAfter(pair.wakeUp)){
                        try{
                            i++;
                            pair.watcher.call().run();
                            pair.wakeUp = LocalDateTime.now().plusMinutes(pair.watcher.getIntervalInMinutes());
                        }
                        catch (Throwable e){
                            log.error("Error on watcher execution " + pair.watcher.getClass().getSimpleName(), e);
                        }
                    }
                }

                if(i > 0)
                    log.info("Called watchers: " + i);
                sleepMinute();
            }
        });
    }

    private void sleepMinute() {
        try{
            Thread.sleep(SLEEPTIME);
        }
        catch (InterruptedException e){
            //
        }
    }

    @AllArgsConstructor
    private static class Pair{
        Watcher watcher;
        LocalDateTime wakeUp;
    }
}
