package com.uciekinierzy.hydroapi.svcsystems.permission;

import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.helpers.CannotDeleteOwnerPermissionException;
import com.uciekinierzy.hydroapi.svcsystems.system.SystemService;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.PermissionTO;
import com.uciekinierzy.hydroapi.utils.AuthUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import static com.uciekinierzy.hydroapi.constants.Endpoints.SYSTEM_ID_PERMISSIONS_ID_PATH;
import static com.uciekinierzy.hydroapi.constants.Endpoints.SYSTEM_ID_PERMISSIONS_PATH;
import static org.springframework.http.ResponseEntity.*;

@RestController
@Slf4j
@RequiredArgsConstructor
public class PermissionController {
    private final PermissionService permissionService;
    private final SystemService systemService;
    private final HttpServletRequest request;

    @GetMapping(SYSTEM_ID_PERMISSIONS_PATH)
    public ResponseEntity getPermissions(@PathVariable String systemId) throws NoSuchResourceException, NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        return ok(permissionService.findPermissionBySystemId(systemId));
    }

    @PostMapping(SYSTEM_ID_PERMISSIONS_PATH)
    public ResponseEntity setPermission(@PathVariable String systemId, @RequestBody PermissionTO permissionTO) throws NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);

        PermissionTO permission = permissionService.createPermission(permissionTO);

        try {
            return created(new URI(SYSTEM_ID_PERMISSIONS_ID_PATH.replace("{systemId}", systemId).replace("{permissionId}", permission.getId())))
                    .body(permission);
        } catch (URISyntaxException e) {
            return ok(permission);
        }
    }

    @DeleteMapping(SYSTEM_ID_PERMISSIONS_ID_PATH)
    public ResponseEntity deletePermission(@PathVariable String systemId, @PathVariable String permissionId) throws NoSuchResourceException, NoAccessException, CannotDeleteOwnerPermissionException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        permissionService.deletePermissionById(permissionId);
        return accepted().build();
    }
}
