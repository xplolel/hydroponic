package com.uciekinierzy.hydroapi.svcmeasures;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.uciekinierzy.hydroapi.svcnotifications.NotificationType;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum WaterLevel {
    LOW("LOW"),
    MEDIUM("MEDIUM"),
    FULL("FULL");

    private final String value;

    @Override
    public String toString()
    {
        return value;
    }

    @JsonCreator
    public static NotificationType fromText(String text)
    {
        return NotificationType.valueOf(text);
    }
}
