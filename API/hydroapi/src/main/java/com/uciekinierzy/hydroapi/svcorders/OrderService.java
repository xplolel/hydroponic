package com.uciekinierzy.hydroapi.svcorders;

import com.google.common.collect.Lists;
import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.system.AuthorizationService;
import com.uciekinierzy.hydroapi.svcsystems.system.ModuleService;
import com.uciekinierzy.hydroapi.svcsystems.system.SystemService;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.ModuleTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SettingTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemTO;
import com.uciekinierzy.hydroapi.utils.AuthUtils;
import com.uciekinierzy.hydroapi.utils.PaginationUtils;
import com.uciekinierzy.hydroapi.watchers.WatcherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.isEmpty;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {
    private final OrderRepo orderRepo;
    private final ModuleService moduleService;
    private final SystemService systemService;
    private final WatcherService watcherService;

    public List<OrderTO> getOrdersForClient(String token, Integer limit, Integer offset, Boolean notExecutedOnly) {
        List<String> systemsIdsForThisClient = getModulesIdsForThisClient(token);

        if(AuthorizationService.isTokenAServer(token)) {
            try{
                watcherService.systemLooper(systemService.getSystemsClientHasAccessTo(token).get(0).getId());
            }
            catch (Throwable e){
                log.warn("While trying to report looper", e);
            }
        }

        boolean getNotExecutedOnly = notExecutedOnly != null ? notExecutedOnly : false;

        final List<OrderTO> collect = systemsIdsForThisClient.stream()
                .map(orderRepo::getAllByModuleId)
                .flatMap(Collection::stream)
                .map(Order::toTO)
                .filter(n -> !getNotExecutedOnly || !n.getExecuted())
                .sorted(Comparator.comparing(OrderTO::getCreated))
                .collect(Collectors.toList());

        return PaginationUtils.paginateList(collect, limit, offset);
    }

    void executed(String id, String token) throws NoSuchResourceException, NoAccessException {
        Order order = orderRepo.findById(id)
                .orElseThrow(NoSuchResourceException::new);

        AuthUtils.checkModuleAccessAndThrowException(order.getModuleId(), token);

        order.setExecuted(true);
        order.setExecutedAt(LocalDateTime.now());

        orderRepo.save(order);
    }

    OrderTO postOrder(OrderAddingTO orderAddingTO) throws IncorrectTOException {
        if(isEmpty(orderAddingTO.getShortNote()) || orderAddingTO.getOrderType() == null)
            throw new IncorrectTOException();

        if(orderAddingTO.getOrderType().equals(OrderType.HALF_LIGHT_ON))
            orderAddingTO.setOrderType(OrderType.FULL_LIGHT_ON);

        if(orderAddingTO.getOrderType().equals(OrderType.HALF_LIGHT_OFF))
            orderAddingTO.setOrderType(OrderType.FULL_LIGHT_OFF);

        Order order = Order.builder()
                .id(UUID.randomUUID().toString())
                .executed(false)
                .created(orderAddingTO.getCreated() != null ? orderAddingTO.getCreated() : LocalDateTime.now())
                .shortNote(orderAddingTO.getShortNote())
                .moduleId(orderAddingTO.getModuleId())
                .orderType(orderAddingTO.getOrderType())
                .build();

        orderRepo.save(order);
        return order.toTO();
    }

    private List<String> getModulesIdsForThisClient(String jwt) {
        return systemService.getSystemsClientHasAccessTo(jwt)
                .stream()
                .map(SystemTO::getId)
                .map(this::getModuleTOBySystemId)
                .flatMap(Collection::stream)
                .map(ModuleTO::getId)
                .collect(Collectors.toList());
    }

    private List<ModuleTO> getModuleTOBySystemId(String sid) {
        List<ModuleTO> result = Lists.newArrayList();
        try {
            result.addAll(moduleService.getModulesBySystem(sid));
        } catch (NoSuchResourceException e) {
            //intended
        }
        return result;
    }

    public OrderTO get(String id, String token) throws NoSuchResourceException, NoAccessException {
        OrderTO order = orderRepo.findById(id)
                .orElseThrow(NoSuchResourceException::new)
                .toTO();

        AuthUtils.checkModuleAccessAndThrowException(order.getModuleId(), token);

        return order;
    }

    public LightStateTO getLightState(String token, String moduleId, SettingTO setting) {
        List<OrderTO> ordersExecuted = getModulesIdsForThisClient(token)
                .stream()
                .map(orderRepo::getAllByModuleId)
                .flatMap(Collection::stream)
                .map(Order::toTO)
                .filter(OrderTO::getExecuted)
                .sorted(Comparator.comparing(OrderTO::getExecutedAt))
                .collect(Collectors.toList());

        boolean lightOnBySetting = isLightOnBySetting(setting, LocalDateTime.now());

        if(ordersExecuted.isEmpty()){
            boolean halfLight = setting.getHalfLight() == null ? false : setting.getHalfLight();
            return new LightStateTO(moduleId, lightOnBySetting, halfLight);
        }
        else {
            OrderTO lastOrder = ordersExecuted.get(ordersExecuted.size() - 1);
            return getLightStateTO(moduleId, setting, lightOnBySetting, lastOrder);
        }
    }

    LightStateTO getLightStateTO(String moduleId, SettingTO setting, boolean lightOnBySetting, OrderTO lastOrder) {
        boolean settingOverrodeOrder = hasSettingScheduleOverrodeOrder(setting, LocalDateTime.now(), lastOrder.getExecutedAt());

        if(settingOverrodeOrder){
            boolean halfLight = setting.getHalfLight() == null ? false : setting.getHalfLight();
            return new LightStateTO(moduleId, lightOnBySetting, halfLight);
        }
        else {
            boolean lightOn = lastOrder.getOrderType().equals(OrderType.FULL_LIGHT_ON) || lastOrder.getOrderType().equals(OrderType.HALF_LIGHT_ON);
            boolean halfLight = lastOrder.getOrderType().equals(OrderType.HALF_LIGHT_ON);
            return new LightStateTO(moduleId, lightOn, halfLight);
        }
    }

    boolean isLightOnBySetting(SettingTO setting, LocalDateTime now) {
        if(setting.getFromHourUTC() <= setting.getToHourUTC()){
            LocalDateTime from = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), setting.getFromHourUTC(), setting.getFromMinuteUTC());
            LocalDateTime to = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), setting.getToHourUTC(), setting.getToMinuteUTC());

            return now.isAfter(from) && now.isBefore(to);
        }
        else {
            if(now.getHour() > setting.getFromHourUTC()) {
                LocalDateTime from = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), setting.getFromHourUTC(), setting.getFromMinuteUTC());
                LocalDateTime to = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth() + 1, setting.getToHourUTC(), setting.getToMinuteUTC());
                return now.isAfter(from) && now.isBefore(to);
            }
            else {
                LocalDateTime from = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth() - 1, setting.getFromHourUTC(), setting.getFromMinuteUTC());
                LocalDateTime to = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), setting.getToHourUTC(), setting.getToMinuteUTC());
                return now.isAfter(from) && now.isBefore(to);
            }
        }
    }

    boolean hasSettingScheduleOverrodeOrder(SettingTO setting, LocalDateTime now, LocalDateTime order) {
        if(setting.getFromHourUTC() <= setting.getToHourUTC()){
            LocalDateTime from = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), setting.getFromHourUTC(), setting.getFromMinuteUTC());
            LocalDateTime to = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), setting.getToHourUTC(), setting.getToMinuteUTC());

            return order.isBefore(from) && now.isAfter(from)  ||  to.isAfter(order) && now.isAfter(to);
        }
        else {
            LocalDateTime from = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), setting.getFromHourUTC(), setting.getFromMinuteUTC());
            LocalDateTime to = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth()+1, setting.getToHourUTC(), setting.getToMinuteUTC());

            return order.isBefore(from) && now.isAfter(from)  ||  to.isAfter(order) && now.isAfter(to);
        }
    }
}
