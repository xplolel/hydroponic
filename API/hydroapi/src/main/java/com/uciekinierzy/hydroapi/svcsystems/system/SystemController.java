package com.uciekinierzy.hydroapi.svcsystems.system;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.helpers.SlotOccupiedException;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.*;
import com.uciekinierzy.hydroapi.utils.AuthUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;
import static com.uciekinierzy.hydroapi.utils.ControllerUtils.getToken;
import static com.uciekinierzy.hydroapi.utils.JwtUtils.getEmailFromTokenWithoutAuth;
import static com.uciekinierzy.hydroapi.utils.JwtUtils.getIdFromTokenWithoutAuth;
import static java.lang.String.format;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.*;

@RestController(SYSTEMS_CONTROLLER)
@RequiredArgsConstructor
@Slf4j
public class SystemController
{
    private final HttpServletRequest request;
    private final SystemService systemService;
    private final ModuleService moduleService;
    private final SlotService slotService;
    private final SystemRegistrationService systemRegistrationService;

    @GetMapping(value = SYSTEM_ALL_PATH)
    public ResponseEntity getMySystems(HttpServletRequest request) {
        String token = getToken(request);
        return ok(systemService.getSystemsClientHasAccessTo(token));
    }

    @GetMapping(value = SYSTEM_ID_PATH)
    public ResponseEntity getSystemById(@PathVariable String systemId) throws NoSuchResourceException, NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        return ok(systemService.getSystemById(systemId));
    }

    @PostMapping(value = SYSTEM_ALL_PATH)
    public ResponseEntity addSystem(@RequestBody SystemAddingTO systemAddingTO)
    {//TODO obsolete - we should use register only
        try {
            String email = getEmailFromTokenWithoutAuth(getToken(request));
            if(!email.equals(systemAddingTO.getOwnerEmail())){
                log.error(format("Denying system creation - email difference. Email from token = %s, Request %s", email, systemAddingTO));
                return badRequest().body("Id from JWT is different than Id in request");
            }
            else {
                if(systemAddingTO.getOwnerEmail() == null)
                    systemAddingTO.setOwnerEmail(email);
                if(systemAddingTO.getOwnerId() == null)
                    systemAddingTO.setOwnerId(getIdFromTokenWithoutAuth(getToken(request)));

                SystemRegistrationResponseTO systemRegistrationResponseTO = systemService.addMockSystem(systemAddingTO);
                return created(URI.create(SYSTEM_ALL_PATH + systemRegistrationResponseTO.getSystem().getId()))
                        .body(systemRegistrationResponseTO);
            }
        }
        catch (Exception e) {
            log.error("", e);
            return unprocessableEntity().body(systemAddingTO);
        }
    }

    @PatchMapping(value = SYSTEM_ID_PATH)
    public ResponseEntity patchSystemById(@PathVariable String systemId, @RequestBody SystemPatchingTO patchingTO) throws NoSuchResourceException, NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        return ok(systemService.patchSystem(systemId, patchingTO));
    }

    @PostMapping(value = SYSTEM_REGISTRATION_PATH)
    public ResponseEntity systemRegistration(@RequestBody SystemRegistrationTO registrationTO) throws NoSuchResourceException, IncorrectTOException {
        return ok(systemRegistrationService.register(registrationTO));
    }

    @PostMapping(MODULE_PATH)
    public ResponseEntity addModule(@PathVariable String systemId, @RequestBody ModuleAddingTO moduleAddingTO) throws URISyntaxException, IncorrectTOException, NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);

        ModuleTO moduleTO = moduleService.addModule(moduleAddingTO);
        URI uri = new URI("/systems/module/" + moduleTO.getId());
        return created(uri).body(moduleTO);
    }

    @GetMapping(MODULE_PATH)
    public ResponseEntity getModules(@PathVariable String systemId, HttpServletRequest request) throws NoSuchResourceException, NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        return ok(moduleService.getModulesBySystem(systemId));
    }

    @GetMapping(MODULE_ID_PATH)
    public ResponseEntity getModule(@PathVariable String moduleId, @PathVariable String systemId) throws NoSuchResourceException, NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        ModuleTO module = moduleService.getModuleById(moduleId);
        return ok(module);
    }

    @PatchMapping(MODULE_ID_PATH)
    public ResponseEntity getModule(@PathVariable String moduleId, @PathVariable String systemId, @RequestBody ModulePatchingTO modulePatchingTO) throws NoSuchResourceException, NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        ModuleTO module = moduleService.patchModule(moduleId, modulePatchingTO);
        return ok(module);
    }

    @DeleteMapping(MODULE_ID_PATH)
    public ResponseEntity deleteModule(@PathVariable String moduleId, @PathVariable String systemId) throws NoSuchResourceException, NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        moduleService.removeModule(moduleId);
        return status(OK).build();
    }

    @GetMapping(SLOTS_INFO_PATH)
    public ResponseEntity getSlotsInfo(){
        return ok(new Info(SlotService.SLOTS_IN_ROW, SlotService.SLOTS_ROWS));
    }

    @GetMapping(SLOTS_IN_ROW_PATH)
    public ResponseEntity getSlotsInRowInfo(){
        return ok(SlotService.SLOTS_IN_ROW);
    }

    @GetMapping(SLOTS_ROWS_PATH)
    public ResponseEntity getSlotsRowsInfo(){
        return ok(SlotService.SLOTS_ROWS);
    }

    @GetMapping(SLOTS_PATH)
    public ResponseEntity getSlots(@PathVariable String moduleId, @PathVariable String systemId) throws NoAccessException, NoSuchResourceException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        return ok().body(slotService.getSlotsForModule(moduleId));

    }

    @GetMapping(SLOT_ID_PATH)
    public ResponseEntity getSlot(@PathVariable String moduleId, @PathVariable String systemId, @PathVariable String slotId) throws NoAccessException, NoSuchResourceException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        return ok(slotService.getSlot(slotId));
    }

    @PutMapping(SLOT_ID_PATH)
    public ResponseEntity setPlant(@PathVariable String systemId, @PathVariable String slotId, @RequestParam String plantId) throws NoAccessException, NoSuchResourceException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);

        try{
            return ok(slotService.setPlant(slotId, plantId));
        }
        catch (SlotOccupiedException e){
            return status(CONFLICT).build();
        }
    }

    @DeleteMapping(SLOT_ID_PATH)
    public ResponseEntity clearPlant(@PathVariable String systemId, @PathVariable String slotId) throws NoAccessException, NoSuchResourceException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);

        SlotTO slotTO = slotService.clearPlant(slotId);
        return ok(slotTO);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class Info{
        private Long slotsInRow;
        private Long slotsRows;
    }
}
