package com.uciekinierzy.hydroapi.svcsystems.system;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcclients.clients.ClientService;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientTO;
import com.uciekinierzy.hydroapi.svcsystems.offlinesystem.OfflineSystemService;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemRegistrationResponseTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemRegistrationTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class SystemRegistrationService {
    private final SystemService systemService;
    private final ClientService clientService;
    private final OfflineSystemService offlineSystemService;

    @Transactional
    public SystemRegistrationResponseTO register(SystemRegistrationTO systemRegistrationTO) throws NoSuchResourceException, IncorrectTOException {
        if(StringUtils.isEmpty(systemRegistrationTO.getSystemUniqueId()))
            throw new IncorrectTOException("System unique ID is empty");

        if(StringUtils.isEmpty(systemRegistrationTO.getUserId()))
            throw new IncorrectTOException("Systems user ID is empty");

        if(offlineSystemService.findById(systemRegistrationTO.getSystemUniqueId()).isEmpty())
            throw new NoSuchResourceException("There's no offline system of id " + systemRegistrationTO.getSystemUniqueId());

        ClientTO clientTO = clientService.getClientByIdAsTO(systemRegistrationTO.getUserId())
                .orElseThrow(() -> new NoSuchResourceException("No such user of id " + systemRegistrationTO.getUserId()));

        systemRegistrationTO.setUserEmail(clientTO.getEmail());

        SystemRegistrationResponseTO systemRegistrationResponseTO = systemService.registerSystem(systemRegistrationTO);

        if(systemRegistrationResponseTO.getSystem() == null){
            log.error("Created system is nulled for TO: " + systemRegistrationTO);
            throw new IncorrectTOException("Unknown error");
        }

        offlineSystemService.deleteById(systemRegistrationTO.getSystemUniqueId());

        return systemRegistrationResponseTO;
    }
}
