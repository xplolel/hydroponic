package com.uciekinierzy.hydroapi.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtils {
    public static String getStackAsString(Throwable e){
        final StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        final String stack = sw.toString();
        return stack;
    }
}
