package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ModulePatchingTO
{
    private String name;
}
