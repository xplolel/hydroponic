package com.uciekinierzy.hydroapi.svcplants.plants.TOs;

import com.uciekinierzy.hydroapi.svcplants.plants.GrowingDifficultyEnum;
import com.uciekinierzy.hydroapi.svcplants.plants.PlantTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddingPlantTO
{
    private String name;
    private Integer growTime;
    private GrowingDifficultyEnum growDifficulty;
    private PlantTypeEnum plantType;
}
