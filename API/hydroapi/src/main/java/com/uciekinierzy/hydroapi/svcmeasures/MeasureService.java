package com.uciekinierzy.hydroapi.svcmeasures;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.uciekinierzy.hydroapi.svcmeasures.TOs.ModuleMeasureExternalTO;
import com.uciekinierzy.hydroapi.svcmeasures.TOs.SystemMeasureExternalTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MeasureService {
    private final SystemMeasureRepo systemMeasureRepo;
    private final ModuleMeasureRepo moduleMeasureRepo;

    public List<SystemMeasure> getSystemMeasures(String systemId){
        List<SystemMeasure> allByModuleId = systemMeasureRepo.findAllBySystemId(systemId);
        return allByModuleId;
    }

    public SystemMeasure add(SystemMeasure systemMeasure) {
        systemMeasureRepo.save(systemMeasure);
        return systemMeasure;
    }

    public Map<String, Optional<LocalDateTime>> getSystemsLastMeasureTimes(){
        Map<String, Optional<LocalDateTime>> map = Maps.newHashMap();

        for (String system : getSystemsThatPostedMeasures()) {
            Optional<LocalDateTime> time = getLastMeasureReportedTime(system);
            map.put(system, time);
        }

        return map;
    }

    public List<String> getSystemsThatPostedMeasures(){
        return systemMeasureRepo.findDistinctSystemIds();
    }

    public Optional<LocalDateTime> getLastMeasureReportedTime(String systemId){
        return systemMeasureRepo.findAllBySystemId(systemId)
                .stream()
                .map(SystemMeasure::getReportedTime)
                .max((LocalDateTime::compareTo));
    }

    public void deleteOld(){
        //delete 6 months old
        LocalDateTime condition = LocalDateTime.now().minusMonths(6);

        List<SystemMeasure> toDelete = Lists.newArrayList();

        systemMeasureRepo.findAll().forEach(measure -> addIfOlderThanCondition(condition, toDelete, measure));
        systemMeasureRepo.deleteAll(toDelete);
    }

    private void addIfOlderThanCondition(LocalDateTime condition, List<SystemMeasure> toDelete, SystemMeasure systemMeasure) {
        if(systemMeasure.getReportedTime().isBefore(condition))
            toDelete.add(systemMeasure);
    }

    public ModuleMeasure addModuleMeasure(ModuleMeasure measure) {
        moduleMeasureRepo.save(measure);
        return measure;
    }

    public List<ModuleMeasure> getModuleMeasures(String moduleId) {
        return moduleMeasureRepo.findAllByModuleId(moduleId);
    }

    public Optional<ModuleMeasureExternalTO> getLastModuleMeasure(String moduleId){
        return moduleMeasureRepo.findAllByModuleId(moduleId)
                .stream()
                .max(Comparator.comparing(ModuleMeasure::getReportedTime))
                .map(ModuleMeasureExternalTO::newOf);
    }

    public Optional<SystemMeasureExternalTO> getLastSystemMeasure(String systemId){
        return systemMeasureRepo.findAllBySystemId(systemId)
                .stream()
                .max(Comparator.comparing(SystemMeasure::getReportedTime))
                .map(SystemMeasureExternalTO::newOf);
    }
}
