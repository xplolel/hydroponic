package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import io.swagger.annotations.ApiModel;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@ApiModel
public class SystemRegistrationResponseTO {
    private SystemTO system;
    private String token;
}
