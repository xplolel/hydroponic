package com.uciekinierzy.hydroapi.svcclients.clients;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
class PasswordResetToken {
    @Id
    private String token;
    private String clientId;
    private LocalDateTime expires;
}
