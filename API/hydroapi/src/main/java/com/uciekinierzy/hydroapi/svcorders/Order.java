package com.uciekinierzy.hydroapi.svcorders;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "request")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class Order {
    @Id
    private String id;
    @Column(nullable = false)
    private Boolean executed;
    @Column(nullable = false)
    private String moduleId;
    @Column(nullable = false)
    private String shortNote;
    @Column(nullable = false)
    private LocalDateTime created;
    private LocalDateTime executedAt;
    @Column(nullable = false)
    private OrderType orderType;

    OrderTO toTO(){
        return OrderTO.builder()
                .id(id)
                .executed(executed)
                .moduleId(moduleId)
                .shortNote(shortNote)
                .created(created)
                .executedAt(executedAt)
                .orderType(orderType)
                .build();
    }
}
