package com.uciekinierzy.hydroapi.svcmeasures;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;


@RestResource(exported = false)
@Repository
public interface ModuleMeasureRepo extends CrudRepository<ModuleMeasure, Long> {
    List<ModuleMeasure> findAllByModuleId(String moduleId);
}
