package com.uciekinierzy.hydroapi.svcsystems.system;

import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SystemTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Data
@EqualsAndHashCode(exclude = {"modules"})
class System
{
    @Id
    private String id;

    @Column(nullable = false)
    private String ownerId;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "system")
    private Set<Module> modules;

    @Column
    private String token;

    @Column
    private Boolean mock;

    SystemTO toTO()
    {
        return new SystemTO(id,
                ownerId,
                name,
                modules == null ? null : modules.stream().map(Module::toTO).collect(Collectors.toList()));
    }
}
