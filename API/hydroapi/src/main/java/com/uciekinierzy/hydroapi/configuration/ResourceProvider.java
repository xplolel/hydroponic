package com.uciekinierzy.hydroapi.configuration;

import com.google.common.base.Charsets;
import com.uciekinierzy.hydroapi.svcmailing.MailingHelper;

import java.io.IOException;
import java.io.InputStream;

/**
 * TODO Reading from fat jar.
 */
public class ResourceProvider {
    public static String readResourceFileAsString(String resourceFile) throws IOException {
        return new String(readResourceFileAsByte(resourceFile), Charsets.UTF_8);
    }

    public static byte[] readResourceFileAsByte(String resourceLocation) throws IOException {
        InputStream is = MailingHelper.class.getClassLoader().getResourceAsStream(resourceLocation);

        if(is == null) {
            resourceLocation = "BOOT-INF/classes/" + resourceLocation;
            is = MailingHelper.class.getClassLoader().getResourceAsStream(resourceLocation);
        }

        if(is == null)
            throw new IOException("Cannot read resource " + resourceLocation);

        try{
            return is.readAllBytes();
        }
        finally {
            is.close();
        }
    }
}
