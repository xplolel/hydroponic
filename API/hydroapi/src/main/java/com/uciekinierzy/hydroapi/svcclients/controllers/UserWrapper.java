package com.uciekinierzy.hydroapi.svcclients.controllers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
class UserWrapper {
    private String userId;
}
