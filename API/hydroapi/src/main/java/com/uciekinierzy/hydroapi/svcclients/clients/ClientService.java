package com.uciekinierzy.hydroapi.svcclients.clients;

import com.uciekinierzy.hydroapi.configuration.PropsProvider;
import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.*;
import com.uciekinierzy.hydroapi.svcclients.helpers.exceptions.InvalidClientException;
import com.uciekinierzy.hydroapi.svcclients.helpers.exceptions.InvalidTokenException;
import com.uciekinierzy.hydroapi.svcclients.helpers.exceptions.PasswordResetTokenExpiredException;
import com.uciekinierzy.hydroapi.svcclients.helpers.exceptions.SuchClientAlreadyExistsException;
import com.uciekinierzy.hydroapi.svcmailing.MailingFacade;
import com.uciekinierzy.hydroapi.svcmailing.MailingHelper;
import com.uciekinierzy.hydroapi.svcmailing.MailingService;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.uciekinierzy.hydroapi.utils.JwtUtils.getIdFromTokenWithoutAuth;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientService implements UserDetailsService
{
    private final ClientsRepo clientsRepo;
    private final TokenRepo tokenRepo;
    private final PasswordEncoder passwordEncoder;
    private final MailingService mailingService;
    private final MailingHelper mailingHelper;

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Value("${spring.application.name}")
    private String application;

    @Value("${app.jwt.hours}")
    private Long hours;

    public String buildToken(Client client){
        String jwt = Jwts.builder()
                .setIssuer(application)
                .setSubject(client.getEmail())
                .claim("type", "user")
                .claim("id", client.getId()) //TODO to uid
                .claim("email", client.getEmail())
                .setIssuedAt(java.sql.Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(getHours(), ChronoUnit.HOURS)))
                .signWith(
                        SignatureAlgorithm.HS256,
                        client.getSalt().getBytes()
                )
                .compact();

        log.info("key: " + jwt);

        return jwt;
    }

    public Long getIdFromToken(String token) throws InvalidTokenException {
        return Jwts.parser()
                .setSigningKey(getSalt(token).getBytes())
                .parseClaimsJws(token).getBody().get("id", Long.class);
    }

    public String getEmailFromToken(String token) throws InvalidTokenException {
        return Jwts.parser()
                .setSigningKey(getSalt(token).getBytes())
                .parseClaimsJws(token).getBody().get("email", String.class);
    }

    public boolean isTokenValid(String token) {

        try {
            String salt = getSalt(token);
            Jwts.parser().setSigningKey(salt.getBytes()).parseClaimsJws(token);
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException |InvalidTokenException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
//            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }

        return false;
    }

    private String getSalt(String token) throws InvalidTokenException {
        String userIdFromToken = getIdFromTokenWithoutAuth(token);
        if(userIdFromToken == null)
            throw new InvalidTokenException("Token did not contain user id");

        Client clientById = getClientById(userIdFromToken)
                .orElseThrow(() -> new InvalidTokenException("No such user found"));

        return clientById.getSalt();
    }

    private Long getHours(){
        return hours != null ? hours : 72;
    }

    public synchronized ClientTO addClient(ClientRegisterTO clientRegisterTO) throws
            InvalidClientException,
            SuchClientAlreadyExistsException
    {
        Set<ConstraintViolation<ClientRegisterTO>> validate = validator.validate(clientRegisterTO);

        if(!validate.isEmpty())
            throw new InvalidClientException(validate.stream().map(Object::toString).collect(Collectors.joining(", ")));

        if(clientsRepo.findByEmail(clientRegisterTO.getEmail()).isPresent())
            throw new SuchClientAlreadyExistsException("Such client already exists " + clientRegisterTO.getEmail());

        Client client = Client.builder()
                .id(UUID.randomUUID().toString())
                .email(clientRegisterTO.getEmail())
                .registered(LocalDateTime.now())
                .roles("USER")
                .salt(UUID.randomUUID().toString())
                .build();

        client.setPasswordHash(passwordEncoder.encode(clientRegisterTO.getPassword()));
        clientsRepo.save(client);

        if(PropsProvider.isAppInTesting())
            log.info("Not sending welcome mail - testing");
        else
            MailingFacade.sendWelcomeEmail(client.getEmail(), Optional.empty());

        return client.toTO();
    }

    @Transactional// adding roles for client
    public void addRoles(List<String> roles){
        String roleDelimiter = ClientSpringAuthTO.ROLE_DELIMITER;

    }

    public List<Client> getAllClients()
    {
        List<Client> result = new ArrayList<>();
        clientsRepo.findAll().forEach(result::add);
        return result;
    }

    public Optional<Client> getClientById(String id)
    {
        return clientsRepo.findById(id);
    }

    public Optional<ClientTO> getClientByIdAsTO(String id)
    {
        return clientsRepo
                .findById(id)
                .flatMap(c -> Optional.of(c.toTO()));
    }

    public Optional<ClientTO> getClientByLoginAsTO(String login)
    {
        return clientsRepo
                .findByEmail(login)
                .flatMap(c -> Optional.of(c.toTO()));
    }

    public String loginAndGetToken(ClientLoginTO clientLoginTO) throws InvalidClientException {
        log.debug(clientLoginTO.toString());
        log.debug(clientsRepo.toString());

        Client client = clientsRepo.findByEmail(clientLoginTO.getEmail())
                                    .orElseThrow(() -> new InvalidClientException("No such client " + clientLoginTO.getEmail()));

        String token = buildToken(client);
        return token;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Client client = clientsRepo.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException(""));

        return client.toClientSpringAuthTO();
    }

    public ClientTO updateClient(String id, ClientUpdateTO clientUpdateTO) throws NoAccessException {
        Client client = clientsRepo.findById(id).orElseThrow(NoAccessException::new);

        if(clientUpdateTO.getFirstName() != null)
            client.setFirstName(clientUpdateTO.getFirstName());

        if(clientUpdateTO.getLastName() != null)
            client.setLastName(clientUpdateTO.getLastName());

        if(clientUpdateTO.getTimezoneShift() != null) {
            client.setTimezoneShift(clientUpdateTO.getTimezoneShift());
        }

        clientsRepo.save(client);

        return client.toTO();
    }

    public void forgottenPassword(String email){
        try{
            String resetPasswordLink = generateAndReturnResetPasswordLink(email);
            MailingFacade.sendPasswordResetEmail(email, resetPasswordLink, Optional.empty());
        }
        catch (NoSuchResourceException e){
            log.warn("NoSuchResource on forgotten password for " + email);
        }
    }
    /**
     * Creates new if there already exists some token.
     */
    @Transactional
    String generateAndReturnResetPasswordLink(String email) throws NoSuchResourceException {
        Client client = clientsRepo.findByEmail(email)
                .orElseThrow(NoSuchResourceException::new);

        tokenRepo.findByClientId(client.getId())
                 .ifPresent(tokenRepo::delete);

        String token = generateToken();
        LocalDateTime expires = LocalDateTime.now().plusHours(PropsProvider.getResetPasswordTokenHourlyTimeout());

        PasswordResetToken passwordResetToken = new PasswordResetToken(token, client.getId(), expires);
        tokenRepo.save(passwordResetToken);

        return PropsProvider.getWebUrl() +
                PropsProvider.getWebResetPasswordEndpoint() +
                "/?token=" + token;
    }

    boolean checkResetToken(String token){
        return tokenRepo.findById(token).isPresent();
    }

    @Transactional
    public void changePassword(String token, String email, String newPassword) throws NoSuchResourceException,
            PasswordResetTokenExpiredException,
            IncorrectTOException
    {
        if(newPassword == null || newPassword.isEmpty() || newPassword.length() < 6)
            throw new IncorrectTOException("Password does not match standards");

        PasswordResetToken passwordReset = tokenRepo.findById(token)
                .orElseThrow(NoSuchResourceException::new);

        if(LocalDateTime.now().isAfter(passwordReset.getExpires()))
            throw new PasswordResetTokenExpiredException("Token expired on " + passwordReset.getExpires());

        Client client = clientsRepo.findByEmail(email)
                .orElseThrow(() -> new NoSuchResourceException("There's no client of email " + email));

        tokenRepo.delete(passwordReset);

        String hash = passwordEncoder.encode(newPassword);
        client.setPasswordHash(hash);
        clientsRepo.save(client);
    }

    static String generateToken(){
        return new String(Base64.getEncoder().encode(UUID.randomUUID().toString().getBytes()));
    }
}
