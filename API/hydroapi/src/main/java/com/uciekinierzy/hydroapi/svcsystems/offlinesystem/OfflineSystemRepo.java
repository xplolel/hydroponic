package com.uciekinierzy.hydroapi.svcsystems.offlinesystem;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@RestResource(exported = false)
@Repository
interface OfflineSystemRepo extends CrudRepository<OfflineSystem, String>
{

}
