package com.uciekinierzy.hydroapi.svcorders;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.settings.SettingController;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SettingTO;
import com.uciekinierzy.hydroapi.utils.AuthUtils;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;
import static com.uciekinierzy.hydroapi.utils.ControllerUtils.getToken;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController(ORDERS_CONTROLLER)
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;
    private final HttpServletRequest request;
    private final SettingController settingController;

    @GetMapping(ORDERS_PATH)
    public ResponseEntity getAllOrders(@RequestParam(required = false) Integer limit, @RequestParam(required = false) Integer offset, @RequestParam(required = false) String notExecutedOnly) {
        return ok(orderService.getOrdersForClient(getToken(request), limit, offset, "true".equalsIgnoreCase(notExecutedOnly)));
    }

    @ApiResponses( value = {
            @ApiResponse(code = 403, message = "You don't have access to this resource"),
            @ApiResponse(code = 400, message = "Incorrect json request")
    })
    @PostMapping(ORDERS_PATH)
    public ResponseEntity postOrder(@RequestBody OrderAddingTO orderAddingTO) throws NoSuchResourceException, NoAccessException, IncorrectTOException {
        AuthUtils.checkModuleAccessAndThrowException(orderAddingTO.getModuleId(), request);
        return status(CREATED).body(orderService.postOrder(orderAddingTO));
    }

    @GetMapping(ORDERS_LIGHT_STATE_PATH)
    public ResponseEntity getLightState(@PathVariable String id) throws NoAccessException, NoSuchResourceException {
        AuthUtils.checkModuleAccessAndThrowException(id, request);
        SettingTO setting = (SettingTO) settingController.getSettingsForModule(id).getBody();

        return ResponseEntity.ok(orderService.getLightState(getToken(request), id, setting));
    }

    @ApiResponses( value = {
        @ApiResponse(code = 404, message = "Order not found")
    })
    @GetMapping(ORDERS_ID_PATH)
    public ResponseEntity getOrder(@PathVariable String id) throws NoSuchResourceException, NoAccessException {
        return ok(orderService.get(id, getToken(request)));
    }

    @PatchMapping(ORDERS_ID_PATH)
    public ResponseEntity orderExecuted(@PathVariable String id) throws NoSuchResourceException, NoAccessException {
        orderService.executed(id, getToken(request));
        return ok().build();
    }
}
