package com.uciekinierzy.hydroapi.svcclients.clients;

import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientDetailsTO;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientSpringAuthTO;
import com.uciekinierzy.hydroapi.svcclients.clients.TOs.ClientTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
class Client
{
    @Id
    private String id;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String passwordHash;

    @Column(nullable = false)
    private String roles;

    @Column(nullable = false)
    private String salt;

    @Column(nullable = true)
    private String firstName;
    @Column(nullable = true)
    private String lastName;

    @Column(nullable = true)
    private LocalDateTime registered;

    @Column(nullable = true)
    private Integer timezoneShift;

    @Column(nullable = true)
    private Boolean active;

    ClientTO toTO()
    {
        return new ClientTO(id, email, toClientDetailsTO());
    }

    ClientDetailsTO toClientDetailsTO()
    {
        return new ClientDetailsTO(firstName, lastName, registered, timezoneShift);
    }

    ClientSpringAuthTO toClientSpringAuthTO() {
        return new ClientSpringAuthTO(email, passwordHash, roles);
    }
}
