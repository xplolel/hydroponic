package com.uciekinierzy.hydroapi.svcmeasures;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ModuleMeasure {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @Column(nullable = false)
    private String moduleId;

    @Column(nullable = false)
    private LocalDateTime reportedTime;

    @Column(nullable = false)
    private WaterLevel waterLevel;
}
