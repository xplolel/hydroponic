package com.uciekinierzy.hydroapi.svcclients.clients.TOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientLoginTO
{
    private String email;
    private String password;
}
