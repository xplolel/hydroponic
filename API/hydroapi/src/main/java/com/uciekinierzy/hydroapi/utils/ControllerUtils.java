package com.uciekinierzy.hydroapi.utils;

import javax.servlet.http.HttpServletRequest;

import static com.uciekinierzy.hydroapi.constants.Endpoints.AUTHORIZATION;
import static com.uciekinierzy.hydroapi.utils.JwtUtils.getEmailFromTokenWithoutAuth;
import static com.uciekinierzy.hydroapi.utils.JwtUtils.getIdFromTokenWithoutAuth;

public class ControllerUtils {
    public static String getToken(HttpServletRequest request){
        return request.getHeader(AUTHORIZATION).replace("Bearer ", "");
    }

    public static String getClientId(HttpServletRequest request) {
        return getIdFromTokenWithoutAuth(getToken(request));
    }

    public static String getClientEmail(HttpServletRequest request) {
        return getEmailFromTokenWithoutAuth(getToken(request));
    }
}
