package com.uciekinierzy.hydroapi.watchers;

public interface Watcher {
    int getIntervalInMinutes();
    Runnable call();
}
