package com.uciekinierzy.hydroapi.svcsystems.permission;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;
import java.util.Optional;

@RestResource(exported = false)
public interface PermissionRepo extends CrudRepository<Permission, String> {
    List<Permission> findAllByHolderEmail(String holderEmail);

    List<Permission> findBySystemId(String systemId);

    Optional<Permission> findByHolderEmailAndSystemId(String holderEmail, String systemId);
}
