package com.uciekinierzy.hydroapi.svcmailing;

public class IncorrectMailFormat extends Exception {
    public IncorrectMailFormat() {
        super();
    }

    public IncorrectMailFormat(String message) {
        super(message);
    }

    public IncorrectMailFormat(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectMailFormat(Throwable cause) {
        super(cause);
    }

    protected IncorrectMailFormat(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
