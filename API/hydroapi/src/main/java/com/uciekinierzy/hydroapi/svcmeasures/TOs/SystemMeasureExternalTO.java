package com.uciekinierzy.hydroapi.svcmeasures.TOs;

import com.uciekinierzy.hydroapi.svcmeasures.SystemMeasure;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class SystemMeasureExternalTO {
    private LocalDateTime reportedTime;
    private Float temperature;
    private Float humidity;

    public static SystemMeasureExternalTO newOf(SystemMeasure systemMeasure){
        return builder()
                .reportedTime(systemMeasure.getReportedTime())
                .humidity(systemMeasure.getHumidity())
                .temperature(systemMeasure.getTemperature())
                .build();
    }
}
