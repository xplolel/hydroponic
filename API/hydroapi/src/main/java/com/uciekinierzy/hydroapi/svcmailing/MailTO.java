package com.uciekinierzy.hydroapi.svcmailing;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Builder
public class MailTO {
    @Email
    String from;
    @Email
    String to;
    @NotBlank
    String subject;
    String content;
}
