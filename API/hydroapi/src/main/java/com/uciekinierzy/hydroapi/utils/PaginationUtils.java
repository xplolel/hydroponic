package com.uciekinierzy.hydroapi.utils;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class PaginationUtils {
    @Value("${app.pagination.default.limit}")
    private static int defaultLimit = 20;
    @Value("${app.pagination.default.offset}")
    private static int defaultOffset = 0;

    public static <T> List<T> paginateList(List<T> collection, Integer limit, Integer offset) {
        return (List<T>) paginateCollection(collection, limit, offset);
    }

    public static <T> Collection<T> paginateCollection(Collection<T> collection, Integer limit, Integer offset) {
        int actualOffset = offset != null && offset >= 0 ? offset : defaultOffset;
        int actualLimit = limit != null && limit >= 0 ? limit : defaultLimit;

        return collection.stream()
                .skip(actualOffset)
                .limit(actualLimit)
                .collect(Collectors.toCollection(Lists::newLinkedList));
    }
}
