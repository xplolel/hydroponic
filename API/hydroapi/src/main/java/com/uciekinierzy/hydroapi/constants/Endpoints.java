package com.uciekinierzy.hydroapi.constants;

public class Endpoints {

    /*** ALL ***/
    public static final String AUTHORIZATION = "Authorization";
    public static final String APPLICATION_JSON = "application/json";

    /*** SVC_CLIENTS ***/
    public static final String CLIENTS_CONTROLLER = "/clients";
    private static final String CLIENTS = CLIENTS_CONTROLLER;

    public static final String CLIENT_ID_PATH = CLIENTS + "/{id}";
    public static final String ALL_CLIENTS_PATH = CLIENTS + "/";

    public static final String REGISTER_SAMPLE_ENDPOINT_URL = CLIENTS + "/register-sample";
    public static final String REGISTER_ENDPOINT_URL = CLIENTS + "/register";

    public static final String LOGIN_ENDPOINT_URL = CLIENTS + "/login";
    public static final String WHO_AM_I = CLIENTS + "/whoami";

    public static final String RESET_PASSWORD_ENDPOINT_URL = CLIENTS + "-reset-password";
    public static final String FORGOT_PASSWORD_ENDPOINT_URL = CLIENTS + "-forgotten-password";


    /*** SVC_PLANTS ***/
    public static final String PLANTS_CONTROLLER = "/plants";
    private static final String PLANTS = PLANTS_CONTROLLER;
    public static final String PLANTS_SAMPLE = PLANTS + "/sample";

    public static final String PLANTS_ID_PATH = PLANTS + "/{id}";
    public static final String ALL_PLANTS_PATH = PLANTS ;


    public static final String PLANTS_ID_UPLOAD_IMAGE_PATH = PLANTS_ID_PATH + "/image";
    public static final String PLANTS_ID_IMAGE_FULL_PATH = PLANTS_ID_PATH + "/image/full";
    public static final String PLANTS_ID_IMAGE_MEDIUM_PATH = PLANTS_ID_PATH + "/image/medium";
    public static final String PLANTS_ID_IMAGE_SMALL_PATH = PLANTS_ID_PATH + "/image/small";


    /*** SVC_SYSTEMS ***/
    public static final String SYSTEMS_CONTROLLER = "/systems";
    private static final String SYSTEMS = SYSTEMS_CONTROLLER;

    public static final String SYSTEM_ALL_PATH = SYSTEMS + "/";
    public static final String SYSTEM_REGISTRATION_PATH = SYSTEMS + "-register";
    public static final String SYSTEM_ID_PATH = SYSTEMS + "/{systemId}";

    public static final String MODULE_PATH =  SYSTEM_ID_PATH + "/modules";
    public static final String MODULE_ID_PATH = MODULE_PATH + "/{moduleId}";

    public static final String SLOTS_PATH = MODULE_ID_PATH + "/slots";
    public static final String SLOT_ID_PATH = MODULE_ID_PATH + "/slot/{slotId}";

    public static final String SLOTS_INFO_PATH = SYSTEMS + "/info";
    public static final String SLOTS_IN_ROW_PATH = SYSTEMS + "/info/slotsInRow";
    public static final String SLOTS_ROWS_PATH = SYSTEMS + "/info/slotsRows";

    public static final String SYSTEM_ID_PERMISSIONS_PATH = SYSTEM_ID_PATH + "/permissions";
    public static final String SYSTEM_ID_PERMISSIONS_ID_PATH = SYSTEM_ID_PATH + "/permissions/{permissionId}";

    /*** SVC_MEASURES ***/
    public static final String MEASURES_PATH = "/measures";
    public static final String SYSTEM_MEASURES_PATH = MEASURES_PATH + "/system";
    public static final String MODULE_MEASURES_PATH = MEASURES_PATH + "/module";
    public static final String SYSTEM_MEASURES_ID_PATH = SYSTEM_MEASURES_PATH + "/{systemId}";
    public static final String MODULE_MEASURES_ID_PATH = MODULE_MEASURES_PATH + "/{moduleId}";

    /*** SVC_SETTINGS ***/
    public static final String SETTINGS_PATH = "/settings";
    public static final String SETTINGS_SYSTEM_ID_PATH = SETTINGS_PATH + "/system/{systemId}";
    public static final String SETTINGS_MODULE_ID_PATH = SETTINGS_PATH + "/module/{moduleId}";
    //TODO next, switch to this
//    public static final String SETTINGS_MODULE_ID_PATH = SETTINGS_SYSTEM_ID_PATH + "/module/{moduleId}";

    /*** SVC_NOTIFCATIONS ***/
    public static final String NOTIFCATIONS_CONTROLLER = "/notifications";
    public static final String NOTIFCATIONS_PATH = "/notifications";
    public static final String NOTIFCATIONS_ID_PATH = "/notifications/{id}";
    public static final String NOTIFCATIONS_SUBSCRIBE = "/notifications-subscribe";

    /*** SVC_ORDERS ***/
    public static final String ORDERS_CONTROLLER = "/orders";
    public static final String ORDERS_PATH = "/orders";
    public static final String ORDERS_LIGHT_STATE_PATH = "/lightState/{id}";
    public static final String ORDERS_ID_PATH = "/orders/{id}";

    /*** SVC_OFFLINE_SYSTEMS ***/
    public static final String OFFLINE_SYSTEMS_CONTROLLER = "/offline-systems";
}
