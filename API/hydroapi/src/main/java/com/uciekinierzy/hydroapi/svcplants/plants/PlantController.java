package com.uciekinierzy.hydroapi.svcplants.plants;

import com.google.common.collect.Lists;
import com.uciekinierzy.hydroapi.svcplants.plants.TOs.AddingPlantTO;
import com.uciekinierzy.hydroapi.svcplants.plants.TOs.PlantTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;
import static com.uciekinierzy.hydroapi.utils.ControllerUtils.getToken;
import static com.uciekinierzy.hydroapi.utils.JwtUtils.getIdFromTokenWithoutAuth;

@RestController(PLANTS_CONTROLLER)
@RequiredArgsConstructor
@Slf4j
public class PlantController
{
    private final PlantService plantService;
    private final HttpServletRequest request;

    @GetMapping(PLANTS_SAMPLE)
    public ResponseEntity getSample()
    {
        return ResponseEntity.ok(
                Lists.newArrayList(
                        AddingPlantTO.builder()
                                .name("plant name 1")
                                .growTime(10)
                                .growDifficulty(GrowingDifficultyEnum.EASY)
                                .plantType(PlantTypeEnum.SPICES)
                                .build(),

                        AddingPlantTO.builder()
                                .name("plant name 2")
                                .growTime(15)
                                .growDifficulty(GrowingDifficultyEnum.MEDIUM)
                                .plantType(PlantTypeEnum.VEGETABLE)
                                .build(),

                        AddingPlantTO.builder()
                                .name("plant name 2")
                                .growTime(15)
                                .growDifficulty(GrowingDifficultyEnum.MEDIUM)
                                .plantType(PlantTypeEnum.HOME_PLANT)
                                .build(),

                        AddingPlantTO.builder()
                                .name("plant name 3")
                                .growTime(3)
                                .growDifficulty(GrowingDifficultyEnum.ADVANCED)
                                .plantType(PlantTypeEnum.FRUIT)
                                .build()
                )
        );
    }

    @GetMapping(ALL_PLANTS_PATH)
    public ResponseEntity getAll()
    {
        return ResponseEntity.ok().body(plantService.getAll());
    }

    @PostMapping(ALL_PLANTS_PATH)
    public ResponseEntity addPlant(@RequestBody AddingPlantTO addingPlantTO)
    {
        PlantTO plantTO = PlantTO.builder()
                .name(addingPlantTO.getName())
                .growDifficulty(addingPlantTO.getGrowDifficulty())
                .plantType(addingPlantTO.getPlantType())
                .growTime(addingPlantTO.getGrowTime())
                .build();

        plantTO.setCreatedTime(LocalDateTime.now());
        plantTO.setCreatedBy(getIdFromTokenWithoutAuth(getToken(request)));

        try {
            PlantTO plant = plantService.addPlant(plantTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(plant);
        }
        catch (PlantTOInvalidForAddingException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping(PLANTS_ID_PATH)
    public ResponseEntity getById(@PathVariable String id)
    {
        try
        {
            return ResponseEntity.ok().body(plantService.getById(id));
        }
        catch (NoSuchPlantException e)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(PLANTS_ID_UPLOAD_IMAGE_PATH)
    public ResponseEntity addPlantImage(@PathVariable String id, @RequestParam("file")MultipartFile file)
    {
        try
        {
            plantService.addImage(id, file);
            return ResponseEntity.ok("Added.");
        }
        catch (Exception e)
        {
            log.info("While adding image", e);
            return ResponseEntity.status(400).build();
        }
    }

    @GetMapping(value = PLANTS_ID_IMAGE_FULL_PATH,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity getPlantImageFull(@PathVariable String id)
    {
        try
        {
            byte[] plantImage = plantService.getImageFull(id);
            return ResponseEntity.ok()
                    .cacheControl(CacheControl.maxAge(300, TimeUnit.SECONDS))
                    .body(plantImage);
        }
        catch (NoImageForGivenPlantException e)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = PLANTS_ID_IMAGE_MEDIUM_PATH,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity getPlantImageMedium(@PathVariable String id)
    {
        try
        {
            byte[] plantImage = plantService.getImageMedium(id);
            return ResponseEntity.ok()
                    .cacheControl(CacheControl.maxAge(300, TimeUnit.SECONDS))
                    .body(plantImage);
        }
        catch (NoImageForGivenPlantException e)
        {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = PLANTS_ID_IMAGE_SMALL_PATH,
            produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity getPlantImageSmall(@PathVariable String id)
    {
        try
        {
            byte[] plantImage = plantService.getImageSmall(id);
            return ResponseEntity.ok()
                    .cacheControl(CacheControl.maxAge(300, TimeUnit.SECONDS))
                    .body(plantImage);
        }
        catch (NoImageForGivenPlantException e)
        {
            return ResponseEntity.notFound().build();
        }
    }
}
