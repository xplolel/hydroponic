package com.uciekinierzy.hydroapi.svcplants.plants;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum GrowingDifficultyEnum
{
    EASY("EASY"),
    MEDIUM("MEDIUM"),
    ADVANCED("ADVANCED");

    private final String value;

    @Override
    public String toString()
    {
        return value;
    }

    @JsonCreator
    public static GrowingDifficultyEnum fromText(String text)
    {
        return GrowingDifficultyEnum.valueOf(text);
    }
}
