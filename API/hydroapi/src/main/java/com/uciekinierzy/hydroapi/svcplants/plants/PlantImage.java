package com.uciekinierzy.hydroapi.svcplants.plants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
class PlantImage
{
    @Id
    private String plantId;

    @Lob
    private byte[] imageFull;

    @Lob
    private byte[] image140;

    @Lob
    private byte[] image400;
}
