package com.uciekinierzy.hydroapi.svcmailing;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class MailingFacade {
    private final MailingHelper mailingHelper;
    private final MailingService mailingService;

    private static MailingFacade instance;

    @PostConstruct
    private void init() {
        instance = this;
    }

    /**
     * .
     * @param clientEmail email to send welcome email to.
     * @param locale - end by default.
     */
    public static void sendWelcomeEmail(String clientEmail, Optional<String> locale){
        try {
            instance.mailingService.sendMail(
                    MailTO.builder()
                            .subject("Welcome to the Hydroponic")
                            .content(instance.mailingHelper.getWelcomeContent(locale.isEmpty() ? "en" : locale.get()))
                            .to(clientEmail)
                            .build()
            );
        }
        catch (IncorrectMailFormat | MailingException e) {
            log.error("While trying to send welcome email", e);
        }
    }

    public static void sendPasswordResetEmail(String clientEmail, String resetLink, Optional<String> locale){
        try {
            String content = instance.mailingHelper.getPasswordResetContent(locale.isEmpty() ? "en" : locale.get(), resetLink);

            instance.mailingService.sendMail(
                    MailTO.builder()
                            .subject("Password reset")
                            .content(content)
                            .to(clientEmail)
                            .build()
            );
        }
        catch (IncorrectMailFormat | MailingException e) {
            log.error("While trying to password reset email", e);
        }
    }
}
