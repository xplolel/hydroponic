package com.uciekinierzy.hydroapi.svcsystems.permission;

import com.uciekinierzy.hydroapi.svcsystems.system.TOs.PermissionTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Permission {
    @Id
    private String id;

    @Column(nullable = false)
    private String systemId;
    @Column(nullable = false)
    private String holderEmail;
    @Column(nullable = false)
    private PermissionType permissionType;

    PermissionTO toTO(){
        return PermissionTO.builder()
                .holderEmail(holderEmail)
                .systemId(systemId)
                .id(id)
                .permissionType(permissionType)
                .build();
    }
}
