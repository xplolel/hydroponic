package com.uciekinierzy.hydroapi.svcnotifications;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class Notification {
    @Id
    private String id;
    @Column(nullable = false)
    private Boolean read;
    @Column(nullable = false)
    private String systemId;
    @Column(nullable = false)
    private String shortNote;
    @Column(nullable = false)
    private String longNote;
    @Column(nullable = false)
    private LocalDateTime created;
    @Column(nullable = false)
    private NotificationType notificationType;
    @Column
    private NotificationType2 notificationType2;

    NotificationTO toTO(){
        return NotificationTO.builder()
                .id(id)
                .read(read)
                .systemId(systemId)
                .shortNote(shortNote)
                .longNote(longNote)
                .created(created)
                .notificationType2(notificationType2)
                .notificationType(notificationType)
                .build();
    }
}
