package com.uciekinierzy.hydroapi.svcsystems.settings;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SettingTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class SettingService {
    private final SettingRepo settingRepo;

    public List<SettingTO> getSettingForModuleIds(List<String> moduleIds){
        return moduleIds.stream()
                .map(settingRepo::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(Setting::toTO)
                .collect(Collectors.toList());
    }

    public SettingTO getSettingForModule(String moduleId) throws NoSuchResourceException {
        return settingRepo.findById(moduleId)
                .orElseThrow(NoSuchResourceException::new)
                .toTO();
    }

    public SettingTO createSetting(SettingTO settingTO) throws IncorrectTOException {
        checkSettingTO(settingTO);
        Setting setting = new Setting();
        applySettingTO(settingTO, setting);
        settingRepo.save(setting);
        return setting.toTO();
    }

    private void applySettingTO(SettingTO settingTO, Setting setting) {
        setting.setModuleId     (settingTO.getModuleId());
        setting.setHalfLight    (settingTO.getHalfLight());
        setting.setFromHourUTC  (settingTO.getFromHourUTC());
        setting.setFromMinuteUTC(settingTO.getFromMinuteUTC());
        setting.setToHourUTC    (settingTO.getToHourUTC());
        setting.setToMinuteUTC  (settingTO.getToMinuteUTC());
    }

    @Transactional
    public SettingTO alterSetting(SettingTO settingTO) throws IncorrectTOException, NoSuchResourceException {
        checkSettingTO(settingTO);

        Setting setting = settingRepo.findById(settingTO.getModuleId())
                                     .orElseThrow(() -> new NoSuchResourceException("No setting of id " + settingTO.getModuleId()));

        applySettingTO(settingTO, setting);

        settingRepo.save(setting);
        return settingTO;
    }

    private static void checkSettingTO(SettingTO settingTO) throws IncorrectTOException {
        if (settingTO.getModuleId() == null)
            throw new IncorrectTOException("ModuleId is " + settingTO.getModuleId());
        if (settingTO.getHalfLight() == null)
            throw new IncorrectTOException("Half light is " + settingTO.getHalfLight());
        if (settingTO.getFromHourUTC() == null || settingTO.getFromHourUTC() > 23 || settingTO.getFromHourUTC() < 0)
            throw new IncorrectTOException("Hour from UTC is " + settingTO.getFromHourUTC());
        if (settingTO.getFromMinuteUTC() == null || settingTO.getFromMinuteUTC() > 59 || settingTO.getFromMinuteUTC() < 0)
            throw new IncorrectTOException("Minute from UTC is " + settingTO.getFromMinuteUTC());
        if (settingTO.getToHourUTC() == null || settingTO.getToHourUTC() > 23 || settingTO.getToHourUTC() < 0)
            throw new IncorrectTOException("Hour to UTC is " + settingTO.getToHourUTC());
        if (settingTO.getToMinuteUTC() == null || settingTO.getToMinuteUTC() > 59 || settingTO.getToMinuteUTC() < 0)
            throw new IncorrectTOException("Hour from UTC is " + settingTO.getToMinuteUTC());
    }

    public void deleteSettingIfExists(String moduleId){
        try{
            settingRepo.findByModuleId(moduleId).ifPresent(settingRepo::delete);
        }
        catch (Throwable e){
            log.info("While trying to remove setting for module " + moduleId);
        }
    }
}
