package com.uciekinierzy.hydroapi.svcsystems.system;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RestResource(exported = false)
interface ModuleRepo extends CrudRepository<Module, String>
{
    List<Module> findAllBySystem(System system);
}
