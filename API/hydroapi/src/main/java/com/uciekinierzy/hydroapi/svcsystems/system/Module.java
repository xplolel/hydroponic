package com.uciekinierzy.hydroapi.svcsystems.system;

import com.uciekinierzy.hydroapi.svcsystems.system.TOs.ModuleTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Data
@EqualsAndHashCode(exclude = {"system", "slots"})
class Module
{
    @Id
    private String id;

    private Integer moduleNumber;

    @Column(nullable = false)
    private String moduleName;

    @ManyToOne
    private System system;

    @OneToMany
    private Set<Slot> slots;

    ModuleTO toTO()
    {
        return ModuleTO.builder()
                .id(id)
                .moduleName(moduleName)
                .moduleNumber(moduleNumber)
                .systemId(system.getId())
                .slots(slots == null ? null : slots.stream().map(Slot::toTO).collect(Collectors.toList()))
                .build();

    }
}
