package com.uciekinierzy.hydroapi.watchers;

import com.uciekinierzy.hydroapi.svcnotifications.NotificationAddingTO;
import com.uciekinierzy.hydroapi.svcnotifications.NotificationService;
import com.uciekinierzy.hydroapi.svcnotifications.NotificationType;
import com.uciekinierzy.hydroapi.svcsystems.system.SystemService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
@Slf4j
public class SystemActivityWatcher implements Watcher {
    static LocalDateTime appStartTime = LocalDateTime.now();

    public static final String INACTIVITY_LONG_NOTE = "Please make sure system is properly connected to the internet";
    public static final String NOT_POSTED_NOTIFICATION = "System has not posted measure in a while";
    private final NotificationService notificationService;
    private final SystemService systemService;
    private final WatcherService watcherService;

    @Override
    public int getIntervalInMinutes() {
        return 1;
    }

    @Override
    public Runnable call() {
        return this::checkSystemsActivity;
    }

    private void checkSystemsActivity(){
        if(!ifWasInLastNMin(1, appStartTime))
            watcherService.getSystemsLastLooper().forEach(
                (system, time) ->{
                    if(!ifWasInLastNMin(1, time) && systemShouldBeActive(system)){
                        if(!notificationService.recentlyNotified(system, INACTIVITY_LONG_NOTE, NOT_POSTED_NOTIFICATION, 24))
                            notificationService.postWithoutAuth(NotificationAddingTO.builder()
                                    .systemId(system)
                                    .shortNote(NOT_POSTED_NOTIFICATION)
                                    .notificationType(NotificationType.FAILURE)
                                    .created(LocalDateTime.now())
                                    .longNote(INACTIVITY_LONG_NOTE)
                                    .build());
                    }
                }
        );
    }

    boolean ifWasInLastNMin(int minute, LocalDateTime time) {
        if(time == null)
            return false;
        return LocalDateTime.now().minusMinutes(minute).isBefore(time);
    }

    boolean systemShouldBeActive(String system) {
        boolean systemRegisteredAndShouldItReportMeasures = systemService.isSystemRegisteredAndShouldItReportMeasures(system);
        if(systemRegisteredAndShouldItReportMeasures)
            log.info("System registered and should report measures : " + system);
        return systemRegisteredAndShouldItReportMeasures;
    }
}
