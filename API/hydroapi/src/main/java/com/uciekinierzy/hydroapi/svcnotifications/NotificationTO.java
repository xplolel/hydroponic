package com.uciekinierzy.hydroapi.svcnotifications;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class NotificationTO {
    private String id;
    private Boolean read;
    private String systemId;
    private String shortNote;
    private String longNote;
    private LocalDateTime created;
    private NotificationType notificationType;
    private NotificationType2 notificationType2;
}
