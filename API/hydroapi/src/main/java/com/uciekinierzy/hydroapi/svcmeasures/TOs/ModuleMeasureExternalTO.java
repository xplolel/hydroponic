package com.uciekinierzy.hydroapi.svcmeasures.TOs;

import com.uciekinierzy.hydroapi.svcmeasures.ModuleMeasure;
import com.uciekinierzy.hydroapi.svcmeasures.WaterLevel;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ModuleMeasureExternalTO {
    private LocalDateTime reportedTime;
    private WaterLevel waterLevel;

    public static ModuleMeasureExternalTO newOf(ModuleMeasure moduleMeasure){
        return builder()
                .reportedTime(moduleMeasure.getReportedTime())
                .waterLevel(moduleMeasure.getWaterLevel())
                .build();
    }
}
