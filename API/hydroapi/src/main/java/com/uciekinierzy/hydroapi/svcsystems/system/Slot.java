package com.uciekinierzy.hydroapi.svcsystems.system;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SlotTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
@Data
@EqualsAndHashCode(exclude = {"module", "plantId", "plantPlantedTime"})
@ToString(exclude = {"module"})
class Slot
{
    @Id
    private String id;

    @Column(nullable = false)
    private Long x;
    @Column(nullable = false)
    private Long y;

    @ManyToOne
    @JsonIgnore
    private Module module;

    private String plantId;
    private LocalDateTime plantPlantedTime;

    SlotTO toTO()
    {
        return SlotTO.builder()
                .id(id)
                .x(x)
                .y(y)
                .plantId(plantId)
                .plantPlantedTime(plantPlantedTime)
                .moduleId(module.getId())
                .build();
    }
}
