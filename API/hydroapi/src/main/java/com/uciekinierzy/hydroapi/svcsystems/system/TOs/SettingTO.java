package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SettingTO {
    String moduleId;
    Boolean halfLight;

    Integer fromHourUTC;
    Integer fromMinuteUTC;
    Integer toHourUTC;
    Integer toMinuteUTC;
}
