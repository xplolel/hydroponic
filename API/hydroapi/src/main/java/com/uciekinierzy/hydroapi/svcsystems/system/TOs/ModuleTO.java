package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModuleTO
{
    private String id;
    private Integer moduleNumber;
    private String moduleName;
    private String systemId;
    private List<SlotTO> slots;
}
