package com.uciekinierzy.hydroapi.svcplants.plants.TOs;

import com.uciekinierzy.hydroapi.svcplants.plants.GrowingDifficultyEnum;
import com.uciekinierzy.hydroapi.svcplants.plants.PlantTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlantTO
{
    private String id;
    private String name;
    private Integer growTime;
    private GrowingDifficultyEnum growDifficulty;
    private PlantTypeEnum plantType;
    private LocalDateTime createdTime;
    private String createdBy;
    private String fullImageUrl;
    private String mediumImageUrl;
    private String smallImageUrl;
}
