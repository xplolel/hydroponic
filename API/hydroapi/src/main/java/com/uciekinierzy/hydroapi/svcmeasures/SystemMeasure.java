package com.uciekinierzy.hydroapi.svcmeasures;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "measure")
public class SystemMeasure {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @Column(nullable = false)
    private String systemId;

    @Column(nullable = false)
    private LocalDateTime reportedTime;

    @Column(nullable = false)
    //Celcius
    private Float temperature;

    @Column(nullable = false)
    //%
    private Float humidity;
}
