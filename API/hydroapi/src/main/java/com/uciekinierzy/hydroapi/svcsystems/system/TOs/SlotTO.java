package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import lombok.*;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class SlotTO
{
    private String id;
    private String plantId;
    private LocalDateTime plantPlantedTime;
    private String moduleId;
    private Long x;
    private Long y;
}

