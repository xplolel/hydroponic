package com.uciekinierzy.hydroapi.svcmailing;

import com.sendgrid.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static java.lang.String.format;

@Service
@Slf4j
public class MailingService {
    private final SendGrid sendGrid;

    @Value("${app.system.email}")
    private String systemEmail;

    @Value("${app.admin.email}")
    private String adminEmail;

    public MailingService() {
        String sendgrid_api_key = System.getenv("sgapikey");
        log.info("sgapikey = " + sendgrid_api_key);
        sendGrid = new SendGrid(sendgrid_api_key);
    }

    /**
     * @param to mail
     * @param subject text
     * @param content text/plain
     * @return mailTO
     * @throws IncorrectMailFormat whenever subject is empty, to is not correct email or from is not correct email.
     */
    public MailTO createSystemMail(String to, String subject, String content) throws IncorrectMailFormat{
        return MailTO.builder()
                .from(systemEmail)
                .to(to)
                .subject(subject)
                .content(content)
                .build();
    }

    public void sendMail(MailTO mailTO) throws IncorrectMailFormat, MailingException {
        Mail mail;

        if(mailTO.getFrom() == null)
            mailTO.setFrom(systemEmail);

        try{
            mail = new Mail(
                    new Email(mailTO.from),
                    mailTO.subject,
                    new Email(mailTO.to),
                    new Content("text/plain", mailTO.content)
            );
        }
        catch (Exception e){
            throw new IncorrectMailFormat(e);
        }

        try {
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            Response response = sendGrid.api(request);

            if(response.getStatusCode() != 202)
                throw new MailingException(format("Status code = %d, Body = %s", response.getStatusCode(), response.getBody()));
            else
                log.info(format("Mail successfully sent from %s to %s. Subject = %s", mailTO.from, mailTO.to, mailTO.subject));
        }
        catch (IOException e){
            throw new MailingException(e);
        }
    }

    public void mailAdminOfAnIssue(String content){
        try{
            sendMail(MailTO.builder()
                    .from(systemEmail)
                    .to(adminEmail)
                    .subject("HydroApi issue")
                    .content(content)
                    .build());
        }
        catch (Throwable e){
            log.error("Wanted to send email to admin about " + content, e);
        }
    }
}
