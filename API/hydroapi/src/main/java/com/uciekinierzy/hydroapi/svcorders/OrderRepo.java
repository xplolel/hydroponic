package com.uciekinierzy.hydroapi.svcorders;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
@RestResource(exported = false)
public interface OrderRepo extends CrudRepository<Order, String> {
    Collection<Order> getAllByModuleId(String moduleId);
}
