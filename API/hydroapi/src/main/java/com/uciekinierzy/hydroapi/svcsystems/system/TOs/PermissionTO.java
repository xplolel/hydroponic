package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import com.uciekinierzy.hydroapi.svcsystems.permission.PermissionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionTO {
    private String id;
    private String systemId;
    private String holderEmail;
    private PermissionType permissionType;
}
