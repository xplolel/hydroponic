package com.uciekinierzy.hydroapi.svcmeasures;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@RestResource(exported = false)
@Repository
public interface SystemMeasureRepo extends CrudRepository<SystemMeasure, Long> {
    List<SystemMeasure> findAllBySystemId(String systemId);

    @Query(value = "SELECT distinct systemId FROM SystemMeasure")
    List<String> findDistinctSystemIds();
}
