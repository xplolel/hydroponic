package com.uciekinierzy.hydroapi.svcnotifications;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.utils.AuthUtils;
import com.uciekinierzy.hydroapi.utils.ControllerUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;
import static com.uciekinierzy.hydroapi.utils.ControllerUtils.getToken;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.*;

@RestController(NOTIFCATIONS_CONTROLLER)
@RequiredArgsConstructor
public class NotificationController {
    private final NotificationService notificationService;
    private final HttpServletRequest request;

    @GetMapping(NOTIFCATIONS_PATH)
    public ResponseEntity getAllNotifications(@RequestParam(required = false) Integer limit, @RequestParam(required = false) Integer offset, @RequestParam(required = false) String unreadOnly, @RequestParam(required = false) String readOnly) throws IncorrectTOException {
        if("true".equals(unreadOnly) && "true".equals(readOnly))
            return badRequest().body("You can't have unread only and read only at once... go rethink your life");
        return ok(notificationService.getNotificationsForClient(getToken(request), limit, offset, "true".equalsIgnoreCase(readOnly), "true".equalsIgnoreCase(unreadOnly)));
    }

    @PostMapping(NOTIFCATIONS_PATH)
    public ResponseEntity postNotification(@RequestBody NotificationAddingTO notificationAddingTO) throws NoSuchResourceException, NoAccessException, IncorrectTOException {
        AuthUtils.checkSystemAccessAndThrowException(notificationAddingTO.getSystemId(), request);
        return status(CREATED).body(notificationService.postNotification(notificationAddingTO, getToken(request)));
    }

    @GetMapping(NOTIFCATIONS_ID_PATH)
    public ResponseEntity getNotification(@PathVariable String id) throws NoSuchResourceException, NoAccessException {
        return ok(notificationService.get(id, getToken(request)));
    }

    @PatchMapping(NOTIFCATIONS_ID_PATH)
    public ResponseEntity readNotification(@PathVariable String id) throws NoSuchResourceException, NoAccessException {
        notificationService.read(id, getToken(request));
        return ok("");
    }

    @PostMapping(NOTIFCATIONS_SUBSCRIBE)
    public ResponseEntity subscribe(@RequestBody String json){
        notificationService.subscribe(ControllerUtils.getClientEmail(request), ControllerUtils.getClientId(request), json);
        return ok("");
    }
}
