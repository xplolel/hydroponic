package com.uciekinierzy.hydroapi.svcclients.controllers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenWrapper
{
    private String token;
    private String userId;
}
