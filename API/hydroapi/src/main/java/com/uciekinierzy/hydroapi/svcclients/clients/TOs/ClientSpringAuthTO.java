package com.uciekinierzy.hydroapi.svcclients.clients.TOs;

import com.google.common.collect.Lists;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Value
@EqualsAndHashCode
@ToString
public class ClientSpringAuthTO implements UserDetails {
    public static final String ROLE_DELIMITER = ";";
    private final String username;
    private final String password;
    private final String roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if(roles == null || roles.isEmpty())
            return Lists.newArrayList(new SimpleGrantedAuthority("USER"));
        else
            return Arrays.stream(roles.split(ROLE_DELIMITER))
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
