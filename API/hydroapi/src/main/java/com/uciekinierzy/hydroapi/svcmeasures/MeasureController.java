package com.uciekinierzy.hydroapi.svcmeasures;

import com.uciekinierzy.hydroapi.exceptions.NoAccessException;
import com.uciekinierzy.hydroapi.utils.AuthUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;

@RestController(MEASURES_PATH)
@RequiredArgsConstructor
public class MeasureController {
    private final MeasureService measureService;
    private final HttpServletRequest request;

    @GetMapping(SYSTEM_MEASURES_ID_PATH)
    public ResponseEntity getSystemMeasures(@PathVariable String systemId, @RequestParam(required = false) String onlyLast) throws NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemId, request);
        List<SystemMeasure> systemMeasures = measureService.getSystemMeasures(systemId);

        if("true".equals(onlyLast)) {
            if(systemMeasures.isEmpty())
                return ResponseEntity.notFound().build();
            else
                return ResponseEntity.ok(systemMeasures.get(systemMeasures.size() - 1));
        }
        else
            return ResponseEntity.ok(systemMeasures);
    }

    @GetMapping(MODULE_MEASURES_ID_PATH)
    public ResponseEntity getModuleMeasures(@PathVariable String moduleId, @RequestParam(required = false) String onlyLast) throws NoAccessException {
        AuthUtils.checkModuleAccessAndThrowException(moduleId, request);
        List<ModuleMeasure> moduleMeasures = measureService.getModuleMeasures(moduleId);

        if("true".equals(onlyLast)) {
            if(moduleMeasures.isEmpty())
                return ResponseEntity.notFound().build();
            else
                return ResponseEntity.ok(moduleMeasures.get(moduleMeasures.size() - 1));
        }
        else
            return ResponseEntity.ok(moduleMeasures);
    }

    @PostMapping(SYSTEM_MEASURES_PATH)
    public ResponseEntity postMeasure(@RequestBody SystemMeasure systemMeasure) throws NoAccessException {
        AuthUtils.checkSystemAccessAndThrowException(systemMeasure.getSystemId(), request);

        return ResponseEntity.ok(measureService.add(systemMeasure));
    }

    @PostMapping(MODULE_MEASURES_PATH)
    public ResponseEntity postMeasure(@RequestBody ModuleMeasure measure) throws NoAccessException {
        AuthUtils.checkModuleAccessAndThrowException(measure.getModuleId(), request);

        return ResponseEntity.ok(measureService.addModuleMeasure(measure));
    }
}
