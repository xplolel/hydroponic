package com.uciekinierzy.hydroapi.svcsystems.offlinesystem;

import com.uciekinierzy.hydroapi.exceptions.ResourceOfSuchIdExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import static com.uciekinierzy.hydroapi.constants.Endpoints.OFFLINE_SYSTEMS_CONTROLLER;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController(OFFLINE_SYSTEMS_CONTROLLER)
@RequiredArgsConstructor
@ApiIgnore
public class OfflineSystemController {
    private final OfflineSystemService service;

    @Value("app.testing")
    private String testing;

    @PostMapping(OFFLINE_SYSTEMS_CONTROLLER)
    public ResponseEntity addOfflineSystem(@RequestBody String id) throws ResourceOfSuchIdExistsException {
        if(!"true".equalsIgnoreCase(testing))
            return status(NOT_FOUND).build();

        service.add(id);
        return ok().build();
    }
}
