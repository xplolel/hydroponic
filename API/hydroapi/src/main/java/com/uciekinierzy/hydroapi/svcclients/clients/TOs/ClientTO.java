package com.uciekinierzy.hydroapi.svcclients.clients.TOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Data
public class ClientTO
{
    private String id;
    private String email;
    private ClientDetailsTO clientDetailsTO;
}
