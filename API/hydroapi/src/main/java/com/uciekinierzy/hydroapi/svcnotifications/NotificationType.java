package com.uciekinierzy.hydroapi.svcnotifications;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum NotificationType {
    INFO("INFO"),
    TAKE_ACTION("TAKE_ACTION"),
    FAILURE("FAILURE"),
    ;

    private final String value;

    @Override
    public String toString()
    {
        return value;
    }

    @JsonCreator
    public static NotificationType fromText(String text)
    {
        return NotificationType.valueOf(text);
    }
}
