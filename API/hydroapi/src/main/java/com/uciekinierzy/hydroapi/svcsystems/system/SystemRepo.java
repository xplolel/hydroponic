package com.uciekinierzy.hydroapi.svcsystems.system;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;
import java.util.Optional;

@RestResource(exported = false)
interface SystemRepo extends CrudRepository<System, String>
{
    List<System> findAllByOwnerId(String ownerId);

    List<System> findAllByIdIn(List<String> ids);

    Optional<System> findByToken(String token);

    Optional<System> findByIdAndToken(String id, String token);
}
