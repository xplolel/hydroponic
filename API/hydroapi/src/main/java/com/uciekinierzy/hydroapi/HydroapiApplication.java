package com.uciekinierzy.hydroapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HydroapiApplication {
	public static void main(String[] args) {
		SpringApplication.run(HydroapiApplication.class, args);
	}
}
