package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class SystemTO
{
    private String id;
    private String ownerId;
    private String name;
    private List<ModuleTO> modules;
}
