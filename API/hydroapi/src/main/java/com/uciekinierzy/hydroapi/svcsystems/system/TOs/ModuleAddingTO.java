package com.uciekinierzy.hydroapi.svcsystems.system.TOs;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ModuleAddingTO
{
    private String systemId;
    private String name;
}
