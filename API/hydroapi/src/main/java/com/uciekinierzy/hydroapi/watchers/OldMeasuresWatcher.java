package com.uciekinierzy.hydroapi.watchers;

import com.uciekinierzy.hydroapi.svcmeasures.MeasureService;
import org.springframework.stereotype.Component;

@Component
public class OldMeasuresWatcher implements Watcher {
    private final MeasureService measureService;

    public OldMeasuresWatcher(MeasureService measureService) {
        this.measureService = measureService;
    }

    @Override
    public int getIntervalInMinutes() {
        return 24*60;
    }

    @Override
    public Runnable call() {
        return measureService::deleteOld;
    }
}
