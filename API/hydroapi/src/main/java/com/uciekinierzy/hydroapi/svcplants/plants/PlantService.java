package com.uciekinierzy.hydroapi.svcplants.plants;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.svcplants.plants.TOs.PlantTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import static com.uciekinierzy.hydroapi.constants.Endpoints.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlantService
{
    private final PlantsRepo plantsRepo;
    private final PlantImageRepo plantImageRepo;

    private ForkJoinPool forkJoinPool = new ForkJoinPool(4);

    private List<PlantTO> cachedAllPlants = null;
    private Map<String, PlantTO> cachedPlantsMap = Maps.newHashMap();

    /**
     * @return id of the created plant object.
     * @throws PlantTOInvalidForAddingException when growtime, name or grow difficulty is empty or nulled.
     */
    @Transactional
    public PlantTO addPlant(PlantTO plantTO) throws PlantTOInvalidForAddingException
    {
        checkIfPlantTOcompleteForAddingPlant(plantTO);

        final Plant plant = new Plant();

        plant.setId(UUID.randomUUID().toString());
        plant.setName(plantTO.getName());
        plant.setGrowingDifficulty(plantTO.getGrowDifficulty());
        plant.setPlantType(plantTO.getPlantType());
        plant.setGrowTime(plantTO.getGrowTime());
        plant.setTimeCreated(plantTO.getCreatedTime());
        plant.setCreatorId(plantTO.getCreatedBy());

        plantsRepo.save(plant);
        invalidate(plantTO.getId());

        return plant.toTO();
    }

    @Transactional
    public void addImage(String plantId, MultipartFile multipartFile) throws IOException, IncorrectTOException {
        checkIfFileIsValid(multipartFile);

        byte[] original = multipartFile.getBytes();
        byte[] resizedTo140 = ResizerUtils.resizeTo(original, 140);
        byte[] resizedTo400 = ResizerUtils.resizeTo(original, 400);

        final PlantImage plantImage = new PlantImage(plantId, original, resizedTo140, resizedTo400);

        plantImageRepo.save(plantImage);

        invalidate(plantId);
    }

    public byte[] getImageFull(String plantId) throws NoImageForGivenPlantException
    {
        return plantImageRepo.findById(plantId)
                .map(PlantImage::getImageFull)
                .orElseThrow(() -> new NoImageForGivenPlantException(plantId.toString()));
    }

    public byte[] getImageMedium(String plantId) throws NoImageForGivenPlantException
    {
        return plantImageRepo.findById(plantId)
                .map(PlantImage::getImage400)
                .orElseThrow(() -> new NoImageForGivenPlantException(plantId.toString()));
    }

    public byte[] getImageSmall(String plantId) throws NoImageForGivenPlantException
    {
        return plantImageRepo.findById(plantId)
                .map(PlantImage::getImage140)
                .orElseThrow(() -> new NoImageForGivenPlantException(plantId.toString()));
    }

    private void invalidate(String plantId) {
        cachedAllPlants = null;
        cachedPlantsMap.remove(plantId);
    }

    private void checkIfFileIsValid(MultipartFile multipartFile) throws IncorrectTOException {
        if(multipartFile.getName().isEmpty())
            throw new IncorrectTOException();
    }

    private void checkIfPlantTOcompleteForAddingPlant(PlantTO plantTO) throws PlantTOInvalidForAddingException
    {
        String msg = "";

        if(plantTO.getName() == null || plantTO.getName().isEmpty())
        {
            msg += "Name is empty or nulled";
        }

        if(plantTO.getGrowTime() == null)
        {
            msg += "GrowTime is nulled";
        }

        if(plantTO.getGrowDifficulty() == null)
        {
            msg += "Grow difficulty is nulled";
        }

        if(!msg.isEmpty())
        {
            log.error(msg, plantTO);
            throw new PlantTOInvalidForAddingException(msg);
        }
    }

    public List<PlantTO> getAll()
    {
        if(cachedAllPlants == null)
            cachedAllPlants = getAllPlantsFromDb();

        return cachedAllPlants;
    }

    private void applyPlantImage(PlantTO pto) {
        plantImageRepo.findById(pto.getId())
                .ifPresent(plantImage ->
                {
                    log.info("Found img - debug time info");
                    pto.setFullImageUrl(PLANTS_ID_IMAGE_FULL_PATH.replace("{id}", pto.getId()));
                    pto.setMediumImageUrl(PLANTS_ID_IMAGE_MEDIUM_PATH.replace("{id}", pto.getId()));
                    pto.setSmallImageUrl(PLANTS_ID_IMAGE_SMALL_PATH.replace("{id}", pto.getId()));
                }
        );
    }

    private List<PlantTO> getAllPlantsFromDb(){
        List<Plant> plants = Lists.newArrayList(plantsRepo.findAll());
        List<PlantTO> plantTOS = plants.stream().map(Plant::toTO).collect(Collectors.toList());

        plantTOS.forEach(pto -> forkJoinPool.submit(() -> applyPlantImage(pto)));

        return plantTOS;
    }

    public PlantTO getById(String id) throws NoSuchPlantException
    {
        PlantTO pto;

        if(cachedPlantsMap.containsKey(id)){
            pto = cachedPlantsMap.get(id);
        }
        else {
            pto = plantsRepo.findById(id)
                    .map(Plant::toTO)
                    .orElseThrow(NoSuchPlantException::new);

            if(plantImageRepo.findById(pto.getId()).isPresent())
            {
                pto.setFullImageUrl(PLANTS_ID_IMAGE_FULL_PATH.replace("{id}", pto.getId()));
                pto.setMediumImageUrl(PLANTS_ID_IMAGE_MEDIUM_PATH.replace("{id}", pto.getId()));
                pto.setSmallImageUrl(PLANTS_ID_IMAGE_SMALL_PATH.replace("{id}", pto.getId()));
            }
        }

        return pto;
    }
}
