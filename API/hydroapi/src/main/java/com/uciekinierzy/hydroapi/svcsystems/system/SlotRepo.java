package com.uciekinierzy.hydroapi.svcsystems.system;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Collection;

@RestResource(exported = false)
interface SlotRepo extends CrudRepository<Slot, String> {
    Collection<Slot> findAllByModule(Module module);
}
