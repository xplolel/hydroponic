package com.uciekinierzy.hydroapi.svcplants.plants;

import com.uciekinierzy.hydroapi.svcplants.plants.TOs.PlantTO;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
class Plant
{
    @Id
    private String id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer growTime;

    @Column(nullable = false)
    private GrowingDifficultyEnum growingDifficulty;

    @Column(nullable = false)
    private PlantTypeEnum plantType;

    @Column(nullable = false)
    private LocalDateTime timeCreated;

    @Column(nullable = false)
    private String creatorId;

    PlantTO toTO()
    {
        return PlantTO.builder()
                .id(id)
                .name(name)
                .growTime(growTime)
                .growDifficulty(growingDifficulty)
                .plantType(plantType)
                .createdTime(timeCreated)
                .createdBy(creatorId)
                .build();
    }
}
