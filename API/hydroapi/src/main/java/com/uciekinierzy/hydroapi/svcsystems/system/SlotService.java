package com.uciekinierzy.hydroapi.svcsystems.system;

import com.google.common.collect.Sets;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.helpers.SlotOccupiedException;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SlotTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SlotService
{
    public static final long SLOTS_ROWS = 2;
    public static final long SLOTS_IN_ROW = 4;

    private final SlotRepo slotRepo;
    private final ModuleRepo moduleRepo;

    @Transactional
    public SlotTO setPlant(String id, String plantId) throws SlotOccupiedException, NoSuchResourceException {
        Slot slot = slotRepo.findById(id).orElseThrow(NoSuchResourceException::new);

        if(slot.getPlantId() != null) {
            throw new SlotOccupiedException();
        }

        slot.setPlantId(plantId);
        slot.setPlantPlantedTime(LocalDateTime.now());

        return slot.toTO();
    }

    public Set<SlotTO> getSlotsForModule(String moduleId) throws NoSuchResourceException {
        Module module = moduleRepo
                .findById(moduleId)
                .orElseThrow(() -> new NoSuchResourceException("Module of given id does not exists"));

        return slotRepo.findAllByModule(module)
                .stream()
                .map(Slot::toTO)
                .collect(Collectors.toSet());
    }

    @Transactional
    public Set<Slot> createNSlotsForModule(Long n, Module module)
    {
        Set<Slot> set = Sets.newHashSet();

        for (int i = 0; i < SLOTS_ROWS; i++)
        {
            for (int j = 0; j < SLOTS_IN_ROW; j++)
            {
                Slot slot = new Slot();

                slot.setId(UUID.randomUUID().toString());
                slot.setModule(module);
                slot.setPlantId(null);
                slot.setX((long) i);
                slot.setY((long) j);

                slotRepo.save(slot);

                set.add(slot);
            }
        }

        return set;
    }

    @Transactional
    void removeSlots(Module module){
        slotRepo.deleteAll(slotRepo.findAllByModule(module));
    }

    public SlotTO getSlot(String id) throws NoSuchResourceException {
        return slotRepo.findById(id)
                .map(Slot::toTO)
                .orElseThrow(() -> new NoSuchResourceException("No slot found of id " + id));
    }

    @Transactional
    public SlotTO clearPlant(String id) throws NoSuchResourceException {
        Slot slot = slotRepo.findById(id)
                .orElseThrow(() -> new NoSuchResourceException("No slot found of id " + id));

        slot.setPlantId(null);
        slot.setPlantPlantedTime(null);

        return slot.toTO();
    }
}
