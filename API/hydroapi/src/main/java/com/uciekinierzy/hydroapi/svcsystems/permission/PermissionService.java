package com.uciekinierzy.hydroapi.svcsystems.permission;

import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.helpers.CannotDeleteOwnerPermissionException;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.PermissionTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PermissionService {
    private final PermissionRepo permissionRepo;

    @Transactional
    public PermissionTO createPermission(PermissionTO permissionTO) {
        if(StringUtils.isEmpty(permissionTO.getId()))
            permissionTO.setId(UUID.randomUUID().toString());

        Permission permission = new Permission();
        permission.setHolderEmail   (permissionTO.getHolderEmail());
        permission.setSystemId      (permissionTO.getSystemId());
        permission.setPermissionType(permissionTO.getPermissionType());
        permission.setId            (permissionTO.getId());

        permissionRepo.save(permission);

        return permission.toTO();
    }

    public void deletePermissionById(String permissionId) throws NoSuchResourceException, CannotDeleteOwnerPermissionException {
        Permission permission = permissionRepo.findById(permissionId)
                .orElseThrow(NoSuchResourceException::new);

        if(permission.getPermissionType().equals(PermissionType.OWNER))
            throw new CannotDeleteOwnerPermissionException();

        permissionRepo.delete(permission);
    }

    public List<PermissionTO> findPermissionBySystemId(String systemId){
        return permissionRepo.findBySystemId(systemId)
                .stream()
                .map(Permission::toTO)
                .collect(Collectors.toList());
    }

    public Optional<PermissionTO> findPermissionByHolderEmailAndSystemId(String email, String systemId){
        return permissionRepo.findByHolderEmailAndSystemId(email, systemId)
                .map(Permission::toTO);
    }

    public List<PermissionTO> findPermissionByHolderEmail(String email){
        return permissionRepo.findAllByHolderEmail(email)
                .stream()
                .map(Permission::toTO)
                .collect(Collectors.toList());
    }
}
