package com.uciekinierzy.hydroapi.svcsystems.helpers;

public class CannotDeleteOwnerPermissionException extends Exception {
    public CannotDeleteOwnerPermissionException() {
        super();
    }

    public CannotDeleteOwnerPermissionException(String message) {
        super(message);
    }

    public CannotDeleteOwnerPermissionException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotDeleteOwnerPermissionException(Throwable cause) {
        super(cause);
    }

    protected CannotDeleteOwnerPermissionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
