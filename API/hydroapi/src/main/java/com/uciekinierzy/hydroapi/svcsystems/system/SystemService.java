package com.uciekinierzy.hydroapi.svcsystems.system;

import com.google.common.collect.Lists;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.permission.PermissionService;
import com.uciekinierzy.hydroapi.svcsystems.permission.PermissionType;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.uciekinierzy.hydroapi.utils.JwtUtils.getEmailFromTokenWithoutAuth;


@Service
@RequiredArgsConstructor
@Slf4j
public class SystemService
{
    private final SystemRepo systemRepo;
    private final PermissionService permissionService;

    SystemTO getSystemById(String id) throws NoSuchResourceException {
        return systemRepo.findById(id)
                .map(System::toTO)
                .orElseThrow(NoSuchResourceException::new);
    }

    SystemRegistrationResponseTO addMockSystem(SystemAddingTO systemAddingTO){
        System system = new System();

        system.setId(UUID.randomUUID().toString());
        system.setName(systemAddingTO.getName());
        system.setOwnerId(systemAddingTO.getOwnerId());
        system.setToken(DigestUtils.md5DigestAsHex(UUID.randomUUID().toString().getBytes()));
        system.setMock(true);
        systemRepo.save(system);

        permissionService.createPermission(PermissionTO.builder()
                .holderEmail(systemAddingTO.getOwnerEmail())
                .systemId(system.getId())
                .permissionType(PermissionType.OWNER)
                .build());

        return SystemRegistrationResponseTO.builder()
                .system(system.toTO())
                .token(system.getToken())
                .build();
    }

    @Transactional
    SystemRegistrationResponseTO registerSystem(SystemRegistrationTO systemRegistrationTO) {
        System system = new System();

        system.setId(systemRegistrationTO.getSystemUniqueId());
        system.setName("My new system");
        system.setOwnerId(systemRegistrationTO.getUserId());
        system.setToken(DigestUtils.md5DigestAsHex(UUID.randomUUID().toString().getBytes()));
        system.setMock(false);
        systemRepo.save(system);

        permissionService.createPermission(PermissionTO.builder()
                .holderEmail(systemRegistrationTO.getUserEmail())
                .systemId(system.getId())
                .permissionType(PermissionType.OWNER)
                .build());

        return SystemRegistrationResponseTO.builder()
                .system(system.toTO())
                .token(system.getToken())
                .build();
    }

    public boolean isSystemRegisteredAndShouldItReportMeasures(String systemId){
        return systemRepo.findById(systemId)
                .filter(value -> value.getMock() != null && !value.getMock())
                .isPresent();
    }

    @Transactional
    public SystemTO patchSystem(String systemId, SystemPatchingTO systemPatchingTO) throws NoSuchResourceException{
        final System system = systemRepo.findById(systemId).orElseThrow(() -> new NoSuchResourceException("No system of id = " + systemId));

        if(!StringUtils.isEmpty(systemPatchingTO.getName()))
            system.setName(systemPatchingTO.getName());

        systemRepo.save(system);
        return system.toTO();
    }

    public List<SystemTO> getSystemsClientHasAccessTo(String token) {
        if(!token.contains(".")) {
            System system = systemRepo.findByToken(token)
                    .orElseThrow(() -> new RuntimeException("No system found for token"));
            return Lists.newArrayList(system.toTO());
        }
        else {
            String email = getEmailFromTokenWithoutAuth(token);

            List<String> systemsIds = permissionService.findPermissionByHolderEmail(email)
                    .stream()
                    .map(PermissionTO::getSystemId)
                    .collect(Collectors.toList());

            return systemRepo.findAllByIdIn(systemsIds)
                    .stream()
                    .map(System::toTO)
                    .collect(Collectors.toList());
        }
    }

    public Set<String> getEmailsWithAccessToSystem(String systemId){
        return permissionService.findPermissionBySystemId(systemId)
                .stream()
                .map(PermissionTO::getHolderEmail)
                .collect(Collectors.toSet());
    }

    //TODO clean this
    public UserDetails getSystemUserDetailsByToken(String token) throws NoSuchResourceException {
        System system = systemRepo.findByToken(token)
                .orElseThrow(() -> new NoSuchResourceException("There's no system with given token"));

        return new UserDetails(){
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return Lists.newArrayList(new SimpleGrantedAuthority("USER"));
            }

            @Override
            public String getPassword() {
                return "hidden";
            }

            @Override
            public String getUsername() {
                return "System" + system.getId();
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        };
    }
}
