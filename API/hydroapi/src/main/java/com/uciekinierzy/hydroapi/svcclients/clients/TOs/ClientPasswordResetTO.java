package com.uciekinierzy.hydroapi.svcclients.clients.TOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClientPasswordResetTO {
    private String email;
    private String token;
    private String password;
}
