package com.uciekinierzy.hydroapi.svcorders;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrderAddingTO {
    private String moduleId;
    private String shortNote;
    private LocalDateTime created;
    private OrderType orderType;
}
