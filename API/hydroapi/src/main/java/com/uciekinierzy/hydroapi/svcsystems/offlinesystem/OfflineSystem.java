package com.uciekinierzy.hydroapi.svcsystems.offlinesystem;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
class OfflineSystem
{
    @Id
    private String id;
}
