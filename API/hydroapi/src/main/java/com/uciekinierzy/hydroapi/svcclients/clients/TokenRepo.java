package com.uciekinierzy.hydroapi.svcclients.clients;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RestResource(exported = false)
public interface TokenRepo extends CrudRepository<PasswordResetToken, String> {
    Optional<PasswordResetToken> findByClientId(String clientId);
}
