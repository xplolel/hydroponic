package com.uciekinierzy.hydroapi.configuration;

import com.uciekinierzy.hydroapi.constants.Endpoints;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class AuthPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        log.error(String.format("Request %s declined - unauthorized", request.toString()));
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Please authorize first on " + Endpoints.LOGIN_ENDPOINT_URL);
    }
}
