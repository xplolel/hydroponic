package com.uciekinierzy.hydroapi.svcnotifications;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
@Slf4j
class ScriptCaller {

    public static final String WEBPUSH_PY = "webpush.py";
    public static final String REPLACEMENT = "\"";
    public static final String TARGET = "\"";
    public static final String EMPTY = "";
    private static final String NEWLINE = "\n";

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Value("${app.pywebpush.path}")
    String scriptPath;

    @Value("${app.pywebpush.pkey}")
    String pkey;

    void testInit(String path, String pkey){
        scriptPath = path;
        this.pkey = pkey;
    }

    boolean call(String sub, NotificationAddingTO to){
        boolean result = true;

        try {
            String fileName = UUID.randomUUID().toString();

            final Path scriptPath = Paths.get(this.scriptPath);
            final File file = scriptPath.resolve(fileName).toFile();
            final File script = scriptPath.resolve(WEBPUSH_PY).toFile();


            try(FileOutputStream fos = new FileOutputStream(file)) {

                final String toString = getNotificationJson(to);
                objectMapper.writeValue(fos,
                        new TO(
                                sub.replace(TARGET, REPLACEMENT).replace(NEWLINE, EMPTY).trim(),
                                toString.replace(TARGET, REPLACEMENT).replace(NEWLINE, EMPTY).trim(),
                                pkey
                        )
                );
            }

            final Process exec = Runtime.getRuntime().exec(
                    String.format("python3 %s %s",
                            script.toString(),
                            file.toString()
                    )
            );

            while(exec.isAlive()){
                Thread.sleep(10);
            }

            log.info("PS Stdout:" + new String(exec.getInputStream().readAllBytes()));

            if(exec.exitValue() != 0){
                result = false;
                log.warn("PS exited with " + exec.exitValue());
                log.warn("PS Stderr:" + new String(exec.getErrorStream().readAllBytes()));
            }

            if(!file.delete())
                log.warn("Failed to delete file: " + file.toString());
        }
        catch (IOException | InterruptedException e) {
            log.error("While trying to write data to file:", e);
            result = false;
        }

        return result;
    }

    private String getNotificationJson(NotificationAddingTO to) throws JsonProcessingException {
        return objectMapper.writeValueAsString(
                            new NotificationTOWrapWrap(new NotificationTOWrap().applyAddingTO(to))
                    );
    }

    public static void main(String[] args) throws JsonProcessingException {
        final ScriptCaller scriptCaller = new ScriptCaller();
        System.out.println(scriptCaller.getNotificationJson(NotificationAddingTO.builder()
                .shortNote("dupa")
                .longNote("haha")
                .build()));
//        scriptCaller.testInit("/Users/adrian/hydroponic/API/hydroapi", "pkeyOLE");
//        scriptCaller.call("{\n" +
//                "    \"endpoint\": \"https://fcm.googleapis.com/fcm/send/fVIrWrwJ0YQ:APA91bHb9uFQN1eGn-U3ZZ7Mzh9R8PqeZgCCOfZBcxx6G3-7TdPlv1UNv--Aotcb3wOXt7KSFvqGUvjcBXGjcffQsiqCVr4Bdv3QCLagY2NLKeSU0tqQ8VodHo-d0Nuz84IVcEUm9wgo\",\n" +
//                "    \"expirationTime\": null,\n" +
//                "    \"keys\": {\n" +
//                "        \"p256dh\": \"BMy8-CNO9WYts_S2Xm_UCUgKXmZ02UZx9mDr5S4W5cswiiLI-WR4gKyKwpeX4O9kixcVpntwYjZkdvtiR-1wixI\",\n" +
//                "        \"auth\": \"wElBP1GyQ5zZDn2boGxQQA\"\n" +
//                "    }\n" +
//                "}", NotificationAddingTO.builder()
//                .shortNote("dupka")
//                .systemId("badanda")
//                .build());
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    private class NotificationTOWrapWrap{
        NotificationTOWrap notification;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    private class NotificationTOWrap{
        String title;
        String body;
        String icon = "https://voidchuck.com/favicon.ico";
        int[] vibrate ={100,50,100};

        NotificationTOWrap applyAddingTO(NotificationAddingTO to){
            title = to.getShortNote();
            body = to.getLongNote();
            return this;
        }
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    private class TO{
        String sub;
        String data;
        String pkey;
    }
}
