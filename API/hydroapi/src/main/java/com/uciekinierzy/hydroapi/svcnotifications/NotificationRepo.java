package com.uciekinierzy.hydroapi.svcnotifications;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
@RestResource(exported = false)
public interface NotificationRepo extends CrudRepository<Notification, String> {
    Collection<Notification> getAllBySystemId(String systemId);

    Collection<Notification> getAllBySystemIdAndShortNoteAndLongNoteAndRead(String systemId, String shortNote, String longNote, boolean read);
}
