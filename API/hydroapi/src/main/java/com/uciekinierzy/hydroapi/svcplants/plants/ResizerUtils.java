package com.uciekinierzy.hydroapi.svcplants.plants;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

abstract class ResizerUtils
{
    static byte[] resizeTo(byte[] original, int width) throws IOException
    {
        try (ByteArrayInputStream is = new ByteArrayInputStream(original);
             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream())
        {
            BufferedImage bufferedImage = ImageIO.read(is);

            double widthRatio = ((double)width) / ((double)bufferedImage.getWidth());
            int height = (int) (widthRatio * (double) bufferedImage.getHeight());

            BufferedImage resized = resize(bufferedImage, height, width);
            ImageIO.write(resized, "png", byteArrayOutputStream);

            return byteArrayOutputStream.toByteArray();
        }
    }

    private static BufferedImage resize(BufferedImage img, int height, int width)
    {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }
}
