package com.uciekinierzy.hydroapi.svcmailing;

import com.uciekinierzy.hydroapi.configuration.ResourceProvider;
import com.uciekinierzy.hydroapi.utils.ExceptionUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@RequiredArgsConstructor
public class MailingHelper {
    private static final String LOCALES = "locales";
    private static final String MANUAL_LINK_KEY = "[%MANUAL_LINK%]";
    private static final String PASSWORD_RESET_LINK_KEY = "[%PASSWORD_RESET_LINK%]";

    private final MailingService mailingService;

    @Value("${app.manual-link}")
    private String MANUAL_LINK_VALUE;

    /**
     * @param locale - user's locale preference.
     * @return message contents.
     */
    public String getWelcomeContent(String locale){
        if(StringUtils.isEmpty(locale))
            locale = "en";

        try{
            String msg = ResourceProvider.readResourceFileAsString(String.format("%s/welcome-%s.txt", LOCALES, locale));
            msg = msg.replace(MANUAL_LINK_KEY, MANUAL_LINK_VALUE);
            return msg;
        }
        catch (Exception e){
            final String stack = ExceptionUtils.getStackAsString(e);
            mailingService.mailAdminOfAnIssue(String.format("Could not read locale %s. Error = %s", locale, stack));
            return "Welcome";
        }
    }

    public String getPasswordResetContent(String locale, String passwordResetLink) throws MailingTemplateException {
        if(StringUtils.isEmpty(locale))
            locale = "en";

        try{
            String msg = ResourceProvider.readResourceFileAsString(String.format("%s/passwordReset-%s.txt", LOCALES, locale));
            msg = msg.replace(PASSWORD_RESET_LINK_KEY, passwordResetLink);
            return msg;
        }
        catch (Exception e){
            final String stack = ExceptionUtils.getStackAsString(e);
            mailingService.mailAdminOfAnIssue(String.format("Could not read locale %s. Error = %s", locale, stack));
            throw new MailingTemplateException(e);
        }
    }

//    public String getWelcomeContent(String locale){
//        if(StringUtils.isEmpty(locale))
//            locale = "en";
//
//        if(locale.equals("en"))
//            return WE_EN.replace(MANUAL_LINK_KEY, MANUAL_LINK_VALUE);
//        else if(locale.equals("pl"))
//            return WE_PL.replace(MANUAL_LINK_KEY, MANUAL_LINK_VALUE);
//        else{
//            mailingService.mailAdminOfAnIssue(String.format("Could not read locale %s", locale));
//            return "Welcome";
//        }
//    }
//
//    public String getPasswordResetContent(String locale, String passwordResetLink) throws MailingTemplateException {
//        if(StringUtils.isEmpty(locale))
//            locale = "en";
//
//        if(locale.equals("en"))
//            return PR_EN.replace(PASSWORD_RESET_LINK_KEY, passwordResetLink);
//        else if(locale.equals("pl"))
//            return PR_PL.replace(PASSWORD_RESET_LINK_KEY, passwordResetLink);
//        else{
//            mailingService.mailAdminOfAnIssue(String.format("Could not read locale %s", locale));
//            return "Welcome";
//        }
//    }

    private final static String PR_EN = "Hello,\n" +
            "\n" +
            "If you have requested password reset link, here it is:\n" +
            "[%PASSWORD_RESET_LINK%]\n" +
            "\n" +
            "If you have not, please notify us by replying to this email.\n" +
            "\n" +
            "All the best,\n" +
            "Hydroponic team";

    private final static String PR_PL = "Dzień dobry,\n" +
            "\n" +
            "Jeżeli chcieli Państwo zresetować hasło, oto link:\n" +
            "[%PASSWORD_RESET_LINK%]\n" +
            "\n" +
            "Jeżeli nie prosili Państwo o zresetowanie hasła, prosimy dać znać poprzez odpowiedź na tego maila.\n" +
            "\n" +
            "Z pozdrowieniami,\n" +
            "Zespół Hydroponic\n";

    private final static String WE_EN = "Welcome to the Hydroponic!\n" +
            "\n" +
            "It's great you've joined, we're very excited about that.\n" +
            "We encourage you to get familiar with our manual to find out how to use Hydroponic best!\n" +
            "[%MANUAL_LINK%]\n" +
            "\n" +
            "In case you had any questions - we are here for you.\n" +
            "\n" +
            "All the best,\n" +
            "Hydroponic Team\n";

    private final static String WE_PL = "Witaj drogi użytkowniku apliakcji Hydroponic!\n" +
            "\n" +
            "Bardzo cieszymy się, że dołączasz do grona użytkowników.\n" +
            "Zachęcamy Cię to zapoznania się z instrukcją obsługi, która podpowie jak najlepiej wykorzystać możliwości Hydroponic.\n" +
            "\n" +
            "[%MANUAL_LINK%]\n" +
            "\n" +
            "W razie pytań służymy pomocą.\n" +
            "\n" +
            "Z pozdrowieniami,\n" +
            "Zespół Hydroponic\n";
}
