package com.uciekinierzy.hydroapi.watchers;

import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;

@Service
public class WatcherService {
    static Map<String, LocalDateTime> systemsLastLooper = Maps.newHashMap();

    public void systemLooper(String systemId){
        systemsLastLooper.put(systemId, LocalDateTime.now());
    }

    public LocalDateTime getLastSystemCall(String systemId){
        return systemsLastLooper.get(systemId);
    }

    public Map<String, LocalDateTime> getSystemsLastLooper() {
        return systemsLastLooper;
    }
}
