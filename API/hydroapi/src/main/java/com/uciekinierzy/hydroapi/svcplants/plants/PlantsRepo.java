package com.uciekinierzy.hydroapi.svcplants.plants;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RestResource(exported = false)
interface PlantsRepo extends CrudRepository<Plant, String>
{
}
