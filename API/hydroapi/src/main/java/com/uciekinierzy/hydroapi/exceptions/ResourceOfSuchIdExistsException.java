package com.uciekinierzy.hydroapi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ResourceOfSuchIdExistsException extends Exception {
    public ResourceOfSuchIdExistsException() {
        super();
    }

    public ResourceOfSuchIdExistsException(String message) {
        super(message);
    }

    public ResourceOfSuchIdExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceOfSuchIdExistsException(Throwable cause) {
        super(cause);
    }

    protected ResourceOfSuchIdExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
