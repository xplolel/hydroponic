package com.uciekinierzy.hydroapi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IncorrectTOException extends Exception{
    public IncorrectTOException() {
        super();
    }

    public IncorrectTOException(String message) {
        super(message);
    }

    public IncorrectTOException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectTOException(Throwable cause) {
        super(cause);
    }

    protected IncorrectTOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
