package com.uciekinierzy.hydroapi.svcsystems.system;

import com.uciekinierzy.hydroapi.exceptions.IncorrectTOException;
import com.uciekinierzy.hydroapi.exceptions.NoSuchResourceException;
import com.uciekinierzy.hydroapi.svcsystems.settings.SettingService;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.ModuleAddingTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.ModulePatchingTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.ModuleTO;
import com.uciekinierzy.hydroapi.svcsystems.system.TOs.SettingTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.uciekinierzy.hydroapi.svcsystems.system.SlotService.SLOTS_IN_ROW;


@RequiredArgsConstructor
@Service
@Slf4j
public class ModuleService
{
    private final ModuleRepo moduleRepo;
    private final SystemRepo systemRepo;

    private final SlotService slotService;
    private final SettingService settingService;

    @Transactional
    public ModuleTO addModule(ModuleAddingTO moduleAddingTO) throws IncorrectTOException {
        if(moduleAddingTO.getSystemId() == null)
            throw new IncorrectTOException("Module id is nulled");

        if(moduleAddingTO.getName() == null)
            throw new IncorrectTOException("Module name is nulled");

        if(moduleAddingTO.getName().isEmpty())
            throw new IncorrectTOException("Module name is empty");

        Optional<System> system = systemRepo.findById(moduleAddingTO.getSystemId());

        if(system.isEmpty())
            throw new IncorrectTOException(String.format("System of id %s does not exists", moduleAddingTO.getSystemId()));

        Integer max = moduleRepo.findAllBySystem(system.get())
                .stream()
                .map(Module::getModuleNumber)
                .max(Integer::compareTo)
                .orElse(0);

        Module module = new Module();
        module.setId(UUID.randomUUID().toString());
        module.setModuleName(moduleAddingTO.getName());
        module.setModuleNumber(max + 1);
        module.setSystem(system.get());
        moduleRepo.save(module);

        module.setSlots(slotService.createNSlotsForModule(SLOTS_IN_ROW, module));
        moduleRepo.save(module);

        addDefaultModuleSetting(module);

        return module.toTO();
    }

    private void addDefaultModuleSetting(Module module) {
        try {
            settingService.createSetting(SettingTO.builder()
                    .fromHourUTC(5)
                    .toHourUTC(21)
                    .fromMinuteUTC(0)
                    .toMinuteUTC(0)
                    .halfLight(false)
                    .moduleId(module.getId())
                    .build());
        } catch (IncorrectTOException e) {
            log.error("Could not auto-create settings for module " + module.getId());
        }
    }

    @Transactional
    public void renameModule(String moduleId, String newName) throws IncorrectTOException
    {
        Module module = moduleRepo.findById(moduleId)
                .orElseThrow(() -> new IncorrectTOException("No module found of id" + moduleId));
        module.setModuleName(newName);
        moduleRepo.save(module);
    }

    @Transactional
    public void removeModule(String moduleId) throws NoSuchResourceException
    {
        Module module = moduleRepo.findById(moduleId).orElseThrow(() -> new NoSuchResourceException("No module found of id" + moduleId));

        module.setSlots(null);
        moduleRepo.save(module);

        slotService.removeSlots(module);

        moduleRepo.delete(module);

        settingService.deleteSettingIfExists(moduleId);
    }

    public ModuleTO getModuleById(String id) throws NoSuchResourceException {
        return moduleRepo.findById(id)
                .map(Module::toTO)
                .orElseThrow(() -> new NoSuchResourceException("There's no module of id=" + id));
    }

    public List<ModuleTO> getModulesBySystem(String systemId) throws NoSuchResourceException {
        System system = systemRepo.findById(systemId).orElseThrow(NoSuchResourceException::new);
        return moduleRepo.findAllBySystem(system)
                .stream()
                .map(Module::toTO)
                .collect(Collectors.toList());
    }

    @Transactional
    public ModuleTO patchModule(String moduleId, ModulePatchingTO modulePatchingTO) throws NoSuchResourceException {
        Module module = moduleRepo.findById(moduleId).orElseThrow(() -> new NoSuchResourceException("No module found of id" + moduleId));

        if(!StringUtils.isEmpty(modulePatchingTO.getName()))
            module.setModuleName(modulePatchingTO.getName());

        moduleRepo.save(module);
        return module.toTO();
    }
}
