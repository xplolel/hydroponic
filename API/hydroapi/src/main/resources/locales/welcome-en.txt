Welcome to the Hydroponic!

It's great you've joined, we're very excited about that.
We encourage you to get familiar with our manual to find out how to use Hydroponic best!
[%MANUAL_LINK%]

In case you had any questions - we are here for you.

All the best,
Hydroponic Team
