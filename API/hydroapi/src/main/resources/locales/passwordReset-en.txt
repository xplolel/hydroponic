Hello,

If you have requested password reset link, here it is:
[%PASSWORD_RESET_LINK%]

If you have not, please notify us by replying to this email.

All the best,
Hydroponic team
