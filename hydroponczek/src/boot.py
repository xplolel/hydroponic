import time

import RPi.GPIO as GPIO

led_pin = 26

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(led_pin ,GPIO.OUT)

for i in range(5):
    # print("LED on")
    GPIO.output(led_pin , GPIO.HIGH)
    time.sleep(0.5)
    # print("LED off")
    GPIO.output(led_pin , GPIO.LOW)
    time.sleep(0.5)


