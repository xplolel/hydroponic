import RPi.GPIO as GPIO  # import RPi.GPIO module

# GPIO PIN numbers used in modules
CONTROL_LED_PIN = 26
FUNCTIONAL_BUTTON_PIN = 16
TEMPERATURE_PIN = 19
WATER_LVL_MOD1_PIN = 13

LIGHT1_MOD1_PIN = 20
LIGHT2_MOD1_PIN = 21


def gpio_set_mode():
    GPIO.setmode(GPIO.BCM)
