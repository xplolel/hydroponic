import datetime
import json

import common.system_info as sys_info


def temp_humid_data_to_json(temp, humid):
    data = {
        'humidity': humid,
        'temperature': temp,
        'reportedTime': datetime.datetime.utcnow().replace(microsecond=0).isoformat(),
        'systemId': sys_info.getserial()
    }

    return json.dumps(data)
