import threading
from datetime import timedelta, datetime, time
from enum import Enum
from time import sleep

import RPi.GPIO as GPIO  # import RPi.GPIO module
from apscheduler.schedulers.blocking import BlockingScheduler
from src.common import pins_assignment


class LightStateEnum(Enum):
    NO_LIGHT = 0
    HALF_LIGHT = 1
    FULL_LIGHT = 2


class LightPreferenceEnum(Enum):
    HALF_LIGHT = 1
    FULL_LIGHT = 2


class LightModule(threading.Thread):

    def __init__(self):
        pins_assignment.gpio_set_mode()
        threading.Thread.__init__(self)
        self.sched = BlockingScheduler()  # scheduler for jobs

        self.start_light_hour = 6
        self.end_light_hour = 19
        self.light_preference = LightPreferenceEnum.FULL_LIGHT

        self.light1_pin = pins_assignment.LIGHT1_MOD1_PIN
        self.light2_pin = pins_assignment.LIGHT2_MOD1_PIN

        self.light_state = LightStateEnum.NO_LIGHT

        self.gpio_setup()
        print(datetime.now())

        self.next_on_time = datetime(datetime.now().year,
                                     datetime.now().month,
                                     datetime.now().day,
                                     self.start_light_hour, 0, 0, 0)
        self.next_off_time = datetime(datetime.now().year,
                                      datetime.now().month,
                                      datetime.now().day,
                                      self.end_light_hour, 0, 0, 0)

        if self.its_time_for_light_up():
            self.turn_lights_on()
        else:
            self.turn_lights_off()
        print(self.next_on_time)
        print(self.next_off_time)

        jobTurnOff = self.sched.add_job(self.turn_lights_off, 'interval', days=1,
                                        start_date=self.next_off_time)
        jobTurnOn = self.sched.add_job(self.turn_lights_on, 'interval', days=1,
                                       start_date=self.next_on_time)

    def gpio_setup(self):
        GPIO.setup(self.light1_pin, GPIO.OUT)
        GPIO.setup(self.light2_pin, GPIO.OUT)

    def its_time_for_light_up(self):
        if time(self.end_light_hour, 0, 0, 0) >= datetime.time(datetime.now()):
            self.next_on_time += timedelta(days=1)
        if time(self.end_light_hour, 0, 0, 0) <= datetime.time(datetime.now()):
            self.next_off_time += timedelta(days=1)

        if time(self.end_light_hour, 0, 0, 0) >= datetime.time(datetime.now()) >= time(self.start_light_hour, 0, 0, 0):
            return True
        else:
            return False

    def turn_lights_on(self):
        if (self.light_preference == LightPreferenceEnum.FULL_LIGHT):
            self.turn_full_lights_on()
        else:
            self.turn_half_lights_on()

    def turn_full_lights_on(self):
        print("Turning lights on")
        GPIO.output(self.light1_pin, GPIO.HIGH)
        GPIO.output(self.light2_pin, GPIO.HIGH)
        self.light_state = LightStateEnum.NO_LIGHT

    def turn_half_lights_on(self):
        print("Turning lights on")
        GPIO.output(self.light2_pin, GPIO.HIGH)
        GPIO.output(self.light1_pin, GPIO.LOW)
        self.light_state = LightStateEnum.HALF_LIGHT

    def turn_lights_off(self):
        print("Turning lights off")
        GPIO.output(self.light1_pin, GPIO.LOW)
        GPIO.output(self.light2_pin, GPIO.LOW)
        self.light_state = LightStateEnum.FULL_LIGHT

    def run(self):
        self.sched.start()


def main():
    thread_for_light = LightModule()
    # thread_for_light.turn_lights_off()

    thread_for_light.start()
    while True:
        pass
        sleep(5)


if __name__ == '__main__':
    main()
