import threading
from datetime import datetime

import Adafruit_DHT
from apscheduler.schedulers.blocking import BlockingScheduler
from src.common import pins_assignment

from web_requests import post_get_functions


class TemperatureModule(threading.Thread):
    def __init__(self):
        pins_assignment.gpio_set_mode()
        threading.Thread.__init__(self)
        self.schedule = BlockingScheduler()  # scheduler for jobs

        self.sensor = Adafruit_DHT.DHT11
        self.temp_pin = pins_assignment.TEMPERATURE_PIN
        self.min_interval = 60
        self.error_state = False

        self.schedule.add_job(self.measureTemperature, 'interval', minutes=self.min_interval,
                                            next_run_time=datetime.now())

    def measureTemperature(self):
        temp_list = []
        humid_list = []

        prev_error_state = self.error_state

        for i in range(0, 18):
            humidity, temperature = Adafruit_DHT.read_retry(self.sensor, self.temp_pin, retries=5)
            # print(humidity,temperature)
            if humidity is None or temperature is None:
                self.error_state = True
                break

            # raise exception
            temp_list.append(temperature)
            humid_list.append(humidity)
        if len(temp_list) == 18:
            self.error_state = False

        if not self.error_state:
            avg_temp = int(sum(temp_list) - max(temp_list) - min(temp_list)) / 16
            avg_humid = int(sum(humid_list) - max(humid_list) - min(humid_list)) / 16

            print('AVG: Temp: {0} C  Humidity: {1} %'.format(avg_temp, avg_humid))
            # TODO
            # SEND TO SERVER

            post_get_functions.send_temp_and_humidity(avg_temp, avg_humid)

        elif prev_error_state == False and self.error_state == True:
            print('NO TEMPERATURE MEASURED!')
            pass
            # TODO
            # SEND ERR TO SERVER

    def run(self):
        self.schedule.start()


# def main():
#     thread_for_temp = TemperatureModule()
#     thread_for_temp.start()
#     while True:
#         pass
#         sleep(5)
#
#
# if __name__ == '__main__':
#     main()
