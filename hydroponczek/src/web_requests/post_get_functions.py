import requests

from common import json_formatter

# api-endpoint
URL = "http://ec2-52-200-48-63.compute-1.amazonaws.com/"
# headers
headers = {'content-type': 'application/json'}


def send_temp_and_humidity(temp, humid):
    json_data = json_formatter.temp_humid_data_to_json(temp, humid)

    endpoint = URL + 'measures'
    r = requests.post(endpoint, data=json_data, headers=headers)

# Test data
# send_temp_and_humidity(23.4, 70.8)
