from modules import temperature_module


def main():
    print("about to start")

    thread_of_temp = temperature_module.TemperatureModule()

    thread_of_temp.start()


if __name__ == '__main__':
    main()
