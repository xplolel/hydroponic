from distutils.core import setup

setup(
    name='Hydrofobowo',
    version='0.1dev',
    author = 'Karolina Konopka',
    packages=['src','src.common','src.modules']
)