# API
- All commands should be executed from inside of repo/API directory.
- Read all first - really do
- By IP we mean IPv4 
## What do you need
#### Step 1)
You will need docker. 

If you don't have it check it out https://docs.docker.com/get-started/ and follow the guide to install it.

#### Step 2)
Enjoy that you don't need anything else.

## How to run
#### Know your ip
Get your local ip address by executing "ip a" in terminal.

You can use either you main interface (Ethernet/WiFi) ip or docker interface one.

The second option work better for you if you connect to different networks and therefore each WiFi you're granted different IP. 

#### Build everything

Execute ./updateAll.sh from repo/API directory.


First build can take lots of time (took 15 min on mac air) - subsequent build will be faster 
cause:
- you already have the java dependencies downloaded by Gradle
- you already have the docker images downloaded by Docker

#### Deploy
Execute ./deployAll.sh and wait up to 2 minutes. I tested it on fresh set up so as long as you have docker running there should be no problems with the setup.

To make sure its works just visit http://localhost:8080/clients/all - should present empty JSON array.

#### Other important info
You stop the system with ./stopAll.sh - it force-stops the docker containers.

You do not need to updateAll.sh every time you wanna deploy the system.
Use it only if you've downloaded new API version or changed the ip (although if you've used the docker interface IP this should not make a difference)

The API can be on on a single computer in LAN - and the rest might simply connect to it with local address.

## Summed up:
You execute update.sh < IP > when:
- you pulled the changes with new API version (including the first time - duh)
- the ip you've given to update.sh script last time is no longer valid

You execute deploy.sh when:
- you wanna turn the system on

You execute stop.sh when:
- you wanna turn the system off