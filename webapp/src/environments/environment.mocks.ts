export const environment = {
  name: 'mocks',
  production: false,
  useMocks: true,
  apiUrl: 'https://api.voidchuck.com',
  raspberryApiURL: 'http://192.168.50.5:5000'
};
