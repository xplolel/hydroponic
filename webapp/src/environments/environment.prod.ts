export const environment = {
  name: 'prod',
  production: true,
  useMocks: false,
  apiUrl: 'https://api.voidchuck.com',
  raspberryApiURL: 'http://192.168.50.5:5000'
};
