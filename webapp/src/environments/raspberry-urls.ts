export enum RaspberryURLs {
  scanNetworks = '/wifilist',
  sendPassword = '/connect'
}
