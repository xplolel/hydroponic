import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '@env/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { interceptors } from './interceptors';
import { AuthModule } from './modules/auth/auth.module';
import { UserInterfaceModule } from './modules/user-interface/user-interface.module';
import * as fromRoot from './store';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    }),
    AuthModule,
    StoreModule.forRoot(fromRoot.Reducers.core, {
      metaReducers: fromRoot.Reducers.meta,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    EffectsModule.forRoot([]),
    AppRoutingModule,
    BrowserAnimationsModule,
    UserInterfaceModule
  ],
  providers: [
    ...interceptors,
    { provide: 'ApiEndpoint', useValue: environment.apiUrl }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
