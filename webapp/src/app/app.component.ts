import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { ToastrService } from 'ngx-toastr';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly swUpdate: SwUpdate,
    private readonly toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.router.events
      .pipe(filter(val => val instanceof NavigationEnd))
      .subscribe(() => {
        document.querySelectorAll('[fill]').forEach(element => {
          const fill = element.getAttribute('fill');
          if (fill.startsWith('url(')) {
            element.setAttribute(
              'fill',
              `url(${this.router.url}${fill.substring(fill.indexOf('#'))}`
            );
          }
        });
      });

    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        this.toastr
          .info('New version is available', 'Click here to reload the app')
          .onHidden.subscribe(() => {
            window.location.reload();
          });
      });
    }
  }
}
