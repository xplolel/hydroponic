import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { PasswordResetComponent } from './components/password-reset/password-reset.component';
import { PasswordResetRoutingModule } from './password-reset-routing';

@NgModule({
  declarations: [PasswordResetComponent],
  imports: [
    CommonModule,
    PasswordResetRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UserInterfaceModule
  ]
})
export class PasswordResetModule {}
