import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterCredentials } from '@auth/models';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { RegisterComponent } from './components/register.page';
import { RegisterRoutingModule } from './register-routing.module';

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UserInterfaceModule
  ]
})
export class RegisterModule {
  credentials: RegisterCredentials = new RegisterCredentials();
}
