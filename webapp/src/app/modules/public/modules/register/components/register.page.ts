import { Component, HostBinding } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import * as fromAuth from '@auth/store';
import { Store } from '@ngrx/store';

function validatePassword(
  c: AbstractControl
): { [key: string]: boolean } | null {
  const value = c.value;

  return value && (value.length < 6 || value.length > 64)
    ? { range: true }
    : null;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html'
})
export class RegisterComponent {
  @HostBinding('class.public-form') defaultClass = true;

  registerForm = this.fb.group({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.required, validatePassword]),
    confirmPassword: new FormControl('', [Validators.required])
  });

  constructor(
    private readonly fb: FormBuilder,
    private readonly store: Store<fromAuth.State>
  ) {}

  onSubmit(form: FormGroup): void {
    if (form.valid) {
      this.store.dispatch(
        fromAuth.Actions.register({
          credentials: {
            email: this.email.value,
            password: this.password.value
          }
        })
      );
    }
  }

  get email(): AbstractControl {
    return this.registerForm.get('email');
  }

  get password(): AbstractControl {
    return this.registerForm.get('password');
  }

  get confirmPassword(): AbstractControl {
    return this.registerForm.get('confirmPassword');
  }
}
