import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoginRoutingModule } from './login-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { LoginPageComponent } from './components/login.page';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UserInterfaceModule
  ],
  declarations: [LoginPageComponent]
})
export class LoginModule {}
