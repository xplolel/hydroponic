import { Component, HostBinding } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import * as fromAuth from '@auth/store';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html'
})
export class LoginPageComponent {
  @HostBinding('class.public-form') defaultClass = true;

  loginForm = this.fb.group({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  constructor(
    private readonly fb: FormBuilder,
    private readonly store: Store<fromAuth.State>
  ) {}

  onSubmit(form: FormGroup): void {
    if (form.valid) {
      this.store.dispatch(
        fromAuth.Actions.login({
          credentials: {
            email: this.email.value,
            password: this.password.value
          }
        })
      );
    }
  }

  get email(): AbstractControl {
    return this.loginForm.get('email');
  }

  get password(): AbstractControl {
    return this.loginForm.get('password');
  }
}
