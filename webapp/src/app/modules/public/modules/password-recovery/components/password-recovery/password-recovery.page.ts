import { Component, HostBinding } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromAuth from '@auth/store';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.page.html'
})
export class PasswordRecoveryComponent {
  @HostBinding('class.public-form') defaultClass = true;

  recoverForm = this.fb.group({
    email: new FormControl('', [Validators.email, Validators.required])
  });

  constructor(
    private readonly fb: FormBuilder,
    private readonly store: Store<fromAuth.State>,
    private readonly toastr: ToastrService
  ) {}

  get email(): AbstractControl {
    return this.recoverForm.get('email');
  }

  onSubmit(form: FormGroup): void {
    if (form.valid) {
      this.store.dispatch(
        fromAuth.Actions.sendResetPasswordEmail({
          email: this.email.value
        })
      );
    }

    this.toastr.success(
      'There you will find further instructions',
      'Check your e-mail'
    );
  }
}
