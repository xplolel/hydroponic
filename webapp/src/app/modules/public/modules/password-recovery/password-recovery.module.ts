import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { PasswordRecoveryComponent } from './components/password-recovery/password-recovery.page';
import { PasswordRecoveryRoutingModule } from './password-recovery-routing.module';

@NgModule({
  declarations: [PasswordRecoveryComponent],
  imports: [
    CommonModule,
    PasswordRecoveryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UserInterfaceModule
  ]
})
export class PasswordRecoveryModule {}
