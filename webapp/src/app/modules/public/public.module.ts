import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ToastContainerModule } from 'ngx-toastr';
import { PublicPageComponent } from './components/public-page/public-page.component';
import { PublicRoutingModule } from './public-routing.module';

@NgModule({
  declarations: [PublicPageComponent],
  imports: [CommonModule, PublicRoutingModule, ToastContainerModule]
})
export class PublicModule {}
