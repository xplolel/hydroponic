import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicPageComponent } from './components/public-page/public-page.component';

const routes: Routes = [
  {
    path: '',
    component: PublicPageComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      {
        path: 'login',
        loadChildren: () =>
          import('./modules/login/login.module').then(m => m.LoginModule)
      },
      {
        path: 'register',
        loadChildren: () =>
          import('./modules/register/register.module').then(
            m => m.RegisterModule
          )
      },
      {
        path: 'recover',
        loadChildren: () =>
          import('./modules/password-recovery/password-recovery.module').then(
            m => m.PasswordRecoveryModule
          )
      },
      {
        path: 'reset',
        loadChildren: () =>
          import('./modules/password-reset/password-reset.module').then(
            m => m.PasswordResetModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule {}
