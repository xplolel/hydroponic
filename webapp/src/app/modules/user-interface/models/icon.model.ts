import { always } from 'ramda';
import { concat, fromEvent, interval, of } from 'rxjs';
import { concatMap, delay, map, startWith, take } from 'rxjs/operators';

export abstract class IconAnimation {
  constructor(
    public trigger: string,
    public time: number,
    public repeatCount: number
  ) {}
}

export class SvgIconAnimation extends IconAnimation {
  constructor(
    public trigger: string,
    public time: number,
    public repeatCount: number,
    public type: string,
    public reverse = false
  ) {
    super(trigger, time, repeatCount);
  }
}

export class LottieIconAnimation extends IconAnimation {
  constructor(
    public trigger: string,
    public time: number,
    public repeatCount: number,
    public startFrame: number,
    public stopFrame: number
  ) {
    super(trigger, time, repeatCount);
  }
}

export enum SvgIconAnimationType {
  LOOP = 'loop',
  STRENGTHEN = 'strengthen',
  ATTRACT = 'attract',
  CUSTOM = 'custom'
}

export enum SvgIconStyle {
  SOLID = 'solid',
  STROKE = 'stroke',
  DEFAULT = 'default'
}

export enum IconSize {
  NONE = 'none',
  XXS = 'xxs',
  XS = 'xs',
  S = 's',
  M = 'm',
  L = 'l',
  XL = 'xl',
  XXL = 'xxl'
}

export enum SvgIconTypes {
  plant = 'plant',
  device = 'device',
  profile = 'profile',
  calendar = 'calendar',
  scissors = 'scissors',
  pencil = 'pencil',
  plantFill = 'plant-fill',
  arrowUp = 'arrow-up',
  arrowDown = 'arrow-down',
  clean = 'clean',
  water = 'water-drop',
  tick = 'tick',
  trash = 'trash-can',
  arrowBack = 'arrow-back',
  close = 'close',
  info = 'info',
  warning = 'exclamation-mark',
  lightBulb = 'light-bulb',
  warningTriangle = 'warning',
  plantBig = 'plant-big',
  seeds = 'seeds',
  temperature = 'temperature'
}

export enum LottieIconTypes {
  loading = 'loading'
}

export const getAnimationObservableByTrigger = <T extends IconAnimation>(
  anim: T,
  icon: HTMLElement
) =>
  anim.trigger === 'instant'
    ? of(anim)
    : fromEvent(icon, anim.trigger).pipe(map(always(anim)));

export const orchestrateAnimation = <T extends IconAnimation>(anim: T) =>
  interval(anim.time).pipe(
    startWith(0),
    take(anim.repeatCount),
    concatMap(() => concat(of(anim), of(null).pipe(delay(anim.time))))
  );
