import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { ToastrModule } from 'ngx-toastr';
import { ButtonComponent } from './components/button/button.component';
import { FormControlComponent } from './components/form-control/form-control.component';
import { LottieIconComponent } from './components/icon/lottie-icon/lottie-icon.component';
import { SvgIconComponent } from './components/icon/svg-icon/svg-icon.component';
import { SignalIconComponent } from './components/signal-icon/signal-icon.component';
import { TimePickerComponent } from './components/time-picker/time-picker.component';
import { ToastComponent } from './components/toast/toast.component';
import { ImgSrcDirective } from './directives/img-src.directive';

@NgModule({
  imports: [
    CommonModule,
    InlineSVGModule.forRoot(),
    ToastrModule.forRoot({
      progressBar: true,
      timeOut: 10000,
      extendedTimeOut: 10000,
      positionClass: 'inline',
      toastComponent: ToastComponent,
      closeButton: true,
      maxOpened: 4,
      autoDismiss: true
    })
  ],
  declarations: [
    LottieIconComponent,
    SvgIconComponent,
    ImgSrcDirective,
    SignalIconComponent,
    TimePickerComponent,
    ButtonComponent,
    FormControlComponent,
    ToastComponent
  ],
  exports: [
    LottieIconComponent,
    SvgIconComponent,
    ImgSrcDirective,
    SignalIconComponent,
    TimePickerComponent,
    ButtonComponent,
    FormControlComponent
  ],
  entryComponents: [ToastComponent]
})
export class UserInterfaceModule {}
