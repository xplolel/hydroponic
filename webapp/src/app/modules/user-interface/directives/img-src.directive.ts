import { Directive, HostBinding, Input } from '@angular/core';
import { environment } from '@env/environment';

@Directive({
  selector: '[appImgSrc]'
})
export class ImgSrcDirective {
  @HostBinding('style.background-image')
  backgroundImage = '';

  @Input() set appImgSrc(src: string) {
    let url = src;
    if (!environment.useMocks) {
      url = `${environment.apiUrl}/${src}`;
    }
    this.backgroundImage = `url(${url})`;
  }
}
