import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '.form-control',
  template: '<ng-content></ng-content>',
  styleUrls: ['./form-control.component.scss']
})
export class FormControlComponent {}
