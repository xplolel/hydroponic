import {
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  OnInit
} from '@angular/core';
import { SafeStyle } from '@angular/platform-browser';
import { BehaviorSubject, empty, interval } from 'rxjs';
import { filter, map, switchMap, take } from 'rxjs/operators';

const TRANSITION_DURATION = 200;

@Component({
  // tslint:disable-next-line: component-selector
  selector: '.btn',
  template: '<ng-content></ng-content>',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  private readonly hasHover = new BehaviorSubject<boolean>(null);
  index = 0;
  baseIndex: number;

  constructor(readonly el: ElementRef) {}

  ngOnInit(): void {
    this.baseIndex = +this.el.nativeElement.style.zIndex;
    this.hasHover
      .pipe(
        filter(val => val !== null),
        map(val => (val ? [this.index, 2] : [this.index, 0])),
        switchMap(([a, b]) =>
          a === b
            ? empty()
            : interval(TRANSITION_DURATION / 100).pipe(
                take(100),
                map(val => val + 1),
                map(val => Math.ceil((val / 100) * (b - a) + a))
              )
        )
      )
      .subscribe(val => {
        this.index = val;
      });
  }

  @HostBinding('style.zIndex')
  get zIndex(): SafeStyle {
    return `${this.index + this.baseIndex}`;
  }

  @HostListener('mouseenter') onEnter(): void {
    if (!this.el.nativeElement.classList.contains('disabled')) {
      this.hasHover.next(true);
    }
  }

  @HostListener('mouseleave') onLeave(): void {
    this.hasHover.next(false);
  }
}
