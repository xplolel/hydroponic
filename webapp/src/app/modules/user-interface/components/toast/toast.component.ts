import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { Component } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Toast, ToastPackage, ToastrService } from 'ngx-toastr';
import { SvgIconTypes } from '../../models/icon.model';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('inactive', style({ opacity: 0, transform: 'translateX(100%)' })),
      state('active', style({ opacity: 1, transform: 'translateX(0)' })),
      state('removed', style({ opacity: 0, transform: 'translateX(100%)' })),
      transition(
        'inactive => active',
        animate('{{ easeTime }}ms {{ easing }}')
      ),
      transition('active => removed', animate('{{ easeTime }}ms {{ easing }}'))
    ])
  ]
})
export class ToastComponent extends Toast {
  SvgIconTypes = SvgIconTypes;

  constructor(
    private readonly sanitizer: DomSanitizer,
    toastrService: ToastrService,
    toastPackage: ToastPackage
  ) {
    super(toastrService, toastPackage);
  }

  getIconName(): SvgIconTypes {
    if (this.toastClasses.includes('success')) {
      return SvgIconTypes.tick;
    }

    if (this.toastClasses.includes('info')) {
      return SvgIconTypes.info;
    }

    if (this.toastClasses.includes('warning')) {
      return SvgIconTypes.warning;
    }

    return SvgIconTypes.close;
  }

  getProgressTransform(): SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle(
      `translateX(${100 - this.width}%)`
    );
  }
}
