import { Component, Input } from '@angular/core';

const ICON_SIZE = 100;

class SignalBar {
  opacity = 1;
  readonly svgPath: string;
  readonly width: number;

  constructor(readonly bars: number, readonly index: number) {
    this.width = ICON_SIZE / (bars * 2 - 1);

    const x = this.width * (index * 2 + 0.5);
    const y = ICON_SIZE - this.width / 2;
    const height = ((ICON_SIZE - this.width) / bars) * (index + 1);

    this.svgPath = `
      M${x} ${y}
      L${x} ${y - height}
    `;
  }

  set(val: boolean): void {
    this.opacity = val ? 1 : 0.3;
  }
}

@Component({
  selector: 'app-signal-icon',
  templateUrl: './signal-icon.component.html',
  styleUrls: ['./signal-icon.component.scss']
})
export class SignalIconComponent {
  viewBox = `0 0 ${ICON_SIZE} ${ICON_SIZE}`;
  signalBars: SignalBar[] = [];
  _value: number;

  @Input()
  set value(val: number) {
    this._value = val;

    this.updateValue();
  }

  @Input()
  set bars(val: number) {
    this.signalBars = [];
    for (let i = 0; i < val; i++) {
      this.signalBars.push(new SignalBar(val, i));
    }

    this.updateValue();
  }

  updateValue(): void {
    this.signalBars.forEach((bar, i) =>
      bar.set(this._value >= (i + 0.5) / this.signalBars.length)
    );
  }
}
