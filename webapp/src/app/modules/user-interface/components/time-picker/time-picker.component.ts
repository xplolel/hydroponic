import { Component, EventEmitter, Input, Output } from '@angular/core';
import { humanizeTimeUnit, Time } from '@core/models/time';
import { cloneDeep } from 'lodash';
import { SvgIconTypes } from '../../models/icon.model';

@Component({
  selector: 'app-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.scss']
})
export class TimePickerComponent {
  SvgIconTypes = SvgIconTypes;
  humanizeTimeUnit = humanizeTimeUnit;
  _time: Time = new Time(0, 0, 0);

  @Input() showHours = false;
  @Input() showMinutes = false;
  @Input() showSeconds = false;

  @Input() set time(val: Time) {
    this._time = cloneDeep(val);
  }

  get time(): Time {
    return this._time;
  }

  @Output() readonly timeChange = new EventEmitter();

  incrementHours(): void {
    this.time.incrementHours();
    this.timeChange.emit(this.time);
  }

  incrementMinutes(): void {
    this.time.incrementMinutes();
    this.timeChange.emit(this.time);
  }

  incrementSeconds(): void {
    this.time.incrementSeconds();
    this.timeChange.emit(this.time);
  }

  decrementHours(): void {
    this.time.decrementHours();
    this.timeChange.emit(this.time);
  }

  decrementMinutes(): void {
    this.time.decrementMinutes();
    this.timeChange.emit(this.time);
  }

  decrementSeconds(): void {
    this.time.decrementSeconds();
    this.timeChange.emit(this.time);
  }
}
