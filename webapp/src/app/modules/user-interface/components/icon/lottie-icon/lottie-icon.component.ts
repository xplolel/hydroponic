import {
  Component,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import * as lottie from 'lottie-web';
import { BehaviorSubject, from, merge, Subscription } from 'rxjs';
import { filter, mergeMap, switchMap } from 'rxjs/operators';
import {
  getAnimationObservableByTrigger,
  LottieIconAnimation,
  LottieIconTypes,
  orchestrateAnimation
} from '../../../models/icon.model';

@Component({
  selector: 'app-lottie-icon',
  templateUrl: './lottie-icon.component.html',
  styleUrls: ['../icon.component.scss']
})
export class LottieIconComponent implements OnInit, OnDestroy {
  @ViewChild('lottiecanvas', { static: true })
  set canvas(el: ElementRef) {
    this.canvasRef = el;
  }

  baseFramerate = 60;
  stopFrame = 0;
  canvasRef: ElementRef;
  anim: lottie.AnimationItem;

  private readonly subscription = new Subscription();
  elementRef: ElementRef;

  @Input()
  animations: LottieIconAnimation[] = [];

  @Input()
  name: LottieIconTypes;

  private readonly _dispatchedAnimations = new BehaviorSubject<
    LottieIconAnimation
  >(null);
  private readonly dispatchedAnimations$ = this._dispatchedAnimations
    .asObservable()
    .pipe(filter(animation => !!animation));

  constructor(
    el: ElementRef,
    private readonly router: Router,
    private readonly ngZone: NgZone
  ) {
    this.elementRef = el;
  }

  ngOnInit(): void {
    this.anim = lottie.loadAnimation({
      container: this.canvasRef.nativeElement,
      renderer: 'svg',
      loop: false,
      autoplay: false,
      path: `assets/icons/lottie/${this.name}.json`
    });

    this.anim.renderer.renderConfig.viewBoxOnly = true;

    this.anim.addEventListener('DOMLoaded', this.onIconLoad.bind(this));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  dispatch(animation: LottieIconAnimation): void {
    this._dispatchedAnimations.next(animation);
  }

  animationCallback(animation: LottieIconAnimation): void {
    if (animation) {
      const frames = animation.stopFrame - animation.startFrame;
      const speed = frames / this.baseFramerate / (animation.time / 1000);
      this.ngZone.runOutsideAngular(() => {
        this.anim.setSpeed(speed);
        this.anim.playSegments(
          [animation.startFrame, animation.stopFrame],
          true
        );
      });
      this.stopFrame = animation.stopFrame;
    } else {
      this.ngZone.runOutsideAngular(() => {
        this.anim.goToAndStop(this.stopFrame, true);
      });
    }
  }

  onIconLoad(): void {
    this.baseFramerate = this.anim.frameRate;
    this.elementRef.nativeElement
      .querySelectorAll('[fill]')
      .forEach((element: HTMLElement) => {
        const fill = element.getAttribute('fill');
        if (fill.startsWith('url(')) {
          element.setAttribute(
            'fill',
            `url(${this.router.url}${fill.substring(fill.indexOf('#'))}`
          );
        }
      });

    this.subscription.add(
      merge(this.dispatchedAnimations$, from(this.animations))
        .pipe(
          mergeMap(animation =>
            getAnimationObservableByTrigger(
              animation,
              this.elementRef.nativeElement
            )
          ),
          switchMap(orchestrateAnimation)
        )
        .subscribe(animation => this.animationCallback(animation))
    );
  }
}
