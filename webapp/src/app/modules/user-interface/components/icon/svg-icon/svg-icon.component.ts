import { Component, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, concat, from, Subscription } from 'rxjs';
import { filter, mergeMap, switchMap } from 'rxjs/operators';
import {
  getAnimationObservableByTrigger,
  orchestrateAnimation,
  SvgIconAnimation,
  SvgIconStyle,
  SvgIconTypes
} from '../../../models/icon.model';

@Component({
  selector: 'app-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['../icon.component.scss']
})
export class SvgIconComponent implements OnInit, OnDestroy {
  @Input()
  iconStyle: SvgIconStyle = SvgIconStyle.SOLID;

  currentAnimation: SvgIconAnimation;

  private readonly subscription = new Subscription();
  elementRef: ElementRef;

  @Input()
  animations: SvgIconAnimation[] = [];

  @Input()
  name: SvgIconTypes;

  private readonly _dispatchedAnimations = new BehaviorSubject<
    SvgIconAnimation
  >(null);
  private readonly dispatchedAnimations$ = this._dispatchedAnimations
    .asObservable()
    .pipe(filter(animation => !!animation));

  constructor(el: ElementRef) {
    this.elementRef = el;
  }

  ngOnInit(): void {
    this.subscription.add(
      concat(this.dispatchedAnimations$, from(this.animations))
        .pipe(
          mergeMap(animation =>
            getAnimationObservableByTrigger(
              animation,
              this.elementRef.nativeElement
            )
          ),
          switchMap(orchestrateAnimation)
        )
        .subscribe(animation => this.animationCallback(animation))
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  dispatch(animation: SvgIconAnimation): void {
    this._dispatchedAnimations.next(animation);
  }

  animationCallback(animation: SvgIconAnimation): void {
    this.currentAnimation = animation;
  }

  getPath(): string {
    return `assets/icons/svg/${this.name}.svg`;
  }

  getStyleClass(): string {
    return `icon-${this.iconStyle}`;
  }

  getAnimationClass(): string {
    return this.currentAnimation
      ? `icon-animating animation-${this.currentAnimation.type}${
          this.currentAnimation.reverse ? '-reverse' : ''
        }`
      : '';
  }

  getClass(): string {
    return `${this.getStyleClass()} ${this.getAnimationClass()} `;
  }
}
