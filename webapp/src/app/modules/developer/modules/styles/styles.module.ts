import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { StylesRoutingModule } from './styles-routing.module';
import { StylesPageComponent } from './styles.page';

@NgModule({
  imports: [CommonModule, StylesRoutingModule, UserInterfaceModule],
  declarations: [StylesPageComponent]
})
export class StylesModule {}
