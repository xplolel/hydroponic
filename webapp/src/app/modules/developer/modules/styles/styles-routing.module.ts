import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StylesPageComponent } from './styles.page';

const routes: Routes = [{ path: '', component: StylesPageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StylesRoutingModule {}
