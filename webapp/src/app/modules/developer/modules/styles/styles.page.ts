import { Component } from '@angular/core';

@Component({
  selector: 'app-styles-page',
  templateUrl: './styles.page.html',
  styleUrls: ['./styles.page.scss']
})
export class StylesPageComponent {
  baseColors = ['light-green', 'aqua', 'blue', 'red', 'pink', 'gray'];

  grays = [
    'white',
    'gray-50',
    'gray-200',
    'gray-400',
    'gray-600',
    'gray-800',
    'black'
  ];

  baseColorAccents = [
    'light-green-white-accent',
    'light-green-black-accent',
    'aqua-white-accent',
    'aqua-black-accent',
    'blue-white-accent',
    'blue-black-accent',
    'red-white-accent',
    'red-black-accent',
    'pink-white-accent',
    'pink-black-accent',
    'gray-white-accent',
    'gray-black-accent'
  ];

  gradients = ['1', '2'];

  colors = [
    'light-green',
    'aqua',
    'blue',
    'red',
    'pink',
    'gray',
    'white',
    'gray-200',
    'gray-400',
    'gray-600',
    'gray-800',
    'black',
    'light-green-white-accent',
    'light-green-black-accent',
    'aqua-white-accent',
    'aqua-black-accent',
    'blue-white-accent',
    'blue-black-accent',
    'red-white-accent',
    'red-black-accent',
    'pink-white-accent',
    'pink-black-accent',
    'gray-white-accent',
    'gray-black-accent'
  ];

  spacingMultipliers = ['xxs', 'xs', 's', 'm', 'l', 'xl', 'xxl'];

  textVariants = ['xxxl', 'xxl', 'xl', 'l', 'm', 's', 'xs', 'xxs'];
}
