import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DeveloperRoutingModule } from './developer-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, DeveloperRoutingModule]
})
export class DeveloperModule {}
