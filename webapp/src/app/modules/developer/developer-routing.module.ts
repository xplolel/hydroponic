import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'styles', pathMatch: 'full' },
  {
    path: 'styles',
    loadChildren: () =>
      import('./modules/styles/styles.module').then(m => m.StylesModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeveloperRoutingModule {}
