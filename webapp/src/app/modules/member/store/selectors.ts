import { createFeatureSelector } from '@ngrx/store';
import { featureKey, FeatureState, State } from './state';

export const selectFeatureState = () =>
  createFeatureSelector<State, FeatureState>(featureKey);
