import { Action, combineReducers } from '@ngrx/store';
import { core as DeviceListReducers } from './features/device-list/reducers';
import { featureKey as DeviceListFeatureKey } from './features/device-list/state';
import { core as ModuleReducers } from './features/module/reducers';
import { featureKey as ModuleFeatureKey } from './features/module/state';
import { core as PlantListReducers } from './features/plant-list/reducers';
import { featureKey as PlantListFeatureKey } from './features/plant-list/state';
import { core as ProfileReducers } from './features/profile/reducers';
import { featureKey as ProfileFeatureKey } from './features/profile/state';
import { FeatureState } from './state';

export function core(
  state: FeatureState | undefined,
  action: Action
): FeatureState {
  return combineReducers({
    [DeviceListFeatureKey]: DeviceListReducers,
    [ModuleFeatureKey]: ModuleReducers,
    [PlantListFeatureKey]: PlantListReducers,
    [ProfileFeatureKey]: ProfileReducers
  })(state, action);
}

export const meta = [];
