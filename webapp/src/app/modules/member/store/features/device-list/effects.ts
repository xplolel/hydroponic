import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceService } from '@member/services/device.service';
import { LoadingService } from '@member/services/loading.service';
import { Actions as Actions$, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import {
  catchError,
  filter,
  finalize,
  map,
  switchMap,
  take,
  tap
} from 'rxjs/operators';
import * as Actions from './actions';
import * as Selectors from './selectors';
import { State } from './state';

const compareByKey = <T>(key: string) => (a: T, b: T) => a[key] - b[key];

@Injectable()
export class Effects {
  loadDevices$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.loadDeviceList),
      tap(() => this.loadingService.start(Actions.loadDeviceList.type)),
      switchMap(() =>
        this.deviceService.getSystems().pipe(
          finalize(() => this.loadingService.stop(Actions.loadDeviceList.type)),
          map(devices =>
            devices.map(device => ({
              ...device,
              modules: device.modules.map(hydroModule => ({
                ...hydroModule,
                slots: hydroModule.slots
                  .sort(compareByKey('y'))
                  .sort(compareByKey('x'))
              }))
            }))
          ),
          map(devices => Actions.loadDeviceListSuccess({ devices })),
          catchError(error => of(Actions.loadDeviceListFailure({ error })))
        )
      )
    )
  );

  addPlantToSlot$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.addPlantToSlot),
      tap(() => this.loadingService.start(Actions.addPlantToSlot.type)),
      switchMap(({ systemId, moduleId, slotId, plantId, plant }) =>
        this.deviceService
          .addPlantToSlot(systemId, moduleId, slotId, plantId)
          .pipe(
            finalize(() =>
              this.loadingService.stop(Actions.addPlantToSlot.type)
            ),
            map(slot =>
              Actions.addPlantToSlotSuccess({
                plant,
                slot,
                systemId,
                moduleId
              })
            ),
            catchError(error => of(Actions.addPlantToSlotFailure({ error })))
          )
      )
    )
  );

  fillDeviceListWithPlants$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.fillDeviceListWithPlants),
      switchMap(() =>
        this.store.pipe(
          select(Selectors.getFilledWithPlants()),
          filter(filledWithPlants => !filledWithPlants),
          take(1)
        )
      ),
      switchMap(() =>
        this.store.pipe(
          select(Selectors.getDeviceList()),
          take(1)
        )
      ),
      tap(() =>
        this.loadingService.start(Actions.fillDeviceListWithPlants.type)
      ),
      switchMap(devices =>
        this.deviceService.fillDeviceListWithPlants(devices).pipe(
          finalize(() =>
            this.loadingService.stop(Actions.fillDeviceListWithPlants.type)
          ),
          map(filledDevices =>
            Actions.fillDeviceListWithPlantsSuccess({
              devices: filledDevices
            })
          ),
          catchError(error =>
            of(
              Actions.fillDeviceListWithPlantsFailure({
                error
              })
            )
          )
        )
      )
    )
  );

  addModule$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.addModule),
      tap(() => this.loadingService.start(Actions.addModule.type)),
      switchMap(({ systemId, moduleName }) =>
        this.deviceService.addModule(systemId, moduleName).pipe(
          finalize(() => this.loadingService.stop(Actions.addModule.type)),
          map(hydroModule => ({
            ...hydroModule,
            slots: hydroModule.slots
              .sort(compareByKey('y'))
              .sort(compareByKey('x'))
          })),
          map(hydroModule =>
            Actions.addModuleSuccess({
              systemId,
              hydroModule
            })
          ),
          catchError(error => of(Actions.addModuleFailure({ error })))
        )
      )
    )
  );

  deleteModule$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.deleteModule),
      tap(() => this.loadingService.start(Actions.deleteModule.type)),
      switchMap(({ systemId, moduleId }) =>
        this.deviceService.deleteModule(systemId, moduleId).pipe(
          finalize(() => this.loadingService.stop(Actions.deleteModule.type)),
          map(
            () =>
              Actions.deleteModuleSuccess({
                systemId,
                moduleId
              }),
            catchError(error => of(Actions.deleteModuleFailure({ error })))
          ),
          tap(() => this.router.navigate(['/member/device']))
        )
      )
    )
  );

  clearPlant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.clearPlant),
      tap(() => this.loadingService.start(Actions.clearPlant.type)),
      switchMap(({ systemId, moduleId, slotId }) =>
        this.deviceService.clearPlant(systemId, moduleId, slotId).pipe(
          finalize(() => this.loadingService.stop(Actions.clearPlant.type)),
          map(() => Actions.clearPlantSucess({ systemId, moduleId, slotId })),
          catchError(error => of(Actions.clearPlantFailure({ error })))
        )
      )
    )
  );

  loadSystemsMeasures$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.loadSystemMeasures),
      tap(() => this.loadingService.start(Actions.clearPlant.type)),
      switchMap(({ devices }) => {
        const observables = devices.map(device =>
          this.deviceService.getSystemMeasures(device.id)
        );

        return combineLatest(observables).pipe(
          finalize(() => this.loadingService.stop(Actions.clearPlant.type)),
          map(systemsMeasure => systemsMeasure.filter(val => !!val)),
          map(systemsMeasure =>
            Actions.loadSystemMeasuresSuccess({ systemsMeasure })
          ),
          catchError(() => of(null))
        );
      })
    )
  );

  loadModulesMeasures$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.loadModulesMeasures),
      tap(() => this.loadingService.start(Actions.clearPlant.type)),
      switchMap(({ hydroModules }) => {
        const observables = hydroModules.map(hydroModule =>
          this.deviceService.getModuleMeasure(hydroModule.id)
        );

        return combineLatest(observables).pipe(
          finalize(() => this.loadingService.stop(Actions.clearPlant.type)),
          map(modulesMeasure => modulesMeasure.filter(val => !!val)),
          map(modulesMeasure =>
            Actions.loadModulesMeasuresSuccess({ modulesMeasure })
          )
        );
      })
    )
  );

  constructor(
    private readonly actions$: Actions$,
    private readonly deviceService: DeviceService,
    private readonly store: Store<State>,
    private readonly router: Router,
    private readonly loadingService: LoadingService
  ) {}
}
