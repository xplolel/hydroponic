import { Device } from '@device/models/device.model';
import {
  ModuleMeasure,
  SystemMeasure
} from '@member/modules/device/models/measure.model';

export { State } from '../../state';

export const featureKey = 'device-list';

export interface FeatureState {
  filledWithPlants: boolean;
  devices: Device[];
  systemsMeasure: SystemMeasure[];
  modulesMeasure: ModuleMeasure[];
}

export const initialState: FeatureState = {
  filledWithPlants: true,
  devices: [],
  systemsMeasure: [],
  modulesMeasure: []
};
