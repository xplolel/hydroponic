import { Slot } from '@device/models/slot.model';
import { Action, createReducer, on } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import * as Actions from './actions';
import { FeatureState, initialState } from './state';

export function core(
  _state: FeatureState | undefined,
  action: Action
): FeatureState {
  return createReducer(
    initialState,
    on(Actions.loadDeviceListSuccess, (state, { devices }) => ({
      ...state,
      devices,
      filledWithPlants: false
    })),
    on(Actions.fillDeviceListWithPlantsSuccess, (state, { devices }) => ({
      ...state,
      devices,
      filledWithPlants: true
    })),
    on(
      Actions.addPlantToSlotSuccess,
      (state, { systemId, moduleId, slot, plant }) => {
        const systemIndex = state.devices
          .map(item => item.id)
          .indexOf(systemId);

        const moduleIndex = state.devices[systemIndex].modules
          .map(item => item.id)
          .indexOf(moduleId);

        const slotIndex = state.devices[systemIndex].modules[moduleIndex].slots
          .map(item => item.id)
          .indexOf(slot.id);

        const newDevices = cloneDeep(state.devices);

        const newSlot: Slot = cloneDeep(slot);
        newSlot.plant = plant;

        newDevices[systemIndex].modules[moduleIndex].slots[slotIndex] = newSlot;

        return {
          ...state,
          devices: newDevices
        };
      }
    ),
    on(Actions.clearPlantSucess, (state, { slotId, systemId, moduleId }) => {
      const systemIndex = state.devices
        .map(device => device.id)
        .indexOf(systemId);

      const moduleIndex = state.devices[systemIndex].modules
        .map(hydroModule => hydroModule.id)
        .indexOf(moduleId);

      const slotIndex = state.devices[systemIndex].modules[moduleIndex].slots
        .map(slot => slot.id)
        .indexOf(slotId);

      const newDevices = cloneDeep(state.devices);

      const newSlot: Slot = cloneDeep(
        state.devices[systemIndex].modules[moduleIndex].slots[slotIndex]
      );
      newSlot.plant = null;
      newSlot.plantId = null;

      newDevices[systemIndex].modules[moduleIndex].slots[slotIndex] = newSlot;

      return {
        ...state,
        devices: newDevices
      };
    }),
    on(Actions.addModuleSuccess, (state, { systemId, hydroModule }) => {
      const systemIndex = state.devices
        .map(device => device.id)
        .indexOf(systemId);

      const newSystems = cloneDeep(state.devices);
      const newSystem = cloneDeep(state.devices[systemIndex]);

      newSystem.modules.push(hydroModule);
      newSystems[systemIndex] = newSystem;

      return {
        ...state,
        devices: newSystems
      };
    }),
    on(Actions.deleteModuleSuccess, (state, { systemId, moduleId }) => {
      const systemIndex = state.devices
        .map(device => device.id)
        .indexOf(systemId);

      const moduleIndex = state.devices[systemIndex].modules
        .map(hydroModule => hydroModule.id)
        .indexOf(moduleId);

      const newSystems = cloneDeep(state.devices);
      const newSystem = cloneDeep(state.devices[systemIndex]);

      newSystem.modules.splice(moduleIndex, 1);
      newSystems[systemIndex] = newSystem;

      return {
        ...state,
        devices: newSystems
      };
    }),
    on(Actions.loadSystemMeasuresSuccess, (state, { systemsMeasure }) => ({
      ...state,
      systemsMeasure
    })),
    on(Actions.loadModulesMeasuresSuccess, (state, { modulesMeasure }) => ({
      ...state,
      modulesMeasure
    }))
  )(_state, action);
}
