import { HttpErrorResponse } from '@angular/common/http';
import { Device } from '@device/models/device.model';
import { HydroModule } from '@device/models/hydro-module.model';
import { Slot } from '@device/models/slot.model';
import {
  ModuleMeasure,
  SystemMeasure
} from '@member/modules/device/models/measure.model';
import { createAction, props } from '@ngrx/store';
import { Plant } from '@plants/models';
import { featureKey } from './state';

export const loadDeviceList = createAction(`[${featureKey}] loadDeviceList`);

export const loadDeviceListSuccess = createAction(
  `[${featureKey}] loadDeviceListSuccess`,
  props<{ devices: Device[] }>()
);

export const loadDeviceListFailure = createAction(
  `[${featureKey}] loadDeviceListFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const fillDeviceListWithPlants = createAction(
  `[${featureKey}] fillDeviceListWithPlants`
);

export const fillDeviceListWithPlantsSuccess = createAction(
  `[${featureKey}] fillDeviceListWithPlantsSuccess`,
  props<{ devices: Device[] }>()
);

export const fillDeviceListWithPlantsFailure = createAction(
  `[${featureKey}] fillDeviceListWithPlantsFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const addPlantToSlot = createAction(
  `[${featureKey}] addPlantToSlot`,
  props<{
    plant: Plant;
    plantId: string;
    slotId: string;
    systemId: string;
    moduleId: string;
  }>()
);

export const addPlantToSlotSuccess = createAction(
  `[${featureKey}] addPlantToSlotSuccess`,
  props<{
    plant: Plant;
    slot: Slot;
    systemId: string;
    moduleId: string;
  }>()
);

export const addPlantToSlotFailure = createAction(
  `[${featureKey}] addPlantToSlotFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const clearPlant = createAction(
  `[${featureKey}] clearPlant`,
  props<{
    slotId: string;
    systemId: string;
    moduleId: string;
  }>()
);

export const clearPlantSucess = createAction(
  `[${featureKey}] clearPlantSucess`,
  props<{
    slotId: string;
    systemId: string;
    moduleId: string;
  }>()
);

export const clearPlantFailure = createAction(
  `[${featureKey}] clearPlantFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const addModule = createAction(
  `[${featureKey}] addModule`,
  props<{ systemId: string; moduleName: string }>()
);

export const addModuleSuccess = createAction(
  `[${featureKey}] addModuleSuccess`,
  props<{ systemId: string; hydroModule: HydroModule }>()
);

export const addModuleFailure = createAction(
  `[${featureKey}] addModuleFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const deleteModule = createAction(
  `[${featureKey}] deleteModule`,
  props<{ systemId: string; moduleId: string }>()
);

export const deleteModuleSuccess = createAction(
  `[${featureKey}] deleteModuleSuccess`,
  props<{ systemId: string; moduleId: string }>()
);

export const deleteModuleFailure = createAction(
  `[${featureKey}] deleteModuleFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const loadSystemMeasures = createAction(
  `[${featureKey}] loadSystemMeasures`,
  props<{ devices: Device[] }>()
);

export const loadSystemMeasuresSuccess = createAction(
  `[${featureKey}] loadSystemMeasuresSuccess`,
  props<{ systemsMeasure: SystemMeasure[] }>()
);
export const loadModulesMeasures = createAction(
  `[${featureKey}] loadModulesMeasures`,
  props<{ hydroModules: HydroModule[] }>()
);

export const loadModulesMeasuresSuccess = createAction(
  `[${featureKey}] loadModulesMeasuresSuccess`,
  props<{ modulesMeasure: ModuleMeasure[] }>()
);
