import { createSelector } from '@ngrx/store';
import { selectFeatureState as selectParentState } from '../../selectors';
import { FeatureState as ParentState } from '../../state';
import { featureKey, FeatureState } from './state';

export const selectFeatureState = () =>
  createSelector(
    selectParentState(),
    (state: ParentState) => state[featureKey]
  );

export const getDeviceList = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.devices
  );

export const getSystemsMeasure = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.systemsMeasure
  );

export const getModulesMeasure = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.modulesMeasure
  );

export const getFilledWithPlants = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.filledWithPlants
  );

export const getModuleList = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.devices.map(device => device.modules)
  );

export const getModuleById = (id: string) =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) =>
      state.devices
        .map(device => device.modules)
        .map(hydroModules =>
          hydroModules.find(hydroModule => hydroModule.id === id)
        )
        .find(hydroModule => !!hydroModule)
  );

export const getSlotById = (slotId: string, moduleId: string) =>
  createSelector(
    getModuleById(moduleId),
    hydroModule =>
      hydroModule && hydroModule.slots.find(slot => slot.id === slotId)
  );
