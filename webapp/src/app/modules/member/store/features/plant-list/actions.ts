import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { Plant } from '@plants/models/plant.model';
import { featureKey } from './state';

export const loadPlantList = createAction(`${featureKey}] loadPlantList`);

export const loadPlantListSuccess = createAction(
  `${featureKey}] loadPlantListSuccess`,
  props<{ plants: Plant[] }>()
);

export const loadPlantListFailure = createAction(
  `${featureKey}] loadPlantListFailure`,
  props<{ error: HttpErrorResponse }>()
);
