import { Plant } from '@plants/models';

export const featureKey = 'plants';

export interface FeatureState {
  plants: Plant[];
}

export const initialState: FeatureState = {
  plants: []
};
