import { Action, createReducer, on } from '@ngrx/store';
import * as Actions from './actions';
import { FeatureState, initialState } from './state';

export function core(
  _state: FeatureState | undefined,
  action: Action
): FeatureState {
  return createReducer(
    initialState,
    on(Actions.loadPlantListSuccess, (state, { plants }) => ({
      ...state,
      plants
    }))
  )(_state, action);
}
