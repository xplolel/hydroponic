import { createSelector } from '@ngrx/store';
import { selectFeatureState as selectParentState } from '../../selectors';
import { FeatureState as ParentState } from '../../state';
import { featureKey, FeatureState } from './state';

export const selectFeatureState = () =>
  createSelector(
    selectParentState(),
    (state: ParentState) => state[featureKey]
  );

export const getPlantList = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.plants
  );

export const getPlantById = (id: string) =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.plants.find(plant => plant.id === id)
  );
