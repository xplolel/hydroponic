import { Injectable } from '@angular/core';
import { LoadingService } from '@member/services/loading.service';
import { Actions as Actions$, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, finalize, map, switchMap, tap } from 'rxjs/operators';
import { PlantService } from '../../../services/plant.service';
import * as Actions from './actions';

@Injectable()
export class Effects {
  loadPlants$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.loadPlantList),
      tap(() => this.loadingService.start(Actions.loadPlantList.type)),
      switchMap(() =>
        this.plantService.getPlantsList().pipe(
          finalize(() => this.loadingService.stop(Actions.loadPlantList.type)),
          map(plants => Actions.loadPlantListSuccess({ plants })),
          catchError(error => of(Actions.loadPlantListFailure({ error })))
        )
      )
    )
  );

  constructor(
    private readonly actions$: Actions$,
    private readonly plantService: PlantService,
    private readonly loadingService: LoadingService
  ) {}
}
