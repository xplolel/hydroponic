import { Action, createReducer, on } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import * as Actions from './actions';
import { FeatureState, initialState } from './state';

export function core(
  _state: FeatureState | undefined,
  action: Action
): FeatureState {
  return createReducer(
    initialState,
    on(Actions.getModuleSettingsSuccess, (state, { settings }) => ({
      ...state,
      settings
    })),
    on(Actions.setModuleSettingsSuccess, (state, { range, halfLight }) => {
      const newSettings = cloneDeep(state.settings);
      newSettings.range = range;
      newSettings.halfLight = halfLight;

      return {
        ...state,
        settings: newSettings
      };
    })
  )(_state, action);
}
