import { createSelector } from '@ngrx/store';
import { selectFeatureState as selectParentState } from '../../selectors';
import { FeatureState as ParentState } from '../../state';
import { featureKey, FeatureState } from './state';

export const selectFeatureState = () =>
  createSelector(
    selectParentState(),
    (state: ParentState) => state[featureKey]
  );

export const getSettings = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.settings
  );
