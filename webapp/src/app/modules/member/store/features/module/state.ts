import { ModuleSettings } from '@device/models/device-settings.model';

export { State } from '../../state';

export const featureKey = 'module';

export interface FeatureState {
  settings: ModuleSettings | null;
}

export const initialState: FeatureState = {
  settings: null
};
