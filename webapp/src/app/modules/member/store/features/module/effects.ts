import { Injectable } from '@angular/core';
import { DeviceService } from '@member/services/device.service';
import { LoadingService } from '@member/services/loading.service';
import { OrderService } from '@member/services/order.service';
import { Actions as Actions$, createEffect, ofType } from '@ngrx/effects';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { catchError, finalize, map, switchMap, tap } from 'rxjs/operators';
import * as Actions from './actions';

@Injectable()
export class Effects {
  getModuleSettings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.getModuleSettings),
      tap(() => this.loadingService.start(Actions.getModuleSettings.type)),
      switchMap(({ moduleId, timezoneShift }) =>
        this.deviceService.getModuleSettings(moduleId, timezoneShift).pipe(
          finalize(() =>
            this.loadingService.stop(Actions.getModuleSettings.type)
          ),
          map(
            settings =>
              Actions.getModuleSettingsSuccess({
                settings
              }),
            catchError(error => of(Actions.getModuleSettingsFailure({ error })))
          )
        )
      )
    )
  );

  setModuleSettings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.setModuleSettings),
      tap(() => this.loadingService.start(Actions.setModuleSettings.type)),
      switchMap(({ systemId, moduleId, range, halfLight, timezoneShift }) =>
        this.deviceService
          .setModuleSettings(
            systemId,
            moduleId,
            range,
            halfLight,
            timezoneShift
          )
          .pipe(
            finalize(() =>
              this.loadingService.stop(Actions.setModuleSettings.type)
            ),
            map(
              () =>
                Actions.setModuleSettingsSuccess({
                  range,
                  halfLight
                }),
              catchError(error =>
                of(Actions.setModuleSettingsFailure({ error }))
              )
            )
          )
      )
    )
  );

  toggleLight$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.toggleLight),
      tap(() => this.loadingService.start(Actions.toggleLight.type)),
      switchMap(({ moduleId }) =>
        this.orderService.toggleLight(moduleId).pipe(
          finalize(() => this.loadingService.stop(Actions.toggleLight.type)),
          tap(lightState => {
            this.toastr.success(
              !lightState.lightOn ? 'Light turning on' : 'Light turning off',
              'Order sent',
              {
                timeOut: 2000
              }
            );
          }),
          map(() => Actions.toggleLightSuccess()),
          catchError(error => of(Actions.setModuleSettingsFailure({ error })))
        )
      )
    )
  );

  setModuleSettingsSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(Actions.setModuleSettingsSuccess),
        tap(() => {
          this.toastr.success(
            'Exposure hours have been updated',
            'Changes saved',
            {
              timeOut: 2000
            }
          );
        })
      ),
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions$,
    private readonly deviceService: DeviceService,
    private readonly orderService: OrderService,
    private readonly toastr: ToastrService,
    private readonly loadingService: LoadingService
  ) {}
}
