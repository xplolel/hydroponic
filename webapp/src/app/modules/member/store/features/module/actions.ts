import { HttpErrorResponse } from '@angular/common/http';
import { HourRange } from '@core/models/hour-range.model';
import { ModuleSettings } from '@device/models/device-settings.model';
import { createAction, props } from '@ngrx/store';
import { featureKey } from './state';

export const toggleLight = createAction(
  `[${featureKey}] toggleLight`,
  props<{ moduleId: string }>()
);

export const toggleLightSuccess = createAction(
  `[${featureKey}] toggleLightSuccess`
);

export const toggleLightFailure = createAction(
  `[${featureKey}] toggleLightFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const getModuleSettings = createAction(
  `[${featureKey}] getModuleSettings`,
  props<{
    moduleId: string;
    timezoneShift: { hours: number; minutes: number };
  }>()
);

export const getModuleSettingsSuccess = createAction(
  `[${featureKey}] getModuleSettingsSuccess`,
  props<{ settings: ModuleSettings }>()
);

export const getModuleSettingsFailure = createAction(
  `[${featureKey}] getModuleSettingsFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const setModuleSettings = createAction(
  `[${featureKey}] setModuleSettings`,
  props<{
    systemId: string;
    moduleId: string;
    range: HourRange;
    halfLight: boolean;
    timezoneShift: { hours: number; minutes: number };
  }>()
);

export const setModuleSettingsSuccess = createAction(
  `[${featureKey}] setModuleSettingsSuccess`,
  props<{ range: HourRange; halfLight: boolean }>()
);

export const setModuleSettingsFailure = createAction(
  `[${featureKey}] setModuleSettingsFailure`,
  props<{ error: HttpErrorResponse }>()
);
