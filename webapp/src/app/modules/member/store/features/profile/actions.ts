import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { UserDetails, UserProfile } from '@profile/models';

import { featureKey } from './state';

export const getUser = createAction(
  `[${featureKey}] getUser`,
  props<{ id: string }>()
);

export const getUserSuccess = createAction(
  `[${featureKey}] getUserSuccess`,
  props<{ profile: UserProfile }>()
);

export const getUserFailure = createAction(
  `[${featureKey}] getUserFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const updateUserDetails = createAction(
  `[${featureKey}] updateUserDetails`,
  props<{ id: string; userDetails: UserDetails }>()
);

export const updateUserDetailsSuccess = createAction(
  `[${featureKey}] updateUserDetailsSuccess`,
  props<{ userDetails: UserDetails }>()
);

export const updateUserDetailsFailure = createAction(
  `[${featureKey}] updateUserDetailsFailure`,
  props<{ error: HttpErrorResponse }>()
);
