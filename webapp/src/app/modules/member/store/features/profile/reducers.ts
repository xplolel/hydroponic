import { Action, createReducer, on } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import * as Actions from './actions';
import { FeatureState, initialState } from './state';

export function core(
  _state: FeatureState | undefined,
  action: Action
): FeatureState {
  return createReducer(
    initialState,
    on(Actions.getUserSuccess, (state, { profile }) => ({
      ...state,
      profile
    })),
    on(Actions.updateUserDetailsSuccess, (state, { userDetails }) => {
      const newProfile = cloneDeep(state.profile);
      newProfile.clientDetailsTO = userDetails;

      return {
        ...state,
        profile: newProfile
      };
    })
  )(_state, action);
}
