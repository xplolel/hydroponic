import { UserProfile } from '@profile/models';

export const featureKey = 'profile';

export interface FeatureState {
  profile: UserProfile | null;
}

export const initialState: FeatureState = {
  profile: null
};
