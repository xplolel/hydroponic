import { Injectable } from '@angular/core';
import { LoadingService } from '@member/services/loading.service';
import { Actions as Actions$, createEffect, ofType } from '@ngrx/effects';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { catchError, finalize, map, switchMap, tap } from 'rxjs/operators';
import { ProfileService } from '../../../services/profile.service';
import * as Actions from './actions';

@Injectable()
export class Effects {
  loadUserProfile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.getUser),
      tap(() => this.loadingService.start(Actions.getUser.type)),
      switchMap(({ id }) =>
        this.profileService.getUserProfile(id).pipe(
          finalize(() => this.loadingService.stop(Actions.getUser.type)),
          map(profile => Actions.getUserSuccess({ profile })),
          catchError(error => of(Actions.getUserFailure({ error })))
        )
      )
    )
  );

  updateUserProfile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.updateUserDetails),
      tap(() => this.loadingService.start(Actions.updateUserDetails.type)),
      switchMap(({ id, userDetails }) =>
        this.profileService.updateUserProfile(id, userDetails).pipe(
          finalize(() =>
            this.loadingService.stop(Actions.updateUserDetails.type)
          ),
          map(() =>
            Actions.updateUserDetailsSuccess({
              userDetails
            })
          ),
          catchError(error => of(Actions.updateUserDetailsFailure({ error })))
        )
      )
    )
  );

  updateUserProfileSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(Actions.updateUserDetailsSuccess),
        tap(() => {
          this.toastr.success(
            'Profile changes have been updated',
            'Changes saved',
            {
              timeOut: 2000
            }
          );
        })
      ),
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions$,
    private readonly profileService: ProfileService,
    private readonly toastr: ToastrService,
    private readonly loadingService: LoadingService
  ) {}
}
