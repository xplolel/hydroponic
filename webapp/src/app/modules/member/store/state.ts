import * as DeviceList from './features/device-list/state';
import * as Module from './features/module/state';
import * as PlantList from './features/plant-list/state';
import * as Profile from './features/profile/state';

export const featureKey = 'member';

export interface FeatureState {
  [DeviceList.featureKey]: DeviceList.FeatureState;
  [Module.featureKey]: Module.FeatureState;
  [PlantList.featureKey]: PlantList.FeatureState;
  [Profile.featureKey]: Profile.FeatureState;
}

export interface State {
  [featureKey]: FeatureState;
}
