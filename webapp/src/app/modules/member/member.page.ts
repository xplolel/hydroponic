import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import * as fromDeviceList from './store/features/device-list';

@Component({
  selector: 'app-member-page',
  templateUrl: './member.page.html',
  styleUrls: ['./member.page.scss']
})
export class MemberPageComponent implements OnInit {
  @ViewChild(ToastContainerDirective, { static: true })
  toastContainer: ToastContainerDirective;

  constructor(
    private readonly store: Store<fromDeviceList.State>,
    private readonly toastrService: ToastrService
  ) {}

  ngOnInit(): void {
    this.toastrService.overlayContainer = this.toastContainer;
    this.store.dispatch(fromDeviceList.Actions.loadDeviceList());
  }
}
