import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemberPageComponent } from './member.page';

const routes: Routes = [
  {
    path: '',
    component: MemberPageComponent,
    children: [
      { path: '', redirectTo: 'plants', pathMatch: 'full' },
      {
        path: 'notifications',
        data: {
          name: 'Notifications'
        },
        loadChildren: () =>
          import('./modules/notifications/notifications.module').then(
            m => m.NotificationsModule
          )
      },
      {
        path: 'plants',
        data: {
          name: 'Plants'
        },
        loadChildren: () =>
          import('./modules/plants/plants.module').then(m => m.PlantsModule)
      },
      {
        path: 'device',
        data: {
          name: 'Device'
        },
        loadChildren: () =>
          import('./modules/device/device.module').then(m => m.DeviceModule)
      },
      {
        path: 'connection',
        data: {
          name: 'Connection',
          showReturn: true
        },
        loadChildren: () =>
          import('./modules/connection/connection.module').then(
            m => m.ConnectionModule
          )
      },
      {
        path: 'profile',
        data: {
          name: 'Profile'
        },
        loadChildren: () =>
          import('./modules/profile/profile.module').then(m => m.ProfileModule)
      },
      {
        path: 'calendar',
        data: {
          name: 'Calendar'
        },
        loadChildren: () =>
          import('./modules/calendar/calendar.module').then(
            m => m.CalendarModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule {}
