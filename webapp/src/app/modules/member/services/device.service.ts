import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { arrayRequest } from '@core/http-helpers/array-request';
import { chainRequest } from '@core/http-helpers/chain-request';
import { HourRange } from '@core/models/hour-range.model';
import { Time } from '@core/models/time';
import { mockedDevices } from '@device/mocks/devices.mock';
import { mockedSettings } from '@device/mocks/settings.mock';
import {
  ApiModuleSettings,
  ModuleSettings
} from '@device/models/device-settings.model';
import { Device } from '@device/models/device.model';
import { HydroModule } from '@device/models/hydro-module.model';
import { Slot } from '@device/models/slot.model';
import {
  mockedModuleMeasure,
  mockedSystemMeasure
} from '@member/modules/device/mocks/measure.mock';
import {
  fabricateSlots,
  fillSlotWithMockedPlant
} from '@member/modules/device/mocks/slots.mock';
import {
  ModuleMeasure,
  SystemMeasure
} from '@member/modules/device/models/measure.model';
import { mockedPlants } from '@plants/mocks';
import { cloneDeep } from 'lodash';
import { Observable, of } from 'rxjs';
import { catchError, delay, map } from 'rxjs/operators';
import { PlantService } from './plant.service';

export abstract class DeviceService {
  abstract getSystems(): Observable<Device[]>;
  abstract fillDeviceListWithPlants(deviceList: Device[]): Observable<Device[]>;
  abstract addPlantToSlot(
    systemId: string,
    moduleId: string,
    slotId: string,
    plantId: string
  ): Observable<Slot>;
  abstract addModule(
    systemId: string,
    moduleName: string
  ): Observable<HydroModule>;
  abstract deleteModule(systemId: string, moduleId: string): Observable<void>;
  abstract getModuleSettings(
    moduleId: string,
    shift: { hours: number; minutes: number }
  ): Observable<ModuleSettings>;
  abstract setModuleSettings(
    systemId: string,
    moduleId: string,
    range: HourRange,
    halfLight: boolean,
    shift: { hours: number; minutes: number }
  ): Observable<void>;
  abstract clearPlant(
    systemId: string,
    moduleId: string,
    slotId: string
  ): Observable<void>;
  abstract getSystemMeasures(systemId: string): Observable<SystemMeasure>;
  abstract getModuleMeasure(moduleId: string): Observable<ModuleMeasure>;
}
@Injectable()
export class MockDeviceService extends DeviceService {
  getSystems(): Observable<Device[]> {
    return of(cloneDeep(mockedDevices)).pipe(delay(300));
  }

  fillDeviceListWithPlants(_deviceList: Device[]): Observable<Device[]> {
    mockedDevices.forEach(device =>
      device.modules.forEach(
        hydroModule =>
          (hydroModule.slots = hydroModule.slots.map(fillSlotWithMockedPlant))
      )
    );

    return of(cloneDeep(mockedDevices)).pipe(delay(300));
  }

  addPlantToSlot(
    systemId: string,
    moduleId: string,
    slotId: string,
    plantId: string
  ): Observable<Slot> {
    const mockedModules = mockedDevices.find(device => device.id === systemId)
      .modules;
    const mockedSlots = mockedModules.find(
      hydroModule => hydroModule.id === moduleId
    ).slots;
    const newSlot = mockedSlots.find(slot => slot.id === slotId);
    const plantToAdd = mockedPlants.find(plant => plant.id === plantId);
    newSlot.plant = plantToAdd;
    newSlot.plantId = plantId;
    newSlot.plantPlantedTime = Date();

    return of(cloneDeep(newSlot));
  }

  addModule(systemId: string, moduleName: string): Observable<HydroModule> {
    const mockedModules = mockedDevices.find(device => device.id === systemId)
      .modules;

    const i =
      mockedModules
        .map(el => parseInt(el.id, 10))
        .reduce((prev, cur) => (cur > prev ? cur : prev), 0) + 1;

    const id = (parseInt(systemId, 10) * 100 + i).toString();

    const hydroModule = {
      id,
      moduleName,
      systemId,
      slots: fabricateSlots(id)
    };

    mockedModules.push(hydroModule);

    return of(cloneDeep(hydroModule));
  }

  deleteModule(systemId: string, moduleId: string): Observable<void> {
    const mockedDevice = mockedDevices.find(device => device.id === systemId);
    const index = mockedDevice.modules.findIndex(
      hydroModule => hydroModule.id === moduleId
    );
    mockedDevice.modules.splice(index, 1);

    return of(null);
  }

  getModuleSettings(
    _moduleId: string,
    _shift: { hours: number; minutes: number }
  ): Observable<ModuleSettings> {
    return of(mockedSettings);
  }

  setModuleSettings(
    _systemId: string,
    _moduleId: string,
    _range: HourRange,
    _halflight: boolean,
    shift: { hours: number; minutes: number }
  ): Observable<void> {
    return of(null);
  }

  clearPlant(
    systemId: string,
    moduleId: string,
    slotId: string
  ): Observable<void> {
    const mockedModules = mockedDevices.find(device => device.id === systemId)
      .modules;
    const mockedSlots = mockedModules.find(
      hydroModule => hydroModule.id === moduleId
    ).slots;
    const mockedSlot = mockedSlots.find(slot => slot.id === slotId);
    mockedSlot.plant = null;
    mockedSlot.plantId = null;
    mockedSlot.plantPlantedTime = null;

    return of(null);
  }

  getSystemMeasures(_systemId: string): Observable<SystemMeasure> {
    return of(mockedSystemMeasure);
  }

  getModuleMeasure(_moduleId: string): Observable<ModuleMeasure> {
    return of(mockedModuleMeasure);
  }
}

@Injectable()
export class RestDeviceService extends DeviceService {
  constructor(
    private readonly http: HttpClient,
    private readonly plantService: PlantService
  ) {
    super();
  }

  getSystems(): Observable<Device[]> {
    const url = '/systems/';

    return this.http.get<Device[]>(url);
  }

  fillDeviceListWithPlants(deviceList: Device[]): Observable<Device[]> {
    return arrayRequest(deviceList, this.fillDeviceWithPlants.bind(this));
  }

  fillDeviceWithPlants(device: Device): Observable<Device> {
    return chainRequest(
      device,
      'modules',
      this.fillModuleWithPlants.bind(this)
    );
  }

  fillModuleWithPlants(deviceModule: HydroModule): Observable<HydroModule> {
    return chainRequest(
      deviceModule,
      'slots',
      this.fillSlotWithPlant.bind(this)
    );
  }

  fillSlotWithPlant(slot: Slot): Observable<Slot> {
    return slot.plantId
      ? this.plantService
          .getPlant(slot.plantId)
          .pipe(map(data => ({ ...slot, plant: data })))
      : of(slot);
  }

  addPlantToSlot(
    systemId: string,
    moduleId: string,
    slotId: string,
    plantId: string
  ): Observable<Slot> {
    const url = `/systems/${systemId}/modules/${moduleId}/slot/${slotId}`;

    return this.http.put<Slot>(url, null, { params: { plantId } });
  }

  addModule(systemId: string, moduleName: string): Observable<HydroModule> {
    const url = `/systems/${systemId}/modules`;

    return this.http.post<HydroModule>(url, { name: moduleName, systemId });
  }

  deleteModule(systemId: string, moduleId: string): Observable<void> {
    const url = `/systems/${systemId}/modules/${moduleId}`;

    return this.http.delete<null>(url);
  }

  addTime(
    time: { hours: number; minutes: number },
    shift: { hours: number; minutes: number }
  ): Time {
    const hours =
      (time.hours +
        shift.hours +
        Math.floor((time.minutes + shift.minutes) / 60) +
        24) %
      24;

    const minutes = (time.minutes + shift.minutes) % 60;

    return new Time(hours, minutes, null);
  }

  substractTime(
    time: { hours: number; minutes: number },
    shift: { hours: number; minutes: number }
  ): Time {
    const hours =
      ((time.minutes < shift.minutes
        ? time.hours - Math.abs(shift.hours) - 1
        : time.hours - Math.abs(shift.hours)) +
        24) %
      24;

    const minutes =
      time.minutes < shift.minutes
        ? time.minutes - shift.minutes + 60
        : time.minutes - shift.minutes;

    return new Time(hours, minutes, null);
  }

  getModuleSettings(
    moduleId: string,
    shift: { hours: number; minutes: number }
  ): Observable<ModuleSettings> {
    const url = `/settings/module/${moduleId}`;

    return this.http.get<ApiModuleSettings>(url).pipe(
      map(obj => {
        const from =
          shift.hours > 0
            ? this.addTime(
                { hours: obj.fromHourUTC, minutes: obj.fromMinuteUTC },
                shift
              )
            : this.substractTime(
                { hours: obj.fromHourUTC, minutes: obj.fromMinuteUTC },
                shift
              );

        const to =
          shift.hours > 0
            ? this.addTime(
                { hours: obj.toHourUTC, minutes: obj.toMinuteUTC },
                shift
              )
            : this.substractTime(
                { hours: obj.toHourUTC, minutes: obj.toHourUTC },
                shift
              );

        return {
          halfLight: obj.halfLight,
          moduleId: obj.moduleId,
          range: {
            from,
            to
          }
        };
      })
    );
  }

  clearPlant(
    systemId: string,
    moduleId: string,
    slotId: string
  ): Observable<void> {
    const url = `/systems/${systemId}/modules/${moduleId}/slot/${slotId}`;

    return this.http.delete<null>(url);
  }

  setModuleSettings(
    systemId: string,
    moduleId: string,
    range: HourRange,
    halfLight: boolean,
    shift: { hours: number; minutes: number }
  ): Observable<void> {
    const from =
      shift.hours > 0
        ? this.addTime(
            { hours: range.from.hours, minutes: range.from.minutes },
            shift
          )
        : this.substractTime(
            { hours: range.from.hours, minutes: range.from.minutes },
            shift
          );

    const to =
      shift.hours > 0
        ? this.substractTime(
            { hours: range.to.hours, minutes: range.to.minutes },
            shift
          )
        : this.addTime(
            { hours: range.to.hours, minutes: range.to.minutes },
            shift
          );

    const settings: ApiModuleSettings = {
      fromHourUTC: from.hours,
      fromMinuteUTC: from.minutes,
      halfLight,
      moduleId,
      toHourUTC: to.hours,
      toMinuteUTC: to.minutes
    };

    const url = `/settings/system/${systemId}`;

    return this.http.post<null>(url, settings);
  }

  getSystemMeasures(systemId: string): Observable<SystemMeasure> {
    const url = `/measures/system/${systemId}?onlyLast=true`;

    return this.http.get<SystemMeasure>(url).pipe(catchError(() => of(null)));
  }

  getModuleMeasure(moduleId: string): Observable<ModuleMeasure> {
    const url = `/measures/module/${moduleId}?onlyLast=true`;

    return this.http.get<ModuleMeasure>(url).pipe(catchError(() => of(null)));
  }
}
