import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { mockedUserProfile } from '@profile/mocks/profile.mock';
import {
  APIUserDetails,
  APIUserProfile,
  UserDetails,
  UserProfile
} from '@profile/models';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

export abstract class ProfileService {
  abstract getUserProfile(id: string): Observable<UserProfile>;
  abstract updateUserProfile(
    id: string,
    userDetails: UserDetails
  ): Observable<UserDetails>;
}
@Injectable()
export class MockProfileService extends ProfileService {
  getUserProfile(_id: string): Observable<UserProfile> {
    return of(mockedUserProfile);
  }

  updateUserProfile(
    _id: string,
    userDetails: UserDetails
  ): Observable<UserDetails> {
    return of(userDetails);
  }
}

@Injectable()
export class RestProfileService extends ProfileService {
  constructor(private readonly http: HttpClient) {
    super();
  }

  getUserProfile(id: string): Observable<UserProfile> {
    const url = `/clients/${id}`;

    return this.http.get<APIUserProfile>(url).pipe(
      map(settings => {
        const timezoneShift = {
          hours:
            settings.clientDetailsTO.timezoneShift > 0
              ? Math.floor(settings.clientDetailsTO.timezoneShift / 100)
              : Math.ceil(settings.clientDetailsTO.timezoneShift / 100),
          minutes: Math.abs(settings.clientDetailsTO.timezoneShift % 100)
        };

        const userDetails: UserDetails = {
          firstName: settings.clientDetailsTO.firstName,
          lastName: settings.clientDetailsTO.lastName,
          timezoneShift
        };

        return { ...settings, clientDetailsTO: userDetails };
      })
    );
  }

  updateUserProfile(
    id: string,
    userDetails: UserDetails
  ): Observable<UserDetails> {
    const url = `/clients/${id}`;

    const apiTimezoneShift =
      userDetails.timezoneShift.hours.toString() +
      (userDetails.timezoneShift.minutes === 0
        ? '00'
        : userDetails.timezoneShift.minutes.toString());

    return this.http
      .put<APIUserDetails>(url, {
        ...userDetails,
        timezoneShift: apiTimezoneShift
      })
      .pipe(
        map(clientDetails => {
          const timezoneShift = {
            hours: Math.floor(clientDetails.timezoneShift / 100),
            minutes: Math.abs(clientDetails.timezoneShift % 100)
          };

          return { ...clientDetails, timezoneShift };
        })
      );
  }
}
