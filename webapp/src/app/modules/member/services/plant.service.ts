import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { mockedPlants } from '@plants/mocks/plants.mock';
import { Plant } from '@plants/models/plant.model';
import { Observable, of } from 'rxjs';

export abstract class PlantService {
  abstract getPlantsList(): Observable<Plant[]>;
  abstract getPlant(id: string): Observable<Plant>;
}
@Injectable()
export class MockPlantService extends PlantService {
  getPlantsList(): Observable<Plant[]> {
    return of(mockedPlants);
  }

  getPlant(id: string): Observable<Plant> {
    const mockedPlant = mockedPlants.find(plant => plant.id === id);

    return of(mockedPlant);
  }
}

@Injectable()
export class RestPlantService extends PlantService {
  constructor(private readonly http: HttpClient) {
    super();
  }

  getPlantsList(): Observable<Plant[]> {
    const url = '/plants';

    return this.http.get<Plant[]>(url);
  }

  getPlant(id: string): Observable<Plant> {
    const url = `/plants/${id}`;

    return this.http.get<Plant>(url);
  }
}
