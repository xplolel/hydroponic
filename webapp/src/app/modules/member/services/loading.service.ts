import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map, scan, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private readonly _targets = new BehaviorSubject<[string, boolean]>(null);
  readonly targets$ = this._targets.asObservable().pipe(
    scan(
      (acc, [key, loading]) =>
        loading ? [...acc, key] : acc.filter(kokos => kokos !== key),
      new Array<string>()
    ),
    share()
  );

  any$ = this.targets$.pipe(map(arr => !!arr.length));

  start(key: string): void {
    this._targets.next([key, true]);
  }

  stop(key: string): void {
    this._targets.next([key, false]);
  }
}
