import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { mockedNotifications } from '@notifications/mocks/notifications.mock';
import { CNotification } from '@notifications/models/notification.model';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

export abstract class NotificationService {
  abstract getNotificationList(): Observable<CNotification[]>;
  abstract markAsRead(id: string): Observable<void>;
  abstract register(payload: PushSubscriptionJSON): Observable<void>;
}
@Injectable()
export class MockNotificationService extends NotificationService {
  getNotificationList(): Observable<CNotification[]> {
    return of(mockedNotifications);
  }

  markAsRead(_id: string): Observable<void> {
    return of(null);
  }

  register(_payload: PushSubscriptionJSON): Observable<void> {
    return of(null);
  }
}

@Injectable()
export class RestNotificationService extends NotificationService {
  constructor(private readonly http: HttpClient) {
    super();
  }

  getNotificationList(): Observable<CNotification[]> {
    const url = '/notifications';

    return this.http.get<CNotification[]>(url).pipe(
      map(notifications =>
        notifications.map(notification => ({
          ...notification,
          created: new Date(notification.created)
        }))
      )
    );
  }

  markAsRead(id: string): Observable<void> {
    const url = `/notifications/${id}`;

    return this.http.patch<null>(url, {});
  }

  register(payload: PushSubscriptionJSON): Observable<void> {
    const url = '/notifications-subscribe';

    return this.http.post<null>(url, payload);
  }
}
