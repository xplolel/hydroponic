import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { mapTo, switchMap } from 'rxjs/operators';

export enum OrderType {
  half_on = 'HALF_LIGHT_ON',
  half_off = 'HALF_LIGHT_OFF',
  full_on = 'FULL_LIGHT_ON',
  full_off = 'FULL_LIGHT_OFF'
}
export interface LightState {
  moduleId: string;
  lightOn: boolean;
  halfLight: boolean;
}

export interface Order {
  moduleId: string;
  orderType: OrderType;
  shortNote: string;
}

const getOrderType = (lightState: LightState) => {
  if (lightState.lightOn) {
    return lightState.halfLight ? OrderType.half_off : OrderType.full_off;
  }

  return lightState.halfLight ? OrderType.half_on : OrderType.full_on;
};

export abstract class OrderService {
  abstract getLightState(moduleId: string): Observable<LightState>;
  abstract order(moduleId: string, lightState: LightState): Observable<void>;

  toggleLight(moduleId: string): Observable<LightState> {
    return this.getLightState(moduleId).pipe(
      switchMap(lightState =>
        this.order(moduleId, lightState).pipe(mapTo(lightState))
      )
    );
  }
}
@Injectable()
export class MockOrderService extends OrderService {
  getLightState(_moduleId: string): Observable<LightState> {
    return of({
      moduleId: '123',
      lightOn: Math.random() > 0.5,
      halfLight: Math.random() > 0.5
    });
  }

  order(_moduleId: string, _lightState: LightState): Observable<void> {
    return of(null);
  }
}

@Injectable()
export class RestOrderService extends OrderService {
  constructor(private readonly http: HttpClient) {
    super();
  }

  getLightState(moduleId: string): Observable<LightState> {
    const url = `/lightState/${moduleId}`;

    return this.http.get<LightState>(url);
  }

  order(moduleId: string, lightState: LightState): Observable<void> {
    const url = '/orders';

    return this.http.post<null>(url, {
      moduleId,
      orderType: getOrderType(lightState),
      shortNote: 'empty'
    });
  }
}
