import { UserProfile } from '../models';

export const mockedUserProfile: UserProfile = {
  id: '3',
  email: 'test@test.pl',
  clientDetailsTO: {
    firstName: 'Katarzyna',
    lastName: 'Kwiatkowska',
    timezoneShift: { hours: 4, minutes: 0 }
  }
};
