import { Component, OnDestroy, OnInit } from '@angular/core';
import * as fromAuth from '@auth/store';
import * as fromProfile from '@member/store/features/profile';
import { select, Store } from '@ngrx/store';
import { SvgIconTypes } from '@ui/models/icon.model';
import { cloneDeep } from 'lodash';
import { Subscription } from 'rxjs';
import { filter, map, switchMap, take } from 'rxjs/operators';
import { timeZones } from './helpers/timezones';
import { TimeZone, UserProfile } from './models';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss']
})
export class ProfilePageComponent implements OnInit, OnDestroy {
  private readonly subscription: Subscription = new Subscription();
  timeZones = timeZones;
  SvgIconTypes = SvgIconTypes;

  selectedTimeZone: TimeZone;
  userProfile: UserProfile;

  private readonly user$ = this.authStore.pipe(
    select(fromAuth.Selectors.getUser())
  );

  constructor(
    private readonly authStore: Store<fromAuth.State>,
    private readonly profileStore: Store<fromProfile.State>
  ) {}

  changeTimeZone(): void {
    this.profileStore.dispatch(
      fromProfile.Actions.updateUserDetails({
        id: this.userProfile.id,
        userDetails: {
          ...this.userProfile.clientDetailsTO,
          timezoneShift: this.selectedTimeZone.shift
        }
      })
    );
  }

  cancelTimeZoneChange(): void {
    this.selectedTimeZone = this.timeZones.find(
      timeZone =>
        timeZone.shift.hours ===
          this.userProfile.clientDetailsTO.timezoneShift.hours &&
        timeZone.shift.minutes ===
          this.userProfile.clientDetailsTO.timezoneShift.minutes
    );
  }

  ngOnInit(): void {
    this.subscription.add(
      this.profileStore
        .pipe(
          select(fromProfile.Selectors.getProfile()),
          filter(profile => !profile),
          switchMap(() => this.user$.pipe(take(1))),
          map(user => user.id)
        )
        .subscribe(id => {
          this.profileStore.dispatch(fromProfile.Actions.getUser({ id }));
        })
    );

    this.subscription.add(
      this.profileStore
        .pipe(
          select(fromProfile.Selectors.getProfile()),
          filter(profile => !!profile),
          map(cloneDeep)
        )
        .subscribe(profile => {
          this.selectedTimeZone = this.timeZones.find(
            timeZone =>
              timeZone.shift.hours ===
                profile.clientDetailsTO.timezoneShift.hours &&
              timeZone.shift.minutes ===
                profile.clientDetailsTO.timezoneShift.minutes
          );
          this.userProfile = profile;
        })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  logOut(): void {
    this.authStore.dispatch(fromAuth.Actions.logout());
  }

  showingWarning(): boolean {
    return !this.selectedTimeZone;
  }
}
