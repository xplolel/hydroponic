import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ProfileRoutingModule } from './profile-routing.module';

import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { ProfilePageComponent } from './profile.page';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    UserInterfaceModule,
    NgSelectModule,
    FormsModule
  ],
  declarations: [ProfilePageComponent]
})
export class ProfileModule {}
