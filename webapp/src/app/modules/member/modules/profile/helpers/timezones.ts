import { TimeZone } from '../models';

const humanize = (val: number): string => `${val < 10 ? '0' : ''}${val}`;

const gmt = (val: { hours: number; minutes: number }): string =>
  `GMT(${val.hours < 0 ? '-' : '+'}${humanize(
    Math.floor(Math.abs(val.hours))
  )}:${humanize(val.minutes)})`;

export const timeZones: TimeZone[] = [
  { label: 'International Date Line West', shift: { hours: -12, minutes: 0 } },
  { label: 'Midway Island, Samoa', shift: { hours: -11, minutes: 0 } },
  { label: 'Hawaii', shift: { hours: -10, minutes: 0 } },
  { label: 'Alaska', shift: { hours: -9, minutes: 0 } },
  { label: 'Pacific Time (US & Canada)', shift: { hours: -8, minutes: 0 } },
  { label: 'Tijuana, Baja California', shift: { hours: -8, minutes: 0 } },
  { label: 'Arizona', shift: { hours: -7, minutes: 0 } },
  { label: 'Chihuahua, La Paz, Mazatlan', shift: { hours: -7, minutes: 0 } },
  { label: 'Mountain Time (US & Canada)', shift: { hours: -7, minutes: 0 } },
  { label: 'Central America', shift: { hours: -6, minutes: 0 } },
  { label: 'Central Time (US & Canada)', shift: { hours: -6, minutes: 0 } },
  {
    label: 'Bogota, Lima, Quito, Rio Branco',
    shift: { hours: -5, minutes: 0 }
  },
  { label: 'Eastern Time (US & Canada)', shift: { hours: -5, minutes: 0 } },
  { label: 'Indiana (East)', shift: { hours: -5, minutes: 0 } },
  { label: 'Atlantic Time (Canada)', shift: { hours: -4, minutes: 0 } },
  { label: 'Caracas, La Paz', shift: { hours: -4, minutes: 0 } },
  { label: 'Manaus', shift: { hours: -4, minutes: 0 } },
  { label: 'Santiago', shift: { hours: -4, minutes: 0 } },
  { label: 'Newfoundland', shift: { hours: -3, minutes: 30 } },
  { label: 'Brasilia', shift: { hours: -3, minutes: 0 } },
  { label: 'Buenos Aires, Georgetown', shift: { hours: -3, minutes: 0 } },
  { label: 'Greenland', shift: { hours: -3, minutes: 0 } },
  { label: 'Montevideo', shift: { hours: -3, minutes: 0 } },
  { label: 'Mid-Atlantic', shift: { hours: -2, minutes: 0 } },
  { label: 'Cape Verde Is.', shift: { hours: -1, minutes: 0 } },
  { label: 'Azores', shift: { hours: -1, minutes: 0 } },
  { label: 'Casablanca, Monrovia, Reykjavik', shift: { hours: 0, minutes: 0 } },
  {
    label: 'Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London',
    shift: { hours: 0, minutes: 0 }
  },
  {
    label: 'Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
    shift: { hours: 1, minutes: 0 }
  },
  {
    label: 'Belgrade, Bratislava, Budapest, Ljubljana, Prague',
    shift: { hours: 1, minutes: 0 }
  },
  {
    label: 'Brussels, Copenhagen, Madrid, Paris',
    shift: { hours: 1, minutes: 0 }
  },
  {
    label: 'Sarajevo, Skopje, Warsaw, Zagreb',
    shift: { hours: 1, minutes: 0 }
  },
  { label: 'West Central Africa', shift: { hours: 1, minutes: 0 } },
  { label: 'Amman', shift: { hours: 2, minutes: 0 } },
  { label: 'Athens, Bucharest, Istanbul', shift: { hours: 2, minutes: 0 } },
  { label: 'Beirut', shift: { hours: 2, minutes: 0 } },
  { label: 'Cairo', shift: { hours: 2, minutes: 0 } },
  { label: 'Harare, Pretoria', shift: { hours: 2, minutes: 0 } },
  {
    label: 'Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius',
    shift: { hours: 2, minutes: 0 }
  },
  { label: 'Jerusalem', shift: { hours: 2, minutes: 0 } },
  { label: 'Minsk', shift: { hours: 2, minutes: 0 } },
  { label: 'Windhoek', shift: { hours: 2, minutes: 0 } },
  { label: 'Kuwait, Riyadh, Baghdad', shift: { hours: 3, minutes: 0 } },
  {
    label: 'Moscow, St. Petersburg, Volgograd',
    shift: { hours: 3, minutes: 0 }
  },
  { label: 'Nairobi', shift: { hours: 3, minutes: 0 } },
  { label: 'Tbilisi', shift: { hours: 3, minutes: 0 } },
  { label: 'Tehran', shift: { hours: 3, minutes: 30 } },
  { label: 'Abu Dhabi, Muscat', shift: { hours: 4, minutes: 0 } },
  { label: 'Baku', shift: { hours: 4, minutes: 0 } },
  { label: 'Yerevan', shift: { hours: 4, minutes: 0 } },
  { label: 'Kabul', shift: { hours: 4, minutes: 30 } },
  { label: 'Yekaterinburg', shift: { hours: 5, minutes: 0 } },
  { label: 'Islamabad, Karachi, Tashkent', shift: { hours: 5, minutes: 0 } },
  { label: 'Sri Jayawardenapura', shift: { hours: 5, minutes: 30 } },
  {
    label: 'Chennai, Kolkata, Mumbai, New Delhi',
    shift: { hours: 5, minutes: 30 }
  },
  { label: 'Kathmandu', shift: { hours: 5, minutes: 45 } },
  { label: 'Almaty, Novosibirsk', shift: { hours: 6, minutes: 0 } },
  { label: 'Astana, Dhaka', shift: { hours: 6, minutes: 0 } },
  { label: 'Yangon (Rangoon)', shift: { hours: 6, minutes: 30 } },
  { label: 'Bangkok, Hanoi, Jakarta', shift: { hours: 7, minutes: 0 } },
  { label: 'Krasnoyarsk', shift: { hours: 7, minutes: 0 } },
  {
    label: 'Beijing, Chongqing, Hong Kong, Urumqi',
    shift: { hours: 8, minutes: 0 }
  },
  { label: 'Kuala Lumpur, Singapore', shift: { hours: 8, minutes: 0 } },
  { label: 'Irkutsk, Ulaan Bataar', shift: { hours: 8, minutes: 0 } },
  { label: 'Perth', shift: { hours: 8, minutes: 0 } },
  { label: 'Taipei', shift: { hours: 8, minutes: 0 } },
  { label: 'Osaka, Sapporo, Tokyo', shift: { hours: 9, minutes: 0 } },
  { label: 'Seoul', shift: { hours: 9, minutes: 0 } },
  { label: 'Yakutsk', shift: { hours: 9, minutes: 0 } },
  { label: 'Adelaide', shift: { hours: 9, minutes: 30 } },
  { label: 'Darwin', shift: { hours: 9, minutes: 30 } },
  { label: 'Brisbane', shift: { hours: 10, minutes: 0 } },
  { label: 'Canberra, Melbourne, Sydney', shift: { hours: 10, minutes: 0 } },
  { label: 'Hobart', shift: { hours: 10, minutes: 0 } },
  { label: 'Guam, Port Moresby', shift: { hours: 10, minutes: 0 } },
  { label: 'Vladivostok', shift: { hours: 10, minutes: 0 } },
  {
    label: 'Magadan, Solomon Is., New Caledonia',
    shift: { hours: 11, minutes: 0 }
  },
  { label: 'Auckland, Wellington', shift: { hours: 12, minutes: 0 } },
  { label: 'Fiji, Kamchatka, Marshall Is.', shift: { hours: 12, minutes: 0 } },
  { label: "Nuku'alofa", shift: { hours: 13, minutes: 0 } }
]
  .reduce((res, cur) => {
    const index = res.findIndex(
      el =>
        el.shift.hours === cur.shift.hours &&
        el.shift.minutes === cur.shift.minutes
    );
    if (index === -1) {
      return [...res, cur];
    }
    res[index].label += `, ${cur.label}`;

    return res;
  }, [])
  .map(el => ({ ...el, label: `${gmt(el.shift)} ${el.label}` }));
