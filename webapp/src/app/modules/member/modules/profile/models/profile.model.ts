export class UserProfile {
  id: string;
  email: string;
  clientDetailsTO: UserDetails;
}

export class UserDetails {
  firstName: string | null;
  lastName: string | null;
  timezoneShift: { hours: number; minutes: number } | null;
}

export class APIUserProfile {
  id: string;
  email: string;
  clientDetailsTO: APIUserDetails;
}

export class APIUserDetails {
  firstName: string | null;
  lastName: string | null;
  timezoneShift: number | null;
}
