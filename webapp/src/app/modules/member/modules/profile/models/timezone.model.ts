export interface TimeZone {
  label: string;
  shift: {
    hours: number;
    minutes: number;
  };
}
