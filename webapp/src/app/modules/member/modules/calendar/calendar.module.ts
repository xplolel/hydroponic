import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { environment } from '@env/environment';
import {
  DeviceService,
  MockDeviceService,
  RestDeviceService
} from '@member/services/device.service';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarPageComponent } from './calendar.page';
import { SlotPlacementComponent } from './components/slot-placement/slot-placement.component';

@NgModule({
  imports: [CommonModule, CalendarRoutingModule, UserInterfaceModule],
  declarations: [CalendarPageComponent, SlotPlacementComponent],
  providers: [
    {
      provide: DeviceService,
      useClass: environment.useMocks ? MockDeviceService : RestDeviceService
    }
  ]
})
export class CalendarModule {}
