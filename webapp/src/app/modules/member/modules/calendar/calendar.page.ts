import { Component, OnInit } from '@angular/core';
import * as fromDeviceList from '@member/store/features/device-list';
import { select, Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { Slot } from '../device/models/slot.model';

@Component({
  selector: 'app-calendar-page',
  templateUrl: './calendar.page.html',
  styleUrls: ['./calendar.page.scss']
})
export class CalendarPageComponent implements OnInit {
  slots$ = this.store.pipe(
    select(fromDeviceList.Selectors.getDeviceList()),
    map(devices =>
      devices
        .reduce(
          (slots, device) => [
            ...slots,
            ...device.modules.reduce(
              (moduleSlots, hydroModule) => [
                ...moduleSlots,
                ...hydroModule.slots
                  .filter(slot => slot.plant)
                  .map(slot => ({
                    ...slot,
                    moduleName: hydroModule.moduleName
                  }))
              ],
              [] as Slot[]
            )
          ],
          [] as Slot[]
        )
        .sort(
          (slota, slotb) => this.getDaysLeft(slota) - this.getDaysLeft(slotb)
        )
    )
  );

  constructor(private readonly store: Store<fromDeviceList.State>) {}

  getBtnClass(slot: Slot): string {
    return this.getDaysLeft(slot) > 0 ? 'btn-gradient-2' : 'btn-red';
  }

  getDaysLeft(slot: Slot): number {
    const oneDay = 24 * 60 * 60 * 1000;
    const firstDate = new Date();
    const secondDate = new Date(slot.plantPlantedTime);

    const diffDays = Math.round(
      Math.abs((firstDate.getTime() - secondDate.getTime()) / oneDay)
    );

    return slot.plant.growTime - diffDays;
  }

  ngOnInit(): void {
    this.store.dispatch(fromDeviceList.Actions.fillDeviceListWithPlants());
  }
}
