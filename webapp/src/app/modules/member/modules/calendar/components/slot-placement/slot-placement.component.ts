import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-slot-placement',
  templateUrl: './slot-placement.component.html',
  styleUrls: ['./slot-placement.component.scss']
})
export class SlotPlacementComponent {
  _x = 0;
  _y = 0;

  @Input() set x(val: number) {
    this._x = val;
  }

  @Input() set y(val: number) {
    this._y = val;
  }

  isActive(x: number, y: number): boolean {
    return x === this._x && y === this._y;
  }
}
