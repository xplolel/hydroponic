import { CNotification } from '../models/notification.model';

export const mockedNotifications: CNotification[] = [
  {
    id: '1',
    read: false,
    created: new Date(),
    notificationType: 'INFO',
    notificationType2: 'WATER',
    longNote: 'Uzupełnij zbiornik z wodą.',
    shortNote: 'Pusty zbiornik',
    systemId: 'string'
  },
  {
    id: '2',
    created: new Date(),
    read: false,
    notificationType: 'TAKE_ACTION',
    notificationType2: 'CLEANING',
    longNote: 'Wyczyść zbiornik z nawozem.',
    shortNote: 'Pusty zbiornik',
    systemId: 'string'
  },
  {
    id: '3',
    created: new Date(),
    read: true,
    notificationType: 'FAILURE',
    notificationType2: '',
    longNote: 'Nie można połączyć z urządzeniem.',
    shortNote: 'Pusty zbiornik',
    systemId: 'string'
  },
  {
    id: '4',
    created: new Date(),
    read: false,
    notificationType: 'INFO',
    notificationType2: 'HARVESTING',
    longNote: 'Masz rośliny gotowe do zebrania.',
    shortNote: 'Pusty zbiornik',
    systemId: 'string'
  }
];
