import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { NotificationsRoutingModule } from './notifications-routing.module';
import { NotificationsPageComponent } from './notifications.page';
import { NotificationListEffects } from './store/effects';
import * as fromNotifications from './store/reducers';

@NgModule({
  imports: [
    CommonModule,
    NotificationsRoutingModule,
    UserInterfaceModule,
    StoreModule.forFeature(
      fromNotifications.featureKey,
      fromNotifications.reducers
    ),
    EffectsModule.forFeature([NotificationListEffects])
  ],
  declarations: [NotificationsPageComponent]
})
export class NotificationsModule {}
