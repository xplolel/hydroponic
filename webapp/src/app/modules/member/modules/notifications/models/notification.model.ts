export class CNotification {
  id: string;
  read: boolean;
  created: Date;
  longNote: string;
  notificationType: string;
  notificationType2: string;
  shortNote: string;
  systemId: string;
}
