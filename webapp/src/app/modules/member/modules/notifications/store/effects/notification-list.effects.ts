import { Injectable } from '@angular/core';
import { NotificationService } from '@member/services/notification.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { NotificationListActions } from '../actions';

@Injectable()
export class NotificationListEffects {
  register$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(NotificationListActions.register),
        switchMap(({ payload }) => this.notificationService.register(payload))
      ),
    { dispatch: false }
  );

  loadNotifications$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NotificationListActions.loadNotificationList),
      switchMap(() =>
        this.notificationService.getNotificationList().pipe(
          map(notivications =>
            NotificationListActions.loadNotificationListSuccess({
              payload: notivications
            })
          ),
          catchError(error =>
            of(
              NotificationListActions.loadNotificationListFailure({
                error
              })
            )
          )
        )
      )
    )
  );

  markAsRead$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NotificationListActions.markasRead),
      switchMap(action =>
        this.notificationService.markAsRead(action.payload).pipe(
          map(() =>
            NotificationListActions.markasReadSuccess({
              payload: action.payload
            })
          ),
          catchError(error =>
            of(
              NotificationListActions.markasReadFailure({
                error
              })
            )
          )
        )
      )
    )
  );

  constructor(
    private readonly actions$: Actions,
    private readonly notificationService: NotificationService
  ) {}
}
