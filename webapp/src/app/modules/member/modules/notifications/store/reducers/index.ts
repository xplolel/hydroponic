import {
  Action,
  combineReducers,
  createFeatureSelector,
  createSelector
} from '@ngrx/store';
import * as fromNotificationList from './notification-list.reducers';

export const featureKey = 'notifications';

export interface NotificationsState {
  [fromNotificationList.featureKey]: fromNotificationList.State;
}

export interface State {
  [featureKey]: NotificationsState;
}

export function reducers(
  state: NotificationsState | undefined,
  action: Action
): NotificationsState {
  return combineReducers({
    [fromNotificationList.featureKey]: fromNotificationList.reducer
  })(state, action);
}

export const selectNotificationsState = createFeatureSelector<
  State,
  NotificationsState
>(featureKey);

export const selectNotificationsNotificationListState = createSelector(
  selectNotificationsState,
  (state: NotificationsState) => state[fromNotificationList.featureKey]
);

export const getNotificationList = () =>
  createSelector(
    selectNotificationsNotificationListState,
    fromNotificationList.getNotificationList
  );
