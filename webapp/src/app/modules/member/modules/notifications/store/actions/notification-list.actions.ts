import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { CNotification } from '../../models/notification.model';

export const register = createAction(
  '[NOTIFICATION_LIST] register',
  props<{ payload: PushSubscriptionJSON }>()
);

export const loadNotificationList = createAction(
  '[NOTIFICATION_LIST] load notification list'
);

export const loadNotificationListSuccess = createAction(
  '[NOTIFICATION_LIST] load notification list success',
  props<{ payload: CNotification[] }>()
);

export const loadNotificationListFailure = createAction(
  '[NOTIFICATION_LIST] load notification list failure',
  props<{ error: HttpErrorResponse }>()
);

export const markasRead = createAction(
  '[NOTIFICATION_LIST] mark as read',
  props<{ payload: string }>()
);

export const markasReadSuccess = createAction(
  '[NOTIFICATION_LIST] mark as read success',
  props<{ payload: string }>()
);

export const markasReadFailure = createAction(
  '[NOTIFICATION_LIST] mark as read failure',
  props<{ error: HttpErrorResponse }>()
);
