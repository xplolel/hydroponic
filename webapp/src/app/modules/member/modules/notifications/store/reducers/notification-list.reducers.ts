import { createReducer, on } from '@ngrx/store';
import { CNotification } from '../../models/notification.model';
import { NotificationListActions } from '../actions';

export const featureKey = 'notificationList';

export interface State {
  notifications: CNotification[];
}

export const initialState: State = {
  notifications: []
};

export const reducer = createReducer(
  initialState,
  on(
    NotificationListActions.loadNotificationListSuccess,
    (state, { payload }) => ({
      ...state,
      notifications: payload
    })
  ),
  on(NotificationListActions.markasReadSuccess, (state, { payload }) => {
    const notificationIndex = state.notifications
      .map(notification => notification.id)
      .indexOf(payload);

    const newNotifications = [...state.notifications];

    const newNotification = { ...newNotifications[notificationIndex] };
    newNotification.read = true;
    newNotifications[notificationIndex] = newNotification;

    return {
      ...state,
      notifications: newNotifications
    };
  })
);

export const getNotificationList = (state: State) => state.notifications;
