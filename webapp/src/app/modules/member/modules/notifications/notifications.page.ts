import { Component, OnInit } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { select, Store } from '@ngrx/store';
import { SvgIconTypes } from '@ui/models/icon.model';
import { CNotification } from './models/notification.model';
import { NotificationListActions } from './store/actions';
import * as fromNotifications from './store/reducers';

@Component({
  selector: 'app-notifications-page',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss']
})
export class NotificationsPageComponent implements OnInit {
  readonly VAPID_PUBLIC_KEY =
    'BI3s9oxc3a2IcA82qjxygUHbngVFy0e_KrDemCdKqPSJ5zXjKlBDQPoy7qYFMswC7BYY2uWQtFhaCEEkRdpXIOY';

  SvgIconTypes = SvgIconTypes;

  notifications$ = this.store.pipe(
    select(fromNotifications.getNotificationList())
  );

  constructor(
    private readonly store: Store<fromNotifications.State>,
    private readonly swPush: SwPush
  ) {}

  markAsRead(id: string): void {
    this.store.dispatch(
      NotificationListActions.markasRead({
        payload: id
      })
    );
  }

  getIcon(type: string): SvgIconTypes {
    switch (type) {
      case 'CLEANING':
        return SvgIconTypes.clean;
      case 'WATER':
        return SvgIconTypes.water;
      case 'HARVESTING':
        return SvgIconTypes.plant;
      default:
        return SvgIconTypes.plant;
    }
  }

  getColor(type: string): string {
    switch (type) {
      case 'INFO':
        return 'blue';
      case 'TAKE_ACTION':
        return 'yellow';
      case 'FAILURE':
        return 'red';
      default:
        return 'green';
    }
  }

  getBtnClass(type: string): string {
    return `btn-${this.getColor(type)}-white-accent`;
  }

  getDate(notification: CNotification): string {
    const date = notification.created;

    return date.toLocaleDateString();
  }

  ngOnInit(): void {
    this.store.dispatch(NotificationListActions.loadNotificationList());

    if (this.swPush.isEnabled) {
      this.swPush
        .requestSubscription({
          serverPublicKey: this.VAPID_PUBLIC_KEY
        })
        .then(sub => {
          this.store.dispatch(
            NotificationListActions.register({ payload: sub.toJSON() })
          );

          // tslint:disable-next-line: no-console
          console.log(
            'Subscribed to notifications',
            JSON.stringify(sub.toJSON())
          );
        })
        .catch(err =>
          console.error('Could not subscribe to notifications', err)
        );

      this.swPush.messages.subscribe(() => {
        this.store.dispatch(NotificationListActions.loadNotificationList());
      });
    }
  }
}
