import { Slot } from './slot.model';

export interface HydroModule {
  id: string;
  moduleName: string;
  systemId: string;
  slots: Slot[];
}

export enum IlluminationState {
  LEFT = 'left',
  BOTH = 'both'
}
