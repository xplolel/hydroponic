export enum WaterLevel {
  LOW,
  MEDIUM,
  FULL
}

export interface SystemMeasure {
  humidity: number;
  reportedTime: string;
  systemId: string;
  temperature: number;
}

export interface ModuleMeasure {
  moduleId: string;
  reportedTime: string;
  waterLevel: WaterLevel;
}
