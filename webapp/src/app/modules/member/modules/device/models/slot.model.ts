import { Plant } from '@plants/models';

export class Slot {
  id: string;
  plantId: string | null;
  plantPlantedTime: string | null;
  moduleId: string;
  x: number;
  y: number;
  plant?: Plant;
}
