import { HourRange } from '@core/models/hour-range.model';

export class ApiModuleSettings {
  fromHourUTC: number;
  fromMinuteUTC: number;
  halfLight: boolean;
  moduleId: string;
  toHourUTC: number;
  toMinuteUTC: number;
}

export class ModuleSettings {
  halfLight: boolean;
  moduleId: string;
  range: HourRange;
}
