import { HydroModule } from './hydro-module.model';

export class Device {
  id: string;
  ownerId: string;
  name: string;
  modules: HydroModule[];
}
