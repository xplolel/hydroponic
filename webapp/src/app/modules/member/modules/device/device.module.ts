import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { DeviceListComponent } from './components/device-list/device-list.component';
import { ModuleDetailsComponent } from './components/module-details/module-details.component';
import { ModuleIlluminationPreviewComponent } from './components/module-illumination-preview/module-illumination-preview.component';
import { DeviceRoutingModule } from './device-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DeviceRoutingModule,
    UserInterfaceModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ModuleIlluminationPreviewComponent,
    DeviceListComponent,
    ModuleDetailsComponent
  ]
})
export class DeviceModule {}
