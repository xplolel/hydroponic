import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import * as fromAuth from '@auth/store';
import { HourRange } from '@core/models/hour-range.model';
import { UserProfile } from '@member/modules/profile/models';
import * as fromDeviceList from '@member/store/features/device-list';
import * as fromModule from '@member/store/features/module';
import * as fromProfile from '@member/store/features/profile';
import { select, Store } from '@ngrx/store';
import { SvgIconTypes } from '@ui/models/icon.model';
import { cloneDeep } from 'lodash';
import { combineLatest, Subscription } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { IlluminationState } from '../../models/hydro-module.model';

@Component({
  selector: 'app-module-details',
  templateUrl: './module-details.component.html',
  styleUrls: ['./module-details.component.scss']
})
export class ModuleDetailsComponent implements OnInit, OnDestroy {
  moduleId$ = this.activeRoute.paramMap.pipe(
    map((params: ParamMap) => params.get('moduleId'))
  );

  module$ = this.moduleId$.pipe(
    switchMap(id =>
      this.store.pipe(
        select(fromDeviceList.Selectors.getModuleById(id)),
        map(cloneDeep)
      )
    )
  );

  settings$ = this.store.pipe(
    select(fromModule.Selectors.getSettings()),
    filter(val => val !== null),
    map(cloneDeep),
    tap(settings => {
      this.state = settings.halfLight
        ? IlluminationState.LEFT
        : IlluminationState.BOTH;
    })
  );

  profile$ = this.profileStore.pipe(
    select(fromProfile.Selectors.getProfile()),
    map(cloneDeep)
  );

  state: IlluminationState;

  subscription = new Subscription();

  SvgIconTypes = SvgIconTypes;

  constructor(
    private readonly store: Store<fromModule.State>,
    private readonly activeRoute: ActivatedRoute,
    private readonly profileStore: Store<fromProfile.State>,
    private readonly authStore: Store<fromAuth.State>
  ) {}

  ngOnInit(): void {
    this.subscription.add(
      combineLatest(
        this.store.pipe(select(fromProfile.Selectors.getProfile())),
        this.authStore.pipe(select(fromAuth.Selectors.getUser()))
      ).subscribe(([profile, user]) => {
        if (!profile) {
          this.profileStore.dispatch(
            fromProfile.Actions.getUser({ id: user.id })
          );
        }
      })
    );

    this.subscription.add(
      combineLatest(
        this.moduleId$,
        this.profile$.pipe(
          filter(profile => !!profile),
          map(profile => profile.clientDetailsTO.timezoneShift),
          map(timezoneShift =>
            timezoneShift ? timezoneShift : { hours: 0, minutes: 0 }
          )
        )
      ).subscribe(([moduleId, timezoneShift]) => {
        this.store.dispatch(
          fromModule.Actions.getModuleSettings({
            moduleId,
            timezoneShift
          })
        );
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  humanizeState(): string {
    return `${this.state} side${
      this.state === IlluminationState.BOTH ? 's' : ''
    }`;
  }

  isSplittable(): boolean {
    return this.state === IlluminationState.BOTH;
  }

  isJoinable(): boolean {
    return !this.isSplittable();
  }

  toggleSplitState(): void {
    this.state =
      this.state !== IlluminationState.BOTH
        ? IlluminationState.BOTH
        : IlluminationState.LEFT;
  }

  deleteModule(systemId: string, moduleId: string): void {
    this.store.dispatch(
      fromDeviceList.Actions.deleteModule({
        systemId,
        moduleId
      })
    );
  }

  saveSettings(systemId: string, moduleId: string, range: HourRange): void {
    const halfLight = this.state === IlluminationState.LEFT;

    this.subscription.add(
      this.profile$.subscribe((profile: UserProfile) => {
        this.store.dispatch(
          fromModule.Actions.setModuleSettings({
            systemId,
            moduleId,
            range,
            halfLight,
            timezoneShift: profile.clientDetailsTO.timezoneShift
          })
        );
      })
    );
  }
}
