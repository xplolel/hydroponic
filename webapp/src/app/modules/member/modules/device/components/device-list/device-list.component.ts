import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as fromDeviceList from '@member/store/features/device-list';
import * as fromModule from '@member/store/features/module';
import { select, Store } from '@ngrx/store';
import { SvgIconTypes } from '@ui/models/icon.model';
import { Subscription } from 'rxjs';
import { HydroModule } from '../../models/hydro-module.model';
import {
  ModuleMeasure,
  SystemMeasure,
  WaterLevel
} from '../../models/measure.model';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss']
})
export class DeviceListComponent implements OnInit, OnDestroy {
  devicesSubscription: Subscription;
  devices$ = this.devicesStore.pipe(
    select(fromDeviceList.Selectors.getDeviceList())
  );

  SvgIconTypes = SvgIconTypes;
  addModuleMode = false;

  moduleForm = this.fb.group({
    name: new FormControl('', [Validators.required])
  });

  systemsMeasure$ = this.devicesStore.pipe(
    select(fromDeviceList.Selectors.getSystemsMeasure())
  );

  modulesMeasure$ = this.devicesStore.pipe(
    select(fromDeviceList.Selectors.getModulesMeasure())
  );

  constructor(
    private readonly fb: FormBuilder,
    private readonly devicesStore: Store<fromDeviceList.State>,
    private readonly moduleStore: Store<fromModule.State>,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.devicesSubscription = this.devices$.subscribe(devices => {
      this.devicesStore.dispatch(
        fromDeviceList.Actions.loadSystemMeasures({ devices })
      );

      const hydroModules = devices.reduce(
        (result, device) => [...result, ...device.modules],
        Array<HydroModule>()
      );
      this.devicesStore.dispatch(
        fromDeviceList.Actions.loadModulesMeasures({ hydroModules })
      );
    });
  }

  getWaterLevel(moduleId: string, measures: ModuleMeasure[]): WaterLevel {
    const result = measures.find(measure => measure.moduleId === moduleId);

    return result ? result.waterLevel : null;
  }

  getHumidity(systemId: string, measures: SystemMeasure[]): WaterLevel {
    const result = measures.find(measure => measure.systemId === systemId);

    return result ? result.humidity : null;
  }

  getTemperature(systemId: string, measures: SystemMeasure[]): WaterLevel {
    const result = measures.find(measure => measure.systemId === systemId);

    return result ? result.temperature : null;
  }

  toggleMode(): void {
    this.addModuleMode = !this.addModuleMode;
    this.moduleForm.reset();
  }

  toggleLight(moduleId: string): void {
    this.moduleStore.dispatch(
      fromModule.Actions.toggleLight({
        moduleId
      })
    );
  }

  addModule(systemId: string): void {
    this.devicesStore.dispatch(
      fromDeviceList.Actions.addModule({
        systemId,
        moduleName: this.moduleForm.controls.name.value
      })
    );

    this.toggleMode();
  }

  edit(moduleId: string): void {
    this.router.navigate([`${moduleId}/details`], {
      relativeTo: this.route
    });
  }

  ngOnDestroy(): void {
    this.devicesSubscription.unsubscribe();
  }
}
