import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IlluminationState } from '../../models/hydro-module.model';

@Component({
  selector: 'app-module-illumination-preview',
  templateUrl: './module-illumination-preview.component.html',
  styleUrls: ['./module-illumination-preview.component.scss']
})
export class ModuleIlluminationPreviewComponent {
  _state: IlluminationState;

  @Output() readonly stateChange = new EventEmitter();

  get state(): IlluminationState {
    return this._state;
  }

  @Input()
  set state(val: IlluminationState) {
    this._state = val;
  }

  getClass(): string {
    return `light-canvas ${this.state}`;
  }
}
