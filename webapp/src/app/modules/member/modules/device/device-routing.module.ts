import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeviceListComponent } from './components/device-list/device-list.component';
import { ModuleDetailsComponent } from './components/module-details/module-details.component';

const routes: Routes = [
  {
    path: '',
    data: {
      name: 'List',
      showReturn: false
    },
    component: DeviceListComponent
  },
  {
    path: ':moduleId/details',
    data: {
      name: 'Details',
      showReturn: true
    },
    component: ModuleDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeviceRoutingModule {}
