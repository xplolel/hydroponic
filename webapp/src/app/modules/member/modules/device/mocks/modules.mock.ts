import { range } from 'lodash';
import { HydroModule } from '../models/hydro-module.model';
import { fabricateEmptySlots, fabricateSlots } from './slots.mock';

export const fabricateEmptyModule = (
  i: number,
  systemId: string
): HydroModule => {
  const id = (parseInt(systemId, 10) * 100 + i).toString();

  return {
    id,
    moduleName: `${Math.random()
      .toString()
      .slice(-5)}`,
    systemId,
    slots: fabricateEmptySlots(id)
  };
};

export const fabricateEmptyModules = (systemId: string): HydroModule[] =>
  range(Math.round(Math.random() * 3) + 1).map(i =>
    fabricateEmptyModule(i + 1, systemId)
  );

export const fabricateModule = (i: number, systemId: string): HydroModule => {
  const id = (parseInt(systemId, 10) * 100 + i).toString();

  return {
    id,
    moduleName: `${Math.random()
      .toString()
      .slice(-5)}`,
    systemId,
    slots: fabricateSlots(id)
  };
};

export const fabricateModules = (systemId: string): HydroModule[] =>
  range(Math.round(Math.random() * 3) + 1).map(i =>
    fabricateModule(i + 1, systemId)
  );
