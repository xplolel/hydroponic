import { mockedPlants } from '@member/modules/plants/mocks';
import { range, sample } from 'lodash';
import { Slot } from '../models/slot.model';

export const fabricateEmptySlot = (
  i: number,
  moduleId: string,
  x: number,
  y: number
): Slot => {
  const id = (parseInt(moduleId, 10) * 100 + i).toString();

  return {
    id,
    plantId: null,
    plantPlantedTime: null,
    moduleId,
    x,
    y
  };
};

export const fabricateEmptySlots = (moduleId: string): Slot[] =>
  range(8).map(i => fabricateEmptySlot(i, moduleId, i >= 4 ? 1 : 0, i % 4));

export const fillSlotWithPlantId = (slot: Slot): Slot => {
  const plant = Math.random() > 0.5 ? sample(mockedPlants) : null;
  const date = new Date();
  date.setDate(date.getDate() - Math.round(Math.random() * 10));

  return {
    ...slot,
    plantId: !!plant ? plant.id : null,
    plantPlantedTime: !!plant ? date.toISOString() : null
  };
};

export const fabricateSlots = (moduleId: string): Slot[] =>
  fabricateEmptySlots(moduleId).map(fillSlotWithPlantId);

export const fillSlotWithMockedPlant = (slot: Slot): Slot => ({
  ...slot,
  plant: slot.plantId
    ? mockedPlants.find(plant => plant.id === slot.plantId)
    : null
});
