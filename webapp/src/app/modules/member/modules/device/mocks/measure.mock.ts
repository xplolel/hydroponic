import {
  ModuleMeasure,
  SystemMeasure,
  WaterLevel
} from '../models/measure.model';

export const mockedSystemMeasure: SystemMeasure = {
  humidity: 10,
  reportedTime: '2020-01-14T10:15:02.017Z',
  systemId: '12121232',
  temperature: 28
};

export const mockedModuleMeasure: ModuleMeasure = {
  moduleId: '1234567',
  reportedTime: '2020-01-14T10:15:02.017Z',
  waterLevel: WaterLevel.LOW
};
