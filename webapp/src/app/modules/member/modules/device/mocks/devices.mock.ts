import { range } from 'lodash';
import { Device } from '../models/device.model';
import { fabricateModules } from './modules.mock';

export const fabricateEmptyDevice = (i: number): Device => {
  const id = i.toString();

  return {
    id,
    ownerId: '1',
    name: `${Math.random()
      .toString()
      .slice(-5)}`,
    modules: []
  };
};

export const fabricateEmptyDevices = (): Device[] =>
  range(Math.round(Math.random() * 1) + 1).map(i =>
    fabricateEmptyDevice(i + 1)
  );

export const fabricateDevice = (i: number): Device => {
  const id = i.toString();

  return {
    id,
    ownerId: '1',
    name: `${Math.random()
      .toString()
      .slice(-5)}`,
    modules: fabricateModules(id)
  };
};

export const fabricateDevices = (): Device[] =>
  range(Math.round(Math.random() * 1) + 1).map(i => fabricateDevice(i + 1));

export let mockedDevices = fabricateDevices();
