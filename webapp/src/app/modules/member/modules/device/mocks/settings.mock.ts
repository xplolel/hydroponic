import { Time } from '@core/models/time';
import { ModuleSettings } from '../models/device-settings.model';

export const mockedSettings: ModuleSettings = {
  halfLight: true,
  moduleId: '1',
  range: {
    from: new Time(5, 0, null),
    to: new Time(10, 0, null)
  }
};
