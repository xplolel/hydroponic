import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { ModuleListComponent } from './components/module-list/module-list.component';
import { PlantDetailsComponent } from './components/plant-details/plant-details.component';
import { PlantItemComponent } from './components/plant-item/plant-item.component';
import { PlantListComponent } from './components/plant-list/plant-list.component';
import { PlantedPlantComponent } from './components/planted-plant/planted-plant.component';
import { PlantsRoutingModule } from './plants-routing.module';

@NgModule({
  imports: [CommonModule, PlantsRoutingModule, UserInterfaceModule],
  declarations: [
    PlantListComponent,
    PlantItemComponent,
    ModuleListComponent,
    PlantDetailsComponent,
    PlantedPlantComponent
  ]
})
export class PlantsModule {}
