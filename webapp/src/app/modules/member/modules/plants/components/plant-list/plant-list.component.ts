import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as fromPlantList from '@member/store/features/plant-list';
import { select, Store } from '@ngrx/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { Plant, PlantTypes } from '../../models';

export class Category {
  name: string;
  plantList: Observable<Plant[]>;
}
@Component({
  selector: 'app-plant-list',
  templateUrl: './plant-list.component.html',
  styleUrls: ['./plant-list.component.scss']
})
export class PlantListComponent implements OnInit {
  filter$: BehaviorSubject<PlantTypes | null> = new BehaviorSubject(null);
  plants$: Observable<Plant[]> = this.store.pipe(
    select(fromPlantList.Selectors.getPlantList()),
    mergeMap(plants =>
      this.filter$.pipe(
        map(categoryName =>
          plants.filter(
            plant => !categoryName || categoryName === plant.plantType
          )
        )
      )
    )
  );

  categories: (PlantTypes | null)[] = [null, ...Object.values(PlantTypes)];

  constructor(
    private readonly store: Store<fromPlantList.State>,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  parseCategory(categoryName: PlantTypes | null): string {
    return categoryName ? categoryName.replace('_', ' ') : 'ALL';
  }

  selectCategory(categoryName: PlantTypes | null): void {
    this.filter$.next(categoryName);
  }

  showDetails(plantId: number): void {
    this.router.navigate([`details/${plantId}`], { relativeTo: this.route });
  }

  ngOnInit(): void {
    this.store.dispatch(fromPlantList.Actions.loadPlantList());
  }

  isSelected(category: PlantTypes | null): boolean {
    return category === this.filter$.getValue();
  }
}
