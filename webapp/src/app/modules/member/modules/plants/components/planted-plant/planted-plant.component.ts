import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import * as fromDeviceList from '@member/store/features/device-list';
import { select, Store } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import { map, switchMap, take } from 'rxjs/operators';

@Component({
  selector: 'app-planted-plant',
  templateUrl: './planted-plant.component.html',
  styleUrls: ['./planted-plant.component.scss']
})
export class PlantedPlantComponent implements OnInit {
  slot$ = this.activeRoute.paramMap.pipe(
    map((params: ParamMap) => [params.get('moduleId'), params.get('slotId')]),
    switchMap(([moduleId, slotId]) =>
      this.store.pipe(
        select(fromDeviceList.Selectors.getSlotById(slotId, moduleId)),
        map(cloneDeep)
      )
    )
  );

  constructor(
    private readonly store: Store<fromDeviceList.State>,
    private readonly activeRoute: ActivatedRoute,
    private readonly router: Router
  ) {}

  clearPlant(moduleId: string, slotId: string): void {
    this.activeRoute.paramMap
      .pipe(map((params: ParamMap) => params.get('systemId')))
      .pipe(take(1))
      .subscribe(systemId => {
        this.store.dispatch(
          fromDeviceList.Actions.clearPlant({ systemId, moduleId, slotId })
        );
        this.router.navigate(['member/plants']);
      });
  }

  ngOnInit(): void {
    this.store.dispatch(fromDeviceList.Actions.fillDeviceListWithPlants());
  }
}
