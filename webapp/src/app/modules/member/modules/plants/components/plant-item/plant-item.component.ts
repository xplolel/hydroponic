import { Component, Input } from '@angular/core';
import { Plant } from '../../models';

@Component({
  selector: 'app-plant-item',
  templateUrl: './plant-item.component.html',
  styleUrls: ['./plant-item.component.scss']
})
export class PlantItemComponent {
  _plant: Plant;
  get plant(): Plant {
    return this._plant;
  }

  @Input()
  set plant(plant: Plant) {
    this._plant = plant;

    // // TO DO - add default image
    // if (plant.id) {
    //   this._plant.imageUrl = 'http://localhost:8080/plants/plant/' + plant.id + '/image';
    // }
  }
}
