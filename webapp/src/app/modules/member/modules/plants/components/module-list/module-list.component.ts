import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as fromDeviceList from '@member/store/features/device-list';
import { select, Store } from '@ngrx/store';
import { SvgIconTypes } from '@ui/models/icon.model';
import { Plant } from '../../models';

@Component({
  selector: 'app-module-list',
  templateUrl: './module-list.component.html',
  styleUrls: ['./module-list.component.scss']
})
export class ModuleListComponent implements OnInit {
  SvgIconTypes = SvgIconTypes;

  devices$ = this.store.pipe(select(fromDeviceList.Selectors.getDeviceList()));

  constructor(
    private readonly store: Store<fromDeviceList.State>,
    private readonly router: Router
  ) {}

  getIcon(plantPlantedTime: Date, growTime: number): SvgIconTypes {
    const oneDay = 24 * 60 * 60 * 1000;
    const diffDays = Math.round(
      Math.abs(
        (new Date().getTime() - new Date(plantPlantedTime).getTime()) / oneDay
      )
    );

    const progress = (diffDays / growTime) * 100;

    return progress < 20
      ? SvgIconTypes.seeds
      : progress < 70
      ? SvgIconTypes.plant
      : SvgIconTypes.plantBig;
  }

  openPlantList(
    plant: Plant,
    deviceId: string,
    moduleId: string,
    slotId: string
  ): void {
    this.router.navigate([
      `/member/plants/list/${deviceId}/${moduleId}/${slotId}${
        !plant ? '' : `/${plant.id}`
      }`
    ]);
  }

  ngOnInit(): void {
    this.store.dispatch(fromDeviceList.Actions.fillDeviceListWithPlants());
  }
}
