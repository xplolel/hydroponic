import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import * as fromDeviceList from '@member/store/features/device-list';
import * as fromPlantList from '@member/store/features/plant-list';
import { select, Store } from '@ngrx/store';
import { cloneDeep } from 'lodash';
import { map, switchMap } from 'rxjs/operators';
import { Plant } from '../../models';

@Component({
  selector: 'app-plant-details',
  templateUrl: './plant-details.component.html',
  styleUrls: ['./plant-details.component.scss']
})
export class PlantDetailsComponent implements OnInit {
  routeParams$ = this.activeRoute.paramMap;
  plantId$ = this.activeRoute.paramMap.pipe(
    map((params: ParamMap) => params.get('plantId'))
  );

  plant$ = this.plantId$.pipe(
    switchMap(id =>
      this.plantsStore.pipe(
        select(fromPlantList.Selectors.getPlantById(id)),
        map(cloneDeep)
      )
    )
  );

  constructor(
    private readonly plantsStore: Store<fromPlantList.State>,
    private readonly devicesStore: Store<fromDeviceList.State>,
    private readonly activeRoute: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.plant$.subscribe(plant => {
      if (!plant) {
        this.plantsStore.dispatch(fromPlantList.Actions.loadPlantList());
      }
    });
  }

  addToSlot(plant: Plant): void {
    this.routeParams$.subscribe((params: ParamMap) => {
      this.devicesStore.dispatch(
        fromDeviceList.Actions.addPlantToSlot({
          plant,
          plantId: params.get('plantId'),
          slotId: params.get('slotId'),
          systemId: params.get('systemId'),
          moduleId: params.get('moduleId')
        })
      );
    });
    this.router.navigate(['member/plants']);
  }
}
