import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleListComponent } from './components/module-list/module-list.component';
import { PlantDetailsComponent } from './components/plant-details/plant-details.component';
import { PlantListComponent } from './components/plant-list/plant-list.component';
import { PlantedPlantComponent } from './components/planted-plant/planted-plant.component';

const routes: Routes = [
  { path: '', component: ModuleListComponent },
  {
    path: 'list/:systemId/:moduleId/:slotId',
    data: {
      name: 'Select',
      showReturn: true
    },
    component: PlantListComponent
  },
  {
    data: {
      name: 'Details',
      showReturn: true
    },
    path: 'list/:systemId/:moduleId/:slotId/details/:plantId',
    component: PlantDetailsComponent
  },
  {
    data: {
      name: 'Details',
      showReturn: true
    },
    path: 'list/:systemId/:moduleId/:slotId/:plantId',
    component: PlantedPlantComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlantsRoutingModule {}
