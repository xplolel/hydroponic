import { DifficultyLevels, Plant, PlantTypes } from '../models';

export const mockedPlant: Plant = {
  id: '7',
  name: 'stevia',
  growTime: 7,
  growDifficulty: DifficultyLevels.easy,
  plantType: PlantTypes.spice,
  mediumImageUrl: 'assets/img/stevia-74187_1920.jpg'
};
