import { DifficultyLevels, Plant, PlantTypes } from '../models';

export const mockedPlants: Plant[] = [
  {
    id: '1',
    name: 'basil',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.spice,
    mediumImageUrl: 'assets/img/basil-917999_1920.jpg'
  },
  {
    id: '2',
    name: 'oregano',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.spice,
    mediumImageUrl: 'assets/img/oregano-2662890_1920.jpg'
  },
  {
    id: '3',
    name: 'tarragon',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.spice,
    mediumImageUrl: 'assets/img/tarragon-115368_1920.jpg'
  },
  {
    id: '4',
    name: 'peppermint',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.spice,
    mediumImageUrl: 'assets/img/peppermint-2326541_1920.jpg'
  },
  {
    id: '5',
    name: 'rosemary',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.spice,
    mediumImageUrl: 'assets/img/rosemary-1583261_1920.jpg'
  },
  {
    id: '6',
    name: 'sage',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.spice,
    mediumImageUrl: 'assets/img/sage-1544884_1920.jpg'
  },
  {
    id: '7',
    name: 'stevia',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.spice,
    mediumImageUrl: 'assets/img/stevia-74187_1920.jpg'
  },
  {
    id: '8',
    name: 'strawberry',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.fruit,
    mediumImageUrl: 'assets/img/strawberry-3501491_1920.jpg'
  },
  {
    id: '9',
    name: 'raspberry',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.fruit,
    mediumImageUrl: 'assets/img/raspberry-3454504_1920.jpg'
  },
  {
    id: '10',
    name: 'blueberry',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.fruit,
    mediumImageUrl: 'assets/img/blueberries-2053127_1920.jpg'
  },
  {
    id: '11',
    name: 'bok choy',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.vegetable,
    mediumImageUrl: 'assets/img/bok-choy-1114678_1920.jpg'
  },
  {
    id: '12',
    name: 'lettuce',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.vegetable,
    mediumImageUrl: 'assets/img/lettuce.jpg'
  },
  {
    id: '13',
    name: 'tomatoes',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.vegetable,
    mediumImageUrl: 'assets/img/tomatoes-320860_1920.jpg'
  },
  {
    id: '14',
    name: 'philodendron',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.houseplant,
    mediumImageUrl: 'assets/img/philodendron-silva-2026502_1920.jpg'
  },
  {
    id: '15',
    name: 'Chinese Evergreen',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.houseplant,
    mediumImageUrl: 'assets/img/ixora-3275918_1920.jpg'
  },
  {
    id: '16',
    name: 'Peace Lily',
    growTime: 7,
    growDifficulty: DifficultyLevels.easy,
    plantType: PlantTypes.houseplant,
    mediumImageUrl: 'assets/img/peace-lily-4040890_1920.jpg'
  }
];
