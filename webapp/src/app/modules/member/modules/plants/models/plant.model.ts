export enum DifficultyLevels {
  easy = 'EASY',
  medium = 'MEDIUM',
  hard = 'HARD'
}

export enum PlantTypes {
  fruit = 'FRUIT',
  vegetable = 'VEGETABLE',
  spice = 'SPICES',
  houseplant = 'HOME_PLANT'
}

export class Plant {
  id?: string;
  name: string;
  growTime: number; // number of days
  growDifficulty: DifficultyLevels;
  plantType: PlantTypes;
  smallImageUrl?: string;
  mediumImageUrl?: string;
  fullImageUrl?: string;
}
