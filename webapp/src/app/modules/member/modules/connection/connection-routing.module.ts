import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnectDeviceComponent } from './components/connect-device/connect-device.component';
import { ExplainerComponent } from './components/explainer/explainer.component';
import { ScanNetworksComponent } from './components/scan-networks/scan-networks.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'explainer/turn-on'
  },
  {
    path: 'explainer/turn-on',
    data: {
      imageUrl: 'assets/img/power.svg',
      caption: 'Connect the system to electricity',
      next: ['../../explainer/connect'],
      step: 0
    },
    component: ExplainerComponent
  },
  {
    path: 'explainer/connect',
    data: {
      imageUrl: 'assets/img/wifi.svg',
      caption:
        "Connect your phone to wireless network named 'Hydroponic-XXXXX'",
      next: ['../../scan'],
      step: 1
    },
    component: ExplainerComponent
  },
  {
    path: 'scan',
    component: ScanNetworksComponent
  },
  {
    path: 'connect/:name',
    component: ConnectDeviceComponent
  },
  {
    path: 'status',
    data: {
      imageUrl: 'assets/img/success.svg',
      caption: "Congratulations, we're setting up your device!",
      next: ['/member/device'],
      step: 4
    },
    component: ExplainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConnectionRoutingModule {}
