import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RaspberryURLs } from '@env/raspberry-urls';
import {
  fabricateEmptyDevice,
  mockedDevices
} from '@member/modules/device/mocks/devices.mock';
import { cloneDeep } from 'lodash';
import { Observable, of } from 'rxjs';
import { catchError, delay, map } from 'rxjs/operators';
import { mockedNetworks } from '../mocks/networks.mock';
import { Network } from '../models/network.model';

export abstract class ConnectionService {
  abstract scanNetworks(): Observable<Network[]>;
  abstract sendPassword(
    name: string,
    password: string,
    userId: string
  ): Observable<string>;
  abstract checkConnectionStatus(systemId: string): Observable<boolean>;
}

@Injectable()
export class MockConnectionService extends ConnectionService {
  scanNetworks(): Observable<Network[]> {
    return of(cloneDeep(mockedNetworks)).pipe(delay(300));
  }

  sendPassword(
    _name: string,
    _password: string,
    _userId: string
  ): Observable<string> {
    const i =
      mockedDevices
        .map(el => parseInt(el.id, 10))
        .reduce((prev, cur) => (cur > prev ? cur : prev), 0) + 1;

    const device = fabricateEmptyDevice(i);

    mockedDevices.push(device);

    return of('Test System Id').pipe(delay(300));
  }

  checkConnectionStatus(_systemId: string): Observable<boolean> {
    return of(true).pipe(delay(300));
  }
}

@Injectable()
export class RestConnectionService extends ConnectionService {
  constructor(private readonly http: HttpClient) {
    super();
  }

  scanNetworks(): Observable<Network[]> {
    return this.http
      .get<{ ssid: string; quality: string }[]>(RaspberryURLs.scanNetworks)
      .pipe(
        map(networks =>
          networks.map(network => {
            const [min, max] = network.quality
              .split('/')
              .map(el => parseInt(el, 10));

            return {
              ...network,
              quality: min / max
            };
          })
        )
      );
  }

  sendPassword(
    ssid: string,
    password: string,
    userId: string
  ): Observable<string> {
    return this.http
      .post<{ systemId: string }>(RaspberryURLs.sendPassword, {
        ssid,
        password,
        userId
      })
      .pipe(map(obj => obj.systemId));
  }

  checkConnectionStatus(systemId: string): Observable<boolean> {
    return this.http
      .get<null>(`/systems/${systemId}`)
      .pipe(catchError(() => of(null)));
  }
}
