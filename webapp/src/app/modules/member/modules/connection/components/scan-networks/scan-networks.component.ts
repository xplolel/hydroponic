import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { ButtonComponent } from '@ui/components/button/button.component';
import { LottieIconAnimation, LottieIconTypes } from '@ui/models/icon.model';
import { ToastrService } from 'ngx-toastr';
import { concat, of, race, Subscription, throwError, timer } from 'rxjs';
import {
  delay,
  exhaustMap,
  filter,
  skip,
  startWith,
  switchMap
} from 'rxjs/operators';
import { Network } from '../../models/network.model';
import { ConnectionService } from '../../services/connection.service';
import * as fromConnection from '../../store';

@Component({
  selector: 'app-scan-networks',
  templateUrl: './scan-networks.component.html',
  styleUrls: ['./scan-networks.component.scss']
})
export class ScanNetworksComponent implements OnDestroy {
  subscription = new Subscription();
  LottieIconTypes = LottieIconTypes;

  _button: HTMLElement;

  @ViewChild('button', { static: false }) set button(val: ButtonComponent) {
    this._button = val.el.nativeElement;
  }

  readonly networks$ = this.store.pipe(
    select(fromConnection.Selectors.getNetworks())
  );

  readonly scanning$ = this.networks$.pipe(
    skip(1),
    switchMap(() => concat(of(false), of(true).pipe(delay(5000)))),
    startWith(true)
  );

  loadingAnimations = [
    new LottieIconAnimation('instant', 1000, Number.POSITIVE_INFINITY, 0, 120)
  ];

  network: Network;

  constructor(
    private readonly store: Store<fromConnection.State>,
    private readonly router: Router,
    private readonly connectionService: ConnectionService,
    private readonly toastr: ToastrService
  ) {
    this.subscription.add(
      this.scanning$
        .pipe(
          filter(val => !!val),
          exhaustMap(() =>
            race(
              this.connectionService.scanNetworks(),
              timer(5000).pipe(switchMap(() => throwError('Timeout')))
            )
          )
        )
        .subscribe(
          networks =>
            this.store.dispatch(
              fromConnection.Actions.loadNetworkListSuccess({ networks })
            ),
          () => {
            this.toastr.error(
              'Please make sure you are connected with the same Wi-Fi as your hydroponic system.',
              'Connection error.'
            );
            this.router.navigate(['/member/connection']);
          }
        )
    );

    this.subscription.add(
      this.networks$
        .pipe(
          filter(
            networks =>
              this.network &&
              !networks.find(network => network.ssid === this.network.ssid)
          )
        )
        .subscribe(() => {
          this.network = null;
        })
    );
  }

  isSelected(network: Network): boolean {
    return this.network && this.network.ssid === network.ssid;
  }

  select(network: Network): void {
    if (this.isSelected(network)) {
      this.network = null;
    } else {
      this.network = network;
      this._button.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest'
      });
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  trackBySsid(_id: number, el: Network): string {
    return el.ssid;
  }

  connect(): void {
    this.router.navigate(['/member/connection/connect', this.network.ssid]);
  }
}
