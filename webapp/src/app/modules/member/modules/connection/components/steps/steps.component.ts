import { Component, Input } from '@angular/core';
import { range } from 'lodash';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss']
})
export class StepsComponent {
  steps = [];
  _max = 0;
  _current = 0;

  @Input() set max(val: number) {
    this._max = val;
    this.updateSteps();
  }

  @Input() set current(val: number) {
    this._current = val;
    this.updateSteps();
  }

  updateSteps(): void {
    this.steps = range(this._max + 1);
  }

  stateClass(step: number): string {
    return this._current > step
      ? 'complete'
      : this._current === step
      ? 'active'
      : '';
  }
}
