import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import * as fromAuth from '@auth/store';
import { select, Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { interval, Subscription } from 'rxjs';
import {
  exhaustMap,
  filter,
  map,
  mapTo,
  startWith,
  switchMap,
  take,
  tap
} from 'rxjs/operators';
import { ConnectionService } from '../../services/connection.service';
import * as fromConnection from '../../store';

@Component({
  selector: 'app-connect-device',
  templateUrl: './connect-device.component.html',
  styleUrls: ['./connect-device.component.scss']
})
export class ConnectDeviceComponent implements OnDestroy, OnInit {
  subscription = new Subscription();

  connectDeviceForm = this.fb.group({
    password: new FormControl('', [Validators.required])
  });

  name: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly store: Store<fromConnection.State>,
    private readonly authStore: Store<fromAuth.State>,
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly connectionService: ConnectionService,
    private readonly toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.subscription = this.route.paramMap
      .pipe(
        map((params: ParamMap) => params.get('name')),
        tap(name => {
          this.name = name;
        }),
        switchMap(name =>
          interval(5000).pipe(
            startWith(0),
            mapTo(name)
          )
        ),
        exhaustMap(ssid =>
          this.connectionService
            .scanNetworks()
            .pipe(
              filter(
                networks => !networks.find(network => network.ssid === ssid)
              )
            )
        )
      )
      .subscribe(() => {
        this.toastr.error(
          'Please try again.',
          `The network "${this.name}" is no longer reachable.`
        );
        this.router.navigate(['/member/connection/scan']);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getPassword(): string {
    return this.connectDeviceForm.controls.password.value;
  }

  onSubmit(): void {
    this.authStore
      .pipe(
        select(fromAuth.Selectors.getUser()),
        take(1),
        map(user => user.id),
        switchMap(id =>
          this.connectionService.sendPassword(this.name, this.getPassword(), id)
        )
      )
      .subscribe(systemId => {
        this.router.navigate(['/member/connection/status']);
        this.store.dispatch(
          fromConnection.Actions.connectionSuccess({ systemId })
        );
      });
  }
}
