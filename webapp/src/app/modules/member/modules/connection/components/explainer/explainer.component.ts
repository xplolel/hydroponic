import { Component } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';

@Component({
  selector: 'app-explainer',
  templateUrl: './explainer.component.html',
  styleUrls: ['./explainer.component.scss']
})
export class ExplainerComponent {
  imageUrl: string;
  caption: string;
  next: RouterLink;
  step: number;

  constructor(activatedRoute: ActivatedRoute) {
    activatedRoute.data.subscribe(({ imageUrl, caption, next, step }) => {
      this.imageUrl = imageUrl;
      this.caption = caption;
      this.next = next;
      this.step = step;
    });
  }

  getBackground(): string {
    return `url(${this.imageUrl})`;
  }
}
