import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { Network } from '../models/network.model';
import { featureKey } from './state';

export const loadNetworkListSuccess = createAction(
  `[${featureKey}] loadNetworkListSuccess`,
  props<{ networks: Network[] }>()
);

export const lostConnection = createAction(
  `[${featureKey}] lostConnection`,
  props<{ ssid: string }>()
);

export const sendPassword = createAction(
  `[${featureKey}] sendPassword`,
  props<{ name: string; password: string }>()
);

export const connectionSuccess = createAction(
  `[${featureKey}] connectionSuccess`,
  props<{ systemId: string }>()
);

export const connectionFailure = createAction(
  `[${featureKey}] connectionFailure`,
  props<{ error: HttpErrorResponse }>()
);
