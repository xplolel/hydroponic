import { Network } from '../models/network.model';

export const featureKey = 'connection';

export interface FeatureState {
  networks: Network[];
}

export const initialState: FeatureState = {
  networks: []
};

export interface State {
  [featureKey]: FeatureState;
}
