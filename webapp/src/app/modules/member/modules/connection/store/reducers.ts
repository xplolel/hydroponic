import { Action, createReducer, on } from '@ngrx/store';
import { Network } from '../models/network.model';
import * as Actions from './actions';
import { FeatureState, initialState } from './state';

export function core(
  _state: FeatureState | undefined,
  action: Action
): FeatureState {
  return createReducer(
    initialState,
    on(Actions.loadNetworkListSuccess, (state, { networks }) => ({
      ...state,
      networks: networks.reduce(
        (res, cur) =>
          !res.find(el => el.ssid === cur.ssid) ? [...res, cur] : res,
        [] as Network[]
      )
    }))
  )(_state, action);
}

export const meta = [];
