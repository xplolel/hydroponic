import { createFeatureSelector, createSelector } from '@ngrx/store';
import { featureKey, FeatureState, State } from './state';

export const selectFeatureState = () =>
  createFeatureSelector<State, FeatureState>(featureKey);

export const getNetworks = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.networks
  );
