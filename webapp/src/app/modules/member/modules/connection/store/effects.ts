import { Injectable } from '@angular/core';
import * as fromAuth from '@auth/store';
import * as fromDeviceList from '@member/store/features/device-list';
import { Actions as Actions$, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { defer, interval, Observable, of, OperatorFunction } from 'rxjs';
import {
  catchError,
  finalize,
  map,
  startWith,
  switchMap,
  take,
  takeWhile,
  tap,
  withLatestFrom
} from 'rxjs/operators';
import { ConnectionService } from '../services/connection.service';
import * as Actions from './actions';

const finalizeWithValue = <T>(
  callback: (value: T) => void
): OperatorFunction<T, T> => (source: Observable<T>) =>
  defer(() => {
    let lastValue: T;

    return source.pipe(
      tap(value => (lastValue = value)),
      finalize(() => callback(lastValue))
    );
  });

@Injectable()
export class Effects {
  sendPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.sendPassword),
      withLatestFrom(this.authStore.select(fromAuth.Selectors.getUser())),
      switchMap(([{ name, password }, user]) =>
        this.connectionService.sendPassword(name, password, user.id).pipe(
          map(systemId => Actions.connectionSuccess({ systemId })),
          catchError(error => of(Actions.connectionFailure({ error })))
        )
      )
    )
  );

  connectionSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(Actions.connectionSuccess),
        map(({ systemId }) => {
          const toast = this.toastr.info(
            'Please wait.',
            'The device is setting up.',
            {
              tapToDismiss: false,
              closeButton: false,
              extendedTimeOut: 0,
              timeOut: 0
            }
          );

          return { systemId, toastId: toast.toastId };
        }),
        switchMap(({ systemId, toastId }) =>
          interval(10000).pipe(
            startWith(0),
            take(6),
            switchMap(() =>
              this.connectionService.checkConnectionStatus(systemId)
            ),
            takeWhile(success => !success, true),
            finalizeWithValue(success => {
              this.toastr.remove(toastId);
              if (success) {
                this.toastr.success(
                  'Your device is ready.',
                  'Setup successful.'
                );
                this.devicesStore.dispatch(
                  fromDeviceList.Actions.loadDeviceList()
                );
              } else {
                this.toastr.error(
                  'Please try again.',
                  'Setting up your device failed.'
                );
              }
            })
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private readonly actions$: Actions$,
    private readonly connectionService: ConnectionService,
    private readonly authStore: Store<fromAuth.State>,
    private readonly devicesStore: Store<fromDeviceList.State>,
    private readonly toastr: ToastrService
  ) {}
}
