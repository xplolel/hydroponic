import { Network } from '../models/network.model';

export const mockedNetworks: Network[] = [
  { ssid: 'Neighbour 1', quality: 0.3 },
  { ssid: 'Home', quality: 0.9 },
  { ssid: 'Neighbour 1', quality: 0.6 },
  { ssid: 'Neighbour 3', quality: 0.6 },
  { ssid: 'Neighbour 4', quality: 0.6 },
  { ssid: 'Neighbour 5', quality: 0.6 },
  { ssid: 'Neighbour 6', quality: 0.6 },
  { ssid: 'Neighbour 7', quality: 0.6 }
];
