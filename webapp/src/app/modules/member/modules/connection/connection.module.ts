import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from '@env/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { UserInterfaceModule } from '@ui/user-interface.module';
import { ConnectDeviceComponent } from './components/connect-device/connect-device.component';
import { ExplainerComponent } from './components/explainer/explainer.component';
import { ScanNetworksComponent } from './components/scan-networks/scan-networks.component';
import { StepsComponent } from './components/steps/steps.component';
import { ConnectionRoutingModule } from './connection-routing.module';
import {
  ConnectionService,
  MockConnectionService,
  RestConnectionService
} from './services/connection.service';
import * as fromConnection from './store';
import { Effects as ConnectionEffects } from './store/effects';

@NgModule({
  imports: [
    CommonModule,
    ConnectionRoutingModule,
    UserInterfaceModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature(
      fromConnection.featureKey,
      fromConnection.Reducers.core,
      {
        metaReducers: fromConnection.Reducers.meta
      }
    ),
    EffectsModule.forFeature([ConnectionEffects])
  ],
  declarations: [
    ConnectDeviceComponent,
    ScanNetworksComponent,
    ExplainerComponent,
    StepsComponent
  ],
  providers: [
    {
      provide: ConnectionService,
      useClass: environment.useMocks
        ? MockConnectionService
        : RestConnectionService
    }
  ]
})
export class ConnectionModule {}
