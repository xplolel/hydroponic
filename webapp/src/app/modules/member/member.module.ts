import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { environment } from '@env/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ToastContainerModule } from 'ngx-toastr';
import { UserInterfaceModule } from '../user-interface/user-interface.module';
import { NavigationComponent } from './components/navigation/navigation.component';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { MemberRoutingModule } from './member-routing.module';
import { MemberPageComponent } from './member.page';
import {
  DeviceService,
  MockDeviceService,
  RestDeviceService
} from './services/device.service';
import {
  MockNotificationService,
  NotificationService,
  RestNotificationService
} from './services/notification.service';
import {
  MockOrderService,
  OrderService,
  RestOrderService
} from './services/order.service';
import {
  MockPlantService,
  PlantService,
  RestPlantService
} from './services/plant.service';
import {
  MockProfileService,
  ProfileService,
  RestProfileService
} from './services/profile.service';
import * as fromMember from './store';
import { Effects as DeviceListEffects } from './store/features/device-list/effects';
import { Effects as ModuleEffects } from './store/features/module/effects';
import { Effects as PlantListEffects } from './store/features/plant-list/effects';
import { Effects as ProfileEffects } from './store/features/profile/effects';

@NgModule({
  declarations: [MemberPageComponent, NavigationComponent, PageHeaderComponent],
  imports: [
    CommonModule,
    MemberRoutingModule,
    UserInterfaceModule,
    ToastContainerModule,
    StoreModule.forFeature(fromMember.featureKey, fromMember.Reducers.core, {
      metaReducers: fromMember.Reducers.meta
    }),
    EffectsModule.forFeature([
      DeviceListEffects,
      ModuleEffects,
      PlantListEffects,
      ProfileEffects
    ])
  ],
  providers: [
    {
      provide: DeviceService,
      useClass: environment.useMocks ? MockDeviceService : RestDeviceService
    },
    {
      provide: PlantService,
      useClass: environment.useMocks ? MockPlantService : RestPlantService
    },
    {
      provide: NotificationService,
      useClass: environment.useMocks
        ? MockNotificationService
        : RestNotificationService
    },
    {
      provide: OrderService,
      useClass: environment.useMocks ? MockOrderService : RestOrderService
    },
    {
      provide: ProfileService,
      useClass: environment.useMocks ? MockProfileService : RestProfileService
    }
  ]
})
export class MemberModule {}
