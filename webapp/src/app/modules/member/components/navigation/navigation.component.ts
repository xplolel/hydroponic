import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { LoadingService } from '@member/services/loading.service';
import { LottieIconComponent } from '@ui/components/icon/lottie-icon/lottie-icon.component';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import {
  LottieIconAnimation,
  LottieIconTypes,
  SvgIconTypes
} from '../../../user-interface/models/icon.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnDestroy, OnInit {
  SvgIconTypes = SvgIconTypes;
  LottieIconTypes = LottieIconTypes;

  subscription = new Subscription();

  @ViewChild('loadingIcon', { static: true })
  loadingIcon: LottieIconComponent;

  constructor(
    private readonly router: Router,
    private readonly loadingService: LoadingService
  ) {}

  getNotificationsLink(): string[] {
    return ['/member/notifications'];
  }

  ngOnInit(): void {
    this.subscription.add(
      this.router.events
        .pipe(filter(val => val instanceof NavigationStart))
        .subscribe(() => {
          this.loadingService.start('navigation');
        })
    );

    this.subscription.add(
      this.router.events
        .pipe(filter(val => val instanceof NavigationEnd))
        .subscribe(() => {
          this.loadingService.stop('navigation');
        })
    );

    this.subscription.add(
      this.loadingService.any$.subscribe(isLoading => {
        this.loadingIcon.dispatch(
          isLoading
            ? new LottieIconAnimation(
                'instant',
                1000,
                Number.POSITIVE_INFINITY,
                0,
                120
              )
            : new LottieIconAnimation('instant', 1000, 1, 120, 240)
        );
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
