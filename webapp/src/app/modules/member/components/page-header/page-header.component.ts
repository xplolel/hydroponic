import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { RoutingService } from '@core/services/routing.service';
import { SvgIconTypes } from '@ui/models/icon.model';
import { combineLatest, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

interface RouteSegment {
  data: Data;
  inheritable: boolean;
}

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent {
  SvgIconTypes = SvgIconTypes;
  showReturn: boolean;
  segments$ = this.routingService.end$.pipe(
    map(() => {
      const segments: Observable<RouteSegment>[] = [];
      let route = this.activatedRoute;

      while (route) {
        const inheritable = !route.component;
        segments.push(route.data.pipe(map(data => ({ inheritable, data }))));
        route = route.firstChild;
      }

      return segments;
    }),
    switchMap(segments => combineLatest(...segments)),
    map((segments: RouteSegment[]) =>
      segments
        .filter(segment => segment.data.name)
        .reduce(
          (res, curr) => {
            const prev = res[res.length - 1];

            return prev && prev.inheritable && prev.data.name === curr.data.name
              ? res
              : [...res, curr];
          },
          [] as RouteSegment[]
        )
    ),
    tap(segments => {
      this.showReturn =
        segments.length && segments[segments.length - 1].data.showReturn;
    }),
    map(segments => segments.map(segment => segment.data.name))
  );

  constructor(
    private readonly routingService: RoutingService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly location: Location
  ) {}

  back(): void {
    this.location.back();
  }
}
