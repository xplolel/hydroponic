export class Credentials {
  email: string;
  password: string;
}

export class RegisterCredentials extends Credentials {
  confirmPassword: string;
}
