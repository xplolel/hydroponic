import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { environment } from '@env/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import {
  AuthService,
  MockAuthService,
  RestAuthService
} from './services/auth.service';
import * as fromAuth from './store';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(fromAuth.featureKey, fromAuth.Reducers.core, {
      metaReducers: fromAuth.Reducers.meta
    }),
    EffectsModule.forFeature([fromAuth.Effects])
  ],
  providers: [
    {
      provide: AuthService,
      useClass: environment.useMocks ? MockAuthService : RestAuthService
    }
  ]
})
export class AuthModule {}
