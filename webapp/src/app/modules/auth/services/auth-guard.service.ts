import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import * as fromAuth from '../store';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(private readonly store: Store<fromAuth.State>) {}

  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(fromAuth.Selectors.getLoggedIn()),
      map(authorized => {
        if (!authorized) {
          this.store.dispatch(fromAuth.Actions.loginRedirect());

          return false;
        }

        return true;
      }),
      take(1)
    );
  }
}
