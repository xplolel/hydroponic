import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { mockedUser } from '../mocks/user.mock';
import { Credentials, User } from '../models';

export abstract class AuthService {
  abstract register(credentials: Credentials): Observable<void>;
  abstract login(credentials: Credentials): Observable<User>;
  abstract sendResetPasswordEmail(email: string): Observable<void>;
  abstract resetPassword(
    credentials: Credentials,
    token: string
  ): Observable<User>;
}

@Injectable()
export class MockAuthService {
  register(_credentials: Credentials): Observable<void> {
    return of(null).pipe(delay(300));
  }

  login(_credentials: Credentials): Observable<User> {
    return of(mockedUser).pipe(delay(300));
  }

  sendResetPasswordEmail(_email: string): Observable<void> {
    return of(null);
  }

  resetPassword(_credentials: Credentials, token: string): Observable<User> {
    return of(mockedUser);
  }
}

@Injectable()
export class RestAuthService {
  constructor(private readonly http: HttpClient) {}

  login(credentials: Credentials): Observable<User> {
    const url = '/clients/login';

    return this.http.post<User>(url, credentials).pipe(
      map(user => user.token),
      map(token => {
        const decoded = jwt_decode<{ id: string; email: string }>(token);

        return {
          id: decoded.id,
          login: decoded.email,
          token
        };
      })
    );
  }

  register(credentials: Credentials): Observable<void> {
    const url = '/clients/register';

    return this.http.post<null>(url, credentials);
  }

  sendResetPasswordEmail(email: string): Observable<void> {
    const url = '/clients-forgotten-password';

    return this.http.post<null>(url, null, { params: { email } });
  }

  resetPassword(credentials: Credentials, token: string): Observable<User> {
    const url = '/clients-reset-password';

    return this.http.post<null>(url, { ...credentials, token });
  }
}
