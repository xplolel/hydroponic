import { User } from '../models';

export const mockedUser: User = {
  id: 'kokosik',
  login: 'kokosik',
  token: 'kokosik'
};
