import { Action, createReducer, on } from '@ngrx/store';
import * as Actions from '../actions';
import { FeatureState, initialState } from '../state';
import { resetOnLogout } from './reset-on-logout';

export function core(
  _state: FeatureState | undefined,
  action: Action
): FeatureState {
  return createReducer(
    initialState,
    on(Actions.loginSuccess, (state, { user }) => ({ ...state, user })),
    on(Actions.logout, () => initialState)
  )(_state, action);
}

export const meta = [resetOnLogout];
