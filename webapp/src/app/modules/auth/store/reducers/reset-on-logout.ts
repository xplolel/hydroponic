import { ActionReducer } from '@ngrx/store';
import * as Actions from '../actions';

export function resetOnLogout(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) =>
    reducer(action.type === Actions.logout.type ? undefined : state, action);
}
