import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions as Actions$, createEffect, ofType } from '@ngrx/effects';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, switchMap, tap } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import * as Actions from './actions';

@Injectable()
export class Effects {
  loginRedirect$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(Actions.loginRedirect),
        tap(() => {
          this.router.navigate(['/public/login']);
        })
      ),
    { dispatch: false }
  );

  logout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(Actions.logout),
        tap(() => {
          this.router.navigate(['/public/login']);
          localStorage.removeItem('user');
        })
      ),
    { dispatch: false }
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.login),
      map(action => action.credentials),
      switchMap(credentials =>
        this.authService.login(credentials).pipe(
          map(user => Actions.loginSuccess({ user })),
          catchError(error => of(Actions.loginFailure({ error })))
        )
      )
    )
  );

  loginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(Actions.loginSuccess),
        map(action => action.user),
        tap(user => {
          this.router.navigate(['/']);
          localStorage.setItem('user', JSON.stringify(user));
        })
      ),
    { dispatch: false }
  );

  loginFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(Actions.loginFailure),
        map(action => action.error),
        tap(error => {
          if (error.status === 400) {
            this.toastr.error('Please try again.', 'Wrong email or password.');
          }
        })
      ),
    { dispatch: false }
  );

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.register),
      map(action => action.credentials),
      exhaustMap(credentials =>
        this.authService.register(credentials).pipe(
          map(() => Actions.login({ credentials })),
          catchError(error => of(Actions.registerFailure({ error })))
        )
      )
    )
  );

  registerFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(Actions.registerFailure),
        map(action => action.error),
        tap(error => {
          if (error.status === 409) {
            this.toastr.error(
              'Please try another email.',
              'User already exists.'
            );
          }
        })
      ),
    { dispatch: false }
  );

  sendResetPasswordEmail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.sendResetPasswordEmail),
      map(action => action.email),
      exhaustMap(email =>
        this.authService.sendResetPasswordEmail(email).pipe(
          map(() => Actions.sendResetPasswordEmailSuccess()),
          catchError(error =>
            of(Actions.sendResetPasswordEmailFailure({ error }))
          )
        )
      )
    )
  );

  resetPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(Actions.resetPassword),
      exhaustMap(({ credentials, token }) =>
        this.authService.resetPassword(credentials, token).pipe(
          map(() => Actions.login({ credentials })),
          catchError(error => of(Actions.resetPasswordFailure({ error })))
        )
      )
    )
  );

  constructor(
    private readonly actions$: Actions$,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly toastr: ToastrService
  ) {}
}
