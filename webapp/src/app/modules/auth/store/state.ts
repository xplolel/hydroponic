import { User } from '../models';

export const featureKey = 'auth';

export interface FeatureState {
  user: User | null;
}

export const initialState: FeatureState = {
  user: !localStorage.getItem('user')
    ? null
    : JSON.parse(localStorage.getItem('user'))
};

export interface State {
  [featureKey]: FeatureState;
}
