import { createFeatureSelector, createSelector } from '@ngrx/store';
import { featureKey, FeatureState, State } from './state';

export const selectFeatureState = () =>
  createFeatureSelector<State, FeatureState>(featureKey);

export const getUser = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => state.user
  );

export const getToken = () =>
  createSelector(
    selectFeatureState(),
    (state: FeatureState) => (state.user ? state.user.token : null)
  );

export const getLoggedIn = () =>
  createSelector(
    getUser(),
    user => !!user
  );
