import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { Credentials, User } from '../models';
import { featureKey } from './state';

export const login = createAction(
  `[${featureKey}] login`,
  props<{ credentials: Credentials }>()
);

export const loginSuccess = createAction(
  `[${featureKey}] loginSuccess`,
  props<{ user: User }>()
);

export const loginFailure = createAction(
  `[${featureKey}] loginFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const loginRedirect = createAction(`[${featureKey}] loginRedirect`);

export const register = createAction(
  `[${featureKey}] register`,
  props<{ credentials: Credentials }>()
);

export const registerFailure = createAction(
  `[${featureKey}] registerFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const logout = createAction(`[${featureKey}] logout`);

export const sendResetPasswordEmail = createAction(
  `[${featureKey}] sendResetPasswordEmail`,
  props<{ email: string }>()
);

export const sendResetPasswordEmailSuccess = createAction(
  `[${featureKey}] sendResetPasswordEmailSuccess`
);

export const sendResetPasswordEmailFailure = createAction(
  `[${featureKey}] sendResetPasswordEmailFailure`,
  props<{ error: HttpErrorResponse }>()
);

export const resetPassword = createAction(
  `[${featureKey}] resetPassword`,
  props<{ credentials: Credentials; token: string }>()
);

export const resetPasswordFailure = createAction(
  `[${featureKey}] resetPasswordFailure`,
  props<{ error: HttpErrorResponse }>()
);
