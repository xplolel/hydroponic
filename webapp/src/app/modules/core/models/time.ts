export class Time {
  constructor(
    public hours: number,
    public minutes: number,
    public seconds: number
  ) {}

  incrementHours(): void {
    this.hours++;
    this.validate();
  }

  incrementMinutes(): void {
    this.minutes++;
    this.validate();
  }

  incrementSeconds(): void {
    this.seconds++;
    this.validate();
  }

  decrementHours(): void {
    this.hours--;
    this.validate();
  }

  decrementMinutes(): void {
    this.minutes--;
    this.validate();
  }

  decrementSeconds(): void {
    this.seconds--;
    this.validate();
  }

  validate(): void {
    this.hours = Number.isInteger(this.hours) ? (this.hours + 24) % 24 : null;

    this.minutes = Number.isInteger(this.minutes)
      ? (this.minutes + 60) % 60
      : null;

    this.seconds = Number.isInteger(this.seconds)
      ? (this.seconds + 60) % 60
      : null;
  }

  humanize(separator = ':'): string {
    let result = `${
      Number.isInteger(this.hours)
        ? `${humanizeTimeUnit(this.hours)}${separator}`
        : ''
    }${
      Number.isInteger(this.minutes)
        ? `${humanizeTimeUnit(this.minutes)}${separator}`
        : ''
    }${
      Number.isInteger(this.seconds)
        ? `${humanizeTimeUnit(this.seconds)}${separator}`
        : ''
    }`;

    if (result.length > separator.length) {
      result = result.slice(0, -separator.length);
    }

    return result;
  }
}

export const humanizeTimeUnit = (val: number): string =>
  `${val < 10 ? 0 : ''}${val}`;

export const timeEquals = (a: Time, b: Time): boolean =>
  a.hours === b.hours && a.minutes === b.minutes && a.seconds === b.seconds;
