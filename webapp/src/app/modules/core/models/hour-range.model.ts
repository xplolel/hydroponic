import { Time } from '@core/models/time';

export interface HourRange {
  from: Time;
  to: Time;
}
