import { Injectable } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { BehaviorSubject, Observable, OperatorFunction } from 'rxjs';
import { filter, pairwise, startWith } from 'rxjs/operators';

type Transition<T extends RouterEvent> = [T, T];

const setupObservable = <T extends RouterEvent>(
  s: BehaviorSubject<Transition<T>>
) => s.asObservable().pipe(filter(val => !!val));

const setupSubject = <T extends RouterEvent>(
  router: Router,
  s: BehaviorSubject<Transition<T>>,
  filterFn: OperatorFunction<T, T>
) =>
  router.events
    .pipe(
      filterFn,
      startWith(null),
      pairwise()
    )
    .subscribe(e => s.next(e));

@Injectable({
  providedIn: 'root'
})
export class RoutingService {
  private readonly _end: BehaviorSubject<Transition<NavigationEnd>>;
  readonly end$: Observable<Transition<NavigationEnd>>;

  constructor(readonly router: Router) {
    this._end = new BehaviorSubject<Transition<NavigationEnd>>(null);
    this.end$ = setupObservable(this._end);
    setupSubject(
      router,
      this._end,
      filter((e): e is NavigationEnd => e instanceof NavigationEnd)
    );
  }
}
