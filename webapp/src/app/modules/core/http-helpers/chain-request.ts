import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { arrayRequest } from './array-request';

export const chainRequest = <T, U>(
  obj: T,
  arrName: string,
  callback: (elem: U) => Observable<U>
): Observable<T> =>
  arrayRequest(obj[arrName], callback).pipe(
    mergeMap(arr =>
      of({
        ...obj,
        [arrName]: arr
      })
    )
  );
