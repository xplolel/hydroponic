import { forkJoin, Observable, of } from 'rxjs';

export const arrayRequest = <T>(
  arr: T[],
  callback: (elem: T) => Observable<T>
): Observable<T[]> => (arr.length ? forkJoin(arr.map(callback)) : of([]));
