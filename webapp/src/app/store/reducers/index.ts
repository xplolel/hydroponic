import { environment } from '@env/environment';
import { MetaReducer } from '@ngrx/store';
import { logReducer } from './log-reducer';

export const meta: MetaReducer<{}>[] = !environment.production
  ? [logReducer]
  : [];

export const core = {};
