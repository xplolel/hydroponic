import { ActionReducer } from '@ngrx/store';

export function logReducer(reducer: ActionReducer<{}>): ActionReducer<{}> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    // tslint:disable: no-console
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();
    // tslint:enable: no-console

    return result;
  };
}
