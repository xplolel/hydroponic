import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { RaspberryURLs } from '@env/raspberry-urls';
import { Observable } from 'rxjs';

@Injectable()
export class ApiEndpointInterceptor implements HttpInterceptor {
  constructor(@Inject('ApiEndpoint') private readonly apiEndpoint: string) {}
  intercept(
    // tslint:disable-next-line: no-any
    request: HttpRequest<any>,
    next: HttpHandler
    // tslint:disable-next-line: no-any
  ): Observable<HttpEvent<any>> {
    return next.handle(
      request.url.includes('assets')
        ? request
        : request.clone({
            url: `${
              request.url.includes(RaspberryURLs.scanNetworks) ||
              request.url.includes(RaspberryURLs.sendPassword)
                ? environment.raspberryApiURL
                : this.apiEndpoint
            }${request.url}`
          })
    );
  }
}
