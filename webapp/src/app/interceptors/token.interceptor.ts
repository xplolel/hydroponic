import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as fromAuth from '@auth/store';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private readonly store: Store<fromAuth.State>) {}
  intercept(
    // tslint:disable-next-line: no-any
    request: HttpRequest<any>,
    next: HttpHandler
    // tslint:disable-next-line: no-any
  ): Observable<HttpEvent<any>> {
    return this.store.pipe(
      select(fromAuth.Selectors.getToken()),
      take(1),
      mergeMap(token => {
        const authReq = !!token
          ? request.clone({
              setHeaders: { Authorization: `Bearer ${token}` }
            })
          : request;

        return next.handle(authReq);
      })
    );
  }
}
