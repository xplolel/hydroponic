import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpSentEvent
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as fromAuth from '@auth/store';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private readonly store: Store<fromAuth.State>,
    private readonly toastr: ToastrService
  ) {}

  intercept(
    // tslint:disable-next-line: no-any
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<HttpSentEvent>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.toastr.error('Please try logging in again', 'Unauthorized');
          this.store.dispatch(fromAuth.Actions.logout());
        }

        return throwError(error);
      })
    );
  }
}
